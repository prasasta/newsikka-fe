@php
    //dd($tubel->data);
        $batas = 10;

        $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;

        $halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;

        $previous = $halaman - 1;

        $next = $halaman + 1;

        $jumlah_data = 8;

        $total_halaman = ceil($jumlah_data / $batas);

        $nomor = $halaman_awal + 1;
@endphp

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <!--begin::Table-->
    <table class="table table-striped  align-middle" id="tabel_tubel">
        <!--begin::Thead-->
        <thead class="fw-bolder bg-secondary fs-6">
            <tr class="text-right">
                <th class="text-center rounded-start">&nbsp;No</th>
                <th class="">Jenjang Pendidikan</th>
                <th class="">Data Pegawai</th>
                <th class="">Semester / IPK / Jumlah SKS</th>
                <th class="">Perguruan Tinggi / Program Studi</th>
                <th class="">Attachment</th>
                @if ($tab=='tolak')
                <th class="">Alasan Tolak</th>
                @endif
                <th class="rounded-end text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if ($tubel->data != null)
        @php
            $no = $start_no;
        @endphp
            @foreach ($tubel->data as $t)
            <tr class="text-right">
                <td class="text-center text-dark  mb-1 fs-6">
                    {{$no++}}
                </td>

                <td class="text-dark  mb-1 fs-6">
                    {{ $t->dataIndukTubelId->jenjangPendidikanId ? $t->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan : null }}
                </td>

                <td class="text-dark  mb-1 fs-6">
                    {{ $t->dataIndukTubelId->namaPegawai }} <br>
                    {{ $t->dataIndukTubelId->namaPangkat }} <br>
                    {{ $t->dataIndukTubelId->nip18 }}
                </td>

                <td class="text-dark  mb-1 fs-6">
                    Semester : {{$t->semesterKe}} <br>
                    (@if ($t->semesterKe % 2 == 0)
                        Genap
                    @else
                        Ganjil

                    @endif / {{$t->tahunAkademik}}) <br>
                    IPK : {{$t->ipk}} <br>
                    Jumlah SKS : {{$t->jumlahSks}}

                </td>

                <td class="text-dark  mb-1 fs-6">
                    {{$t->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi}} <br> {{$t->ptPesertaTubelId->programStudiId->programStudi}}

                </td>

                <td class="text-dark  mb-1 fs-6">
                    <a href=""><i class="fa fa-file"></i> Laporan Hasil Studi.pdf</a>
                    <br>
                    <a href=""><i class="fa fa-file"></i>  Kartu Hasil Stusi.pdf</a>
                </td>

                @if ($tab=='tolak')
                    <td class="text-dark  mb-1 fs-6">
                        {{$t->logStatusProbis->keterangan}}
                    </td>
                @endif

                <td class="text-center text-dark mb-1 fs-6">
                    @if ($tab=='tunggu')
                    {{-- <a href="{{ url('tubel/telitilps/'. $t->id) }}"
                        class="btn btn-sm btn-primary m-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Teliti">
                        <i class="fa fa-cog"></i>Teliti
                    </a> --}}
                    <button
                        class="btn btn-sm btn-primary m-2 btnTeliti" data-bs-toggle="tooltip" data-bs-placement="top" title="Teliti" data-id="">
                        <i class="fa fa-cog"></i>Teliti
                    </button>
                    @elseif($tab=='setuju')
                        <a href="{{ url('tubel/detaillps/'. $t->id) }}"
                            class="btn btn-sm btn-primary m-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail">
                            <i class="fa fa-eye"></i>Detail
                        </a>
                        {{-- <br>
                        <a href="#"
                            class="btn btn-sm btn-light-success m-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                            <i class="fa fa-paper-plane"></i>Kirim
                        </a> --}}
                    @elseif($tab=='tolak')
                        <a href="{{ url('tubel/detaillps/'. $t->id) }}"
                            class="btn btn-sm btn-primary m-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail">
                            <i class="fa fa-eye"></i>Detail
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        @else
                <tr class="text-center">
                    <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
                </tr>
        @endif

        </tbody>
        <!--end::Tbody-->
    </table>

    @if ($tubel->data != null)
    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>
        @php
            if ($page>1){
                $prev = $page -1;

            }else{
                $prev = 1;
            }
            $next = $page +1;

        @endphp
        <!--begin::Pages-->
        {{-- <ul class="pagination">
            <li class="page-item previous">
                <a href="{{ url($link.'?page='.$prev) }}" class="page-link">
                    <i class="previous"></i>
                </a>
            </li>
            <li class="page-item active">
                <a href="#" class="page-link">1</a>
            </li>
            <li class="page-item">
                <a href="#" class="page-link">2</a>
            </li>
            <li class="page-item next">
                <a href="{{ url($link.'?page='.$next) }}" class="page-link">
                    <i class="next"></i>
                </a>
            </li>
        </ul> --}}
        <!--end::Pages-->
    </div>
    @endif

</div>
