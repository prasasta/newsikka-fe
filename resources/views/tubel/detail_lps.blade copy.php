@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        @if ($tab=='teliti')
                        <span class="fw-bold fs-3 text-white">Form Penelitian LPS</span>
                        @else
                        <span class="fw-bold fs-3 text-white">Detail LPS</span>
                        @endif

                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md row mb-3">
                        @if ($tab!== 'teliti')
                        <div class="col">
                            <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
                        </div>

                        @endif

                        <div class="col text-end">
                        </div>
                    </div>

                    <div class="rounded border p-4 d-flex flex-column mb-6">
                        <div class="row mb-3">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nama Pegawai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">:  {{ $lps->dataIndukTubelId->namaPegawai }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nip 18 </label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">:  {{ $lps->dataIndukTubelId->nip18 }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->jenjangPendidikanId ? $lps->dataIndukTubelId->jenjangPendidikanId->jenjangPendidikan : '#N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Program Beasiswa</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->programBeasiswa ? $lps->dataIndukTubelId->programBeasiswa : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->lokasiPendidikanId ? $lps->dataIndukTubelId->lokasiPendidikanId->lokasi : '#N/A'  }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Lama Pendidikan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->lamaPendidikan ? $lps->dataIndukTubelId->lamaPendidikan . ' Tahun' : '#N/A'}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Total Semester</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->totalSemester ? $lps->dataIndukTubelId->totalSemester . ' Semester': '#N/A' }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Surat Tugas</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->nomorSt ? $lps->dataIndukTubelId->nomorSt : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($lps->dataIndukTubelId->tglSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Mulai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($lps->dataIndukTubelId->tglMulaiSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Selesai</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($lps->dataIndukTubelId->tglSelesaiSt,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <br>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor KEP Pembebasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->nomorKepPembebasan ? $lps->dataIndukTubelId->nomorKepPembebasan : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($lps->dataIndukTubelId->tglKepPembebasan,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">TMT</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($lps->dataIndukTubelId->tmtKepPembebasan,'j F Y','') }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Email Non Kedinasan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->emailNonPajak ? $lps->dataIndukTubelId->emailNonPajak : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Nomor Handphone</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->noHandphone ? $lps->dataIndukTubelId->noHandphone : '#N/A' }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-5 col-md fw-bold fs-6">Alamat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $lps->dataIndukTubelId->alamatTinggal ? $lps->dataIndukTubelId->alamatTinggal : '#N/A' }} </span>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>


                    <div class="rounded border p-4 d-flex flex-column mb-6">
                        <div class="row">
                            <div class="form-group col-6 mb-4">
                                <label class="col col-form-label">Nama Perguruan Tinggi</label>
                                <div class="col-10">
                                    <input class="form-control" value="{{ $lps->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" disabled>
                                </div>
                            </div>
                            <div class="form-group col-3 mb-4">
                                <label class="col col-form-label">Tahun Akademik</label>
                                <div class="col-8">
                                    <input class="form-control" value="{{ $lps->tahunAkademik }}" disabled>
                                </div>
                            </div>
                            <div class="form-group col-3 mb-4">
                                <label class="col col-form-label">Jumlah SKS</label>
                                <div class="col-8">
                                    <input class="form-control" value="{{ $lps->jumlahSks }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6 mb-4">
                                <label class="col col-form-label">Program Studi</label>
                                <div class="col-10">
                                    <input class="form-control" value="{{ $lps->ptPesertaTubelId->programStudiId->programStudi }}" disabled>
                                </div>
                            </div>
                            <div class="form-group col-3 mb-4">
                                <label class="col col-form-label">Semester Genap/Ganjil</label>
                                <div class="col-8">
                                    <input class="form-control"
                                    @if ($lps->flagSemester == 1)
                                        value="Ganjil"
                                    @elseif ($lps->flagSemester == 2)
                                        value="Genap"
                                    @else
                                        value="-"
                                    @endif disabled>
                                </div>
                            </div>
                            <div class="form-group col-3 mb-4">
                                <label class="col col-form-label">IPK</label>
                                <div class="col-8">
                                    <input class="form-control" value="{{ $lps->ipk }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6 mb-4">
                            </div>
                            <div class="form-group col-3 mb-4">
                                <label class="col col-form-label">Semester</label>
                                <div class="col-8">
                                    <input class="form-control" value="{{ $lps->semesterKe }}" disabled>
                                </div>
                            </div>
                            <div class="form-group col-3 mb-4">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col mb-4">
                                <label class="col-2 col-form-label">File LPS / KHS</label>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed rounded-3 p-6">
                                            <div class="d-flex flex-stack flex-grow-1">
                                                <div class="fw-bold">
                                                    <h4 class="text-gray-900 fw-bolder">File LPS</h4>
                                                    <a href="{{ url('files/' . $lps->pathLps) }}" class="fw-bolder">{{ $lps->pathLps }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed rounded-3 p-6">
                                            <div class="d-flex flex-stack flex-grow-1">
                                                <div class="fw-bold">
                                                    <h4 class="text-gray-900 fw-bolder">File KHS</h4>
                                                    <a href="{{ url('files/' . $lps->pathKhs) }}" class="fw-bolder">{{ $lps->pathKhs }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        @if ($tab == 'teliti')
                            <a href="javascript: history.go(-1)" class="btn btn-sm btn-secondary"><i class="fa fa-reply"></i> Batal</a>
                            <button type="button" data-act="27741cda-2ddf-4b98-8cfb-f9ba4ce3c9bd" data-url="{{url('tubel/updateStatusLps')}}" class="btn btn-sm btn-danger btnTolak"><i class="fa fa-times"></i> Tolak</button>
                            <button type="button" data-act="ffb7abb9-922b-455d-94cb-8290686a8f14"  data-url="{{url('tubel/updateStatusLps')}}" class="btn btn-sm btn-success btnSetuju"><i class="fa fa-check-circle"></i> Setuju</button>
                            {{-- <button type="button" onclick="history.back()" class="btn btn-sm btn-secondary"><< Batal</button> --}}
                        @endif
                    </div>

                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>
@endsection

@section('js')
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-app.js"></script>
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-account.js"></script>

    <script>

        $(".btnSetuju").click(function(){
            var idLps = '{{$idLps}}';
            var act = $(this).data("act");
            var url = $(this).data("url");

            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menyetujui data?',
                text: "Data akan masuk ke tahap berikutnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#40BA50',
                cancelButtonColor: '#B2B2B2',
                reverseButtons: true,
                confirmButtonText: 'Setuju!',
                cancelButtonText: 'Batal'
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(url);
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: idLps,
                            keterangan : null
                        },
                        success: function(response) {
                            //console.log(response['status']);
                            if(response['status']==0){
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                                });



                            }else{
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di setujui',
                                showConfirmButton: false,
                                // width: 600,
                                // padding: '5em',
                                timer: 1600
                                });

                                window.location = "{{url('tubel/lps')}}";

                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });

                }
            })
        });


        $(".btnTolak").click(function(){
            var idLps = '{{$idLps}}';
            var act = $(this).data("act");
            var url = $(this).data("url");

            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                input: 'textarea',
                inputLabel: 'Alasan Tolak',
                inputPlaceholder: 'Masukan Alasan Penolakan',
                inputAttributes: {
                    'aria-label': 'Masukan Alasan Penolakan'
                },
                title: 'Yakin akan menolak data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Tolak!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(result.value);

                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: idLps,
                            keterangan : result.value
                        },
                        success: function(response) {
                            if(response['status']==0){
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                                });

                            }else{
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di tolak',
                                showConfirmButton: false,
                                timer: 1600
                                });
                                window.location = "{{url('tubel/lps')}}";
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });


                }
            })
        });

    </script>
@endsection
