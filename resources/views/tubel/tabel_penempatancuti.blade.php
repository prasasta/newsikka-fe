<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_keppenempatancuti">
        <thead class="fw-bolder bg-secondary fs-6">
            <tr class="">
                <th class="rounded-start ps-4">Data Pegawai</th>
                <th class="">Pendidikan</th>
                <th class="">Surat Izin PT / Sponsor</th>
                <th class="">Mulai / Selesai Cuti</th>
                <th class="">Kantor / Jabatan Asal</th>
                <th class="">Status</th>
                <th class="rounded-end text-center">File</th>
            </tr>
        </thead>
        <tbody>
        @if ($penempatancuti != null)
            @php
                $no = 1;
            @endphp

            @if ($tab == 'setuju')

                @foreach ($penempatancuti->data as $t)
                    <tr class="text-right">

                        <td class="text-dark fw-bolder mb-1 fs-6 ps-4">
                            {{ $t->indukTubel->namaPegawai }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->nip18 }}</span>
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->indukTubel->jenjangPendidikanId->jenjangPendidikan }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->lokasiPendidikanId->lokasi }}</span>
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->nomorSuratKampus }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKampus,'j F Y','') }}</span>
                            <hr>
                            {{ $t->nomorSuratSponsor }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor,'j F Y','') }}</span>
                        </td>

                        <td class="text-dark  mb-1 fs-6">
                            {{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}
                            <span class="text-muted fw-bold text-muted d-block fs-6">s.d</span>
                            {{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->namaKantor }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->namaJabatan }}</span>
                        </td>

                        <td class="text-center text-dark  mb-1 fs-6">
                            {{-- @if ()

                            @else

                            @endif --}}
                        </td>

                        <td class="text-center text-dark  mb-1 fs-6">

                            @if ($t->logStatusProbis->output == 'Permohonan cuti disetujui')
                                <button class="btn btn-icon btn-sm btn-warning m-2 btnRekamKep"
                                data-nama="{{ $t->indukTubel->namaPegawai }}" data-nip="{{ $t->indukTubel->nip18 }}" data-idcuti="{{ $t->id }}" data-alasancuti="{{$t->alasanCuti}}" data-tglmulai="{{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}" data-tglselesai="{{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}"  data-suratkampus="{{$t->nomorSuratKampus}}" data-tglkampus="{{AppHelper::instance()->indonesian_date($t->tglSuratKampus,'j F Y','')}}" data-suratsponsor="{{$t->nomorSuratSponsor}}" data-tglsponsor="{{AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor,'j F Y','')}}" data-filesuratsponsor="{{$t->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$t->pathSuratIjinCutiKampus}}"
                                data-bs-toggle="tooltip" data-bs-placement="top" title="Rekam">
                                    <i class="fa fa-edit"></i>
                                </button>
                            @else

                            @endif

                        </td>
                    </tr>
                @endforeach

            @elseif ($tab == 'selesai')

                @foreach ($penempatancuti->data as $t)
                    <tr class="text-right">

                        <td class="text-dark fw-bolder mb-1 fs-6 ps-4">
                            {{ $t->indukTubel->namaPegawai }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->nip18 }}</span>
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->indukTubel->jenjangPendidikanId->jenjangPendidikan }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->indukTubel->lokasiPendidikanId->lokasi }}</span>
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->nomorSuratKampus }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKampus,'j F Y','') }}</span>
                            <hr>
                            {{ $t->nomorSuratSponsor }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor,'j F Y','') }}</span>
                        </td>

                        <td class="text-dark  mb-1 fs-6">
                            {{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}
                            <span class="text-muted fw-bold text-muted d-block fs-6">s.d</span>
                            {{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}
                        </td>

                        <td class="text-dark fw-bolder mb-1 fs-6">
                            {{ $t->namaKantor }}
                            <br>
                            <span class="text-muted fw-bold text-muted fs-6">{{ $t->namaJabatan }}</span>
                        </td>

                        <td class="text-dark  mb-1 fs-6">
                            <a href=""><i class="fa fa-file-alt"></i> File KEP Penempatan.pdf</a>
                        </td>

                        <td class="text-center text-dark  mb-1 fs-6">
                            {{-- <button class="btn btn-icon btn-sm btn-info m-2 btnLihatKep"
                            data-nama="{{ $t->indukTubel->namaPegawai }}" data-nip="{{ $t->indukTubel->nip18 }}" data-idcuti="{{ $t->id }}" data-alasancuti="{{$t->alasanCuti}}" data-tglmulai="{{AppHelper::instance()->indonesian_date($t->tglMulaiCuti,'j F Y','')}}" data-tglselesai="{{AppHelper::instance()->indonesian_date($t->tglSelesaiCuti,'j F Y','')}}"  data-suratkampus="{{$t->nomorSuratKampus}}" data-tglkampus="{{AppHelper::instance()->indonesian_date($t->tglSuratKampus,'j F Y','')}}" data-suratsponsor="{{$t->nomorSuratSponsor}}" data-tglsponsor="{{AppHelper::instance()->indonesian_date($t->tglSuratKepSponsor,'j F Y','')}}" data-filesuratsponsor="{{$t->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$t->pathSuratIjinCutiKampus}}"
                            data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat">
                                <i class="fa fa-file-alt"></i>
                            </button> --}}
                        </td>
                    </tr>
                @endforeach

            @endif
        @else
            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="7">Tidak terdapat data</td>
            </tr>
        @endif

        </tbody>
    </table>

    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
        <div class="fs-6 fw-bold text-gray-700"></div>
        <ul class="pagination">
            <li class="page-item previous">
                <a href="#" class="page-link">
                    <i class="previous"></i>
                </a>
            </li>
            <li class="page-item active">
                <a href="#" class="page-link">1</a>
            </li>
            <li class="page-item">
                <a href="#" class="page-link">2</a>
            </li>
            <li class="page-item next">
                <a href="#" class="page-link">
                    <i class="next"></i>
                </a>
            </li>
        </ul>
    </div>
</div>
