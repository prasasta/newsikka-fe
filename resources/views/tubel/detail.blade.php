@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-4 text-white">Administrasi Pegawai Tugas Belajar</span>
                    </h3>

                </div>
                <div class="card-body">
                    <div class="col-md row mb-3">
                        <div class="col">
                            <a href="{{ url('tubel/administrasi') }}" class="btn btn-sm btn-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
                        </div>
                        <div class="col text-end">
                            <a href="{{ $tubel->id. '/update' }}" class="btn btn-sm btn-primary fs-6">
                                <i class="fa fa-edit"></i> Perbarui Data
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->jenjangPendidikanId ? $tubel->jenjangPendidikanId->jenjangPendidikan : '#N/A' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Program Beasiswa</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->programBeasiswa ? $tubel->programBeasiswa : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Kampus STAN</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->flagStan == 0 ? 'Tidak' : 'Ya' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Lokasi Pendidikan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : '#N/A'  }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Lama Pendidikan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lamaPendidikan ? $tubel->lamaPendidikan . ' Tahun' : '#N/A'}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Total Semester</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->totalSemester ? $tubel->totalSemester . ' Semester': '#N/A' }}</span>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <br>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Kantor Asal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantorAsal ? $tubel->namaKantorAsal : '#N/A'  }}
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Jabatan Asal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatanAsal ? $tubel->namaJabatanAsal : '#N/A'  }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor Surat Tugas</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorSt ? $tubel->nomorSt : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Mulai</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglMulaiSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Selesai</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSelesaiSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <br>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor KEP Pembebasan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorKepPembebasan ? $tubel->nomorKepPembebasan : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglKepPembebasan,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">TMT</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tmtKepPembebasan,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Email Non Kedinasan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->emailNonPajak ? $tubel->emailNonPajak : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor Handphone</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->noHandphone ? $tubel->noHandphone : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Alamat</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->alamatTinggal ? $tubel->alamatTinggal : '#N/A' }} </span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <div class="text-end mb-4">
                                        <a href="{{ $tubel->id. '/update' }}" class="btn btn-sm btn-primary fs-6">Perbarui Data</a>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="rounded border p-4 d-flex flex-column mb-8">
                            <div class="col-lg row mb-6">
                                <div class="col-lg text-end">
                                    {{-- <button data-bs-toggle="modal" data-bs-target="#add_perguruan_tinggi"  class="btn btn-sm btn-primary fs-6"> --}}
                                    <button class="btn btn-sm btn-primary fs-6 btnAddPt">
                                        <i class="fa fa-plus"></i> Tambah Perguruan Tinggi
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <table class="table align-middle">
                                    <thead>
                                        <tr class="fw-bolder bg-secondary">
                                            <th class="ps-4 min-w-200px rounded-start">Nama Perguruan Tinggi</th>
                                            <th class="min-w-125px">Program Studi</th>
                                            <th class="min-w-125px">Negara</th>
                                            <th class="min-w-150px">Tanggal Mulai Studi</th>
                                            <th class="min-w-100px text-center rounded-end">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($perguruantinggi)
                                            @foreach ($perguruantinggi as $p)
                                            <tr id="pid{{ $p->id }}" class="">
                                                    <td class="ps-4 text-dark text-hover-primary mb-1 fs-6">
                                                        {{ $p->perguruanTinggiId->namaPerguruanTinggi }}
                                                        {{-- <span id="namaPerguruanTinggi" value="{{ $p->perguruanTinggiId->namaPerguruanTinggi }}" hidden></span> --}}
                                                    </td>
                                                    <td class="text-dark text-hover-primary mb-1 fs-6">
                                                        {{ $p->programStudiId->programStudi }}
                                                    </td>
                                                    <td class="text-dark text-hover-primary mb-1 fs-6">
                                                        {{ $p->negaraPtId->namaNegara }}
                                                    </td>
                                                    <td class="text-dark text-hover-primary mb-1 fs-6">
                                                        {{ $p->tglMulai }}
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-icon btn-warning btnEditPt" data-toggle="modal" data-target="#update_perguruan_tinggi" data-id="{{ $p->id }}" data-1="{{ $p->perguruanTinggiId->id }}" data-2="{{ $p->programStudiId->id }}" data-3="{{ $p->negaraPtId->id }}" data-4="{{ $p->tglMulai }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <button class="btn btn-sm btn-icon btn-danger btnHapusPt" data-id="{{ $p->id }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-center" colspan="5">
                                                    Tidak terdapat data
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                        </div>
                    </div>

                    <div class="card border-warning mt-3" id="tab">
                        <div class="card-header card-header-stretch" id="card-header">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder" id="kt_security_summary_tabs">
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='lps'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=lps#tab') }}">Laporan Perkembangan Studi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='cuti'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=cuti#tab') }}">Pengajuan Cuti Belajar</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='pengaktifan'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=pengaktifan#tab') }}">Permohonan Aktif Kembali</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='perpanjangan'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=perpanjangan#tab') }}">Perpanjangan ST Tubel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade <?php if ($tab=='lps'){ echo 'active show';} ?> " id="lps" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_laporan_perkembangan_studi" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Laporan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($tab=='lps')
                                    @include('tubel.tabel_lps')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='cuti'){ echo 'active show';} ?> " id="cuti" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_cuti" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Permohonan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($tab=='cuti')
                                    @include('tubel.tabel_cuti')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='pengaktifan'){ echo 'active show';} ?> " id="cuti" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_permohonan_aktif" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Permohonan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($tab=='cuti')
                                    @include('tubel.tabel_cuti')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='perpanjangan'){ echo 'active show';} ?> " id="perpanjangan" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_perpanjangan_st" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Permohonan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($tab=='perpanjangan')
                                    @include('tubel.tabel_perpanjangan')
                                    @endif
                                </div>
                            </div>
                            <!--end::Tab content-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>

    {{-- Modal add perguruan tinggi --}}
    <div class="modal fade" id="add_perguruan_tinggi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-600px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4>Tambah Perguruan Tinggi</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" method="post" action="{{ url('/tubel/administrasi/perguruanTinggi/'.$tubel->id) }}">
                    @csrf
                    {{ method_field('post') }}
                    <div class="modal-body scroll-y px-10 px-lg-15">
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Perguruan Tinggi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan nama perguruan tinggi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="pt" required>
                                <option selected disabled>Pilih Perguruan Tinggi</option>
                                @foreach ($ref_perguruantinggi as $pt)
                                    <option value="{{ $pt->id }}">{{ $pt->namaPerguruanTinggi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Program Studi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan perogram studi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="prodi" required>
                                <option selected disabled>Pilih prodi</option>
                                @foreach ($ref_prodi as $p)
                                    <option value="{{ $p->id }}">{{ $p->programStudi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Negara</label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="negara" required>
                                @foreach ($ref_negara as $n)
                                    <option value="{{ $n->id }}">{{ $n->namaNegara }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                    <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control form-control-solid ps-12 flatpickr-input active" id="tgl_mulai_pendidikan2" placeholder="Pilih tanggal..." name="tglMulai" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                        <button type="submit" class="btn btn-sm btn-success">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="update_perguruan_tinggi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-600px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h2>Edit Perguruan Tinggi</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form id="form_update_perguruan_tinggi" class="form" method="patch" action="">
                    @csrf
                    <input type="hidden" id="idTubel" name="idTubel" value="{{ $tubel->id }}" >
                    <input type="hidden" id="idPerguruanTinggi" name="idPerguruanTinggi" value="" >
                    <div class="modal-body scroll-y px-10 px-lg-15">
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Perguruan Tinggi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan nama perguruan tinggi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="pt2" name="pt2" value="" required>
                                @foreach ($ref_perguruantinggi as $pt)
                                    <option value="{{ $pt->id }}">{{ $pt->namaPerguruanTinggi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Program Studi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan perogram studi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="prodi2" name="prodi2" value="" required>
                                @foreach ($ref_prodi as $p)
                                    <option value="{{ $p->id }}">{{ $p->programStudi }}</option>
                                @endforeach
                            </select>                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Negara</label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="negara2" name="negara2" value="" required>
                                @foreach ($ref_negara as $n)
                                    <option value="{{ $n->id }}">{{ $n->namaNegara }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                    <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control form-control-solid ps-12 flatpickr-input active" id="tgl_mulai_pendidikan" placeholder="Pilih tanggal..." name="tglMulai2" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                        <button type="submit" class="btn btn-sm btn-success btnUpdatePt"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Laporan Perkembangan Studi</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form id="formAddLps" class="form" method="post" action="{{ url('tubel/administrasi/'.$tubel->id.'/lps') }}">
                    @csrf
                    {{ method_field('post') }}
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rounded border p-4 d-flex flex-column">
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguran Tinggi</label>
                                <div class="col-lg">
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="perguruantinggi" required>
                                        {{-- <option selected disabled>Pilih Perguruan Tinggi</option> --}}
                                        @foreach ($ptTubel as $pt)
                                            <option value="{{ $pt->id }}">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                                <div class="col-lg">
                                    <input type="text" class="form-control form-control form-control-solid" name="semesterke" required/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                                <div class="col-lg">
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" value="1" name="semester" id="flexRadioDefault" required/>
                                        <label class="form-check-label" for="flexRadioDefault">
                                            Ganjil
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="2" name="semester" id="flexRadioDefault2" required/>
                                        <label class="form-check-label" for="flexRadioDefault">
                                            Genap
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                <div class="col-lg">
                                    <input id="thakademik" type="text" class="form-control form-control form-control-solid" name="tahun" required/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                                <div class="col-lg">
                                    <input id="ipk" type="text" class="form-control form-control form-control-solid" name="ipk" required/>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                                <div class="col-lg">
                                    <input type="text" class="form-control form-control form-control-solid" name="jumlah_sks" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="get_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Laporan Perkembangan Studi</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" method="post" action="{{ url('tubel/administrasi/'.$tubel->id.'/lps') }}">
                    @csrf
                    {{ method_field('post') }}
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama perguran Tinggi</label>
                                <div class="col-lg">
                                    <input id="perguruantinggi" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                                <div class="col-lg">
                                    <input id="semesterke" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                                <div class="col-lg">
                                    <input id="semester" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                <div class="col-lg">
                                    <input id="tahun" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                                <div class="col-lg">
                                    <input id="lihatipk" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                                <div class="col-lg">
                                    <input id="jumlah_sks" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div>
                            {{-- <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan ditolak</label>
                                <div class="col-lg">
                                    <input id="alasan" type="text" class="form-control form-control form-control-solid" disabled />
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Laporan Perkembangan Studi</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form id="formEditLps" class="form" method="patch" action="">
                    @csrf
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-2">
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Nama</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPegawai }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">NIP 18</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nip18 }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Pangkat</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaPangkat }} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md col-lg">
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Jabatan</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Unit Organisasi</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaUnitOrg }} </span>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-2">
                                        <label class="col-lg-4 col-md fw-bold fs-6">Kantor</label>
                                        <div class="col-lg d-flex align-items-center">
                                            <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor }} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rounded border p-4 d-flex flex-column">
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Nama Perguruan Tinggi</label>
                                <div class="col-lg">
                                    <input type="hidden" id="idLps" name="idLps">
                                    {{-- <input id="eperguruantinggi" name="eperguruantinggi" type="text" class="form-control form-control form-control-solid" /> --}}
                                    <div class="col-lg">
                                        <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" id="eperguruantinggi" name="eperguruantinggi" required>

                                            @foreach ($perguruantinggi as $pt)
                                                <option class="optPt" value="{{ $pt->id }}">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</option>
                                            @endforeach
                                            {{-- @foreach ($perguruantinggi as $pt)
                                                <option class="optPt" value="{{ $pt->perguruanTinggiId->id }}">{{ $pt->perguruanTinggiId->namaPerguruanTinggi }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester ke-</label>
                                <div class="col-lg">
                                    <input id="esemesterke" name="esemesterke" type="text" class="form-control form-control form-control-solid" />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Semester</label>
                                <div class="col-lg">
                                    <div class="col-lg">
                                        <div class="form-check form-check-custom form-check-solid mb-2">
                                            <input class="form-check-input" type="radio" value="1" name="esemester" id="rGanjil"/>
                                            <label class="form-check-label" for="flexRadioDefault">
                                                Ganjil
                                            </label>
                                        </div>
                                        <div class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="radio" value="2" name="esemester" id="rGenap"/>
                                            <label class="form-check-label" for="flexRadioDefault">
                                                Genap
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tahun Akademik</label>
                                <div class="col-lg">
                                    <input id="etahun" type="text" name="etahun" class="form-control form-control form-control-solid" />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">IPK</label>
                                <div class="col-lg">
                                    <input id="eipk" type="text" name="eipk" class="form-control form-control form-control-solid" />
                                </div>
                            </div>
                            <div class="row mb-6">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Jumlah SKS</label>
                                <div class="col-lg">
                                    <input id="ejumlah_sks" type="text" name="ejumlah_sks" class="form-control form-control form-control-solid" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                        <button type="submit" class="btn btn-sm btn-success btnUpdateLps"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Permohonan Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formAddCuti" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/addcutibelajar') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulai" placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text" value="{{ old('tglMulaiSt') }}" required>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active datepick" id="tglSelesai" placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text" value="{{ old('tglSelesaiSt') }}" required>
                                </div>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" name="alasan_cuti" rows="4" placeholder="alasan pengajuan cuti belajar" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti" rows="4" placeholder="surat izin cuti" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSurat" placeholder="Pilih tanggal " name="tglSurat" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_cuti">
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor" placeholder="Pilih tanggal " name="tglSuratSponsor" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_sponsors" required>
                            </div>
                        </div>
                    </div>

                    <div class="text-center mb-8">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>

                        <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Permohonan Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form formEditCuti"  method="post" action="{{ url('tubel/administrasi/updatecuti') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">
                    <input type="hidden" id="idCuti" name="idCuti" value="">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulaiEdit" placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text" value="{{ old('tglMulaiSt') }}" required>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglSelesaiEdit" placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text" value="{{ old('tglSelesaiSt') }}" required>
                                    </div>
                                </div>

                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                <div class="col-lg">
                                    <textarea class="form-control" type="text" id="alasanCutiEdit" name="alasan_cuti" rows="4" placeholder="alasan pengajuan cuti belajar" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="noSuratKamEdit" name="surat_izin_cuti" rows="4" placeholder="surat izin cuti" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSuratKamEdit" placeholder="Pilih tanggal " name="tglSurat" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_cuti">
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" id="noSuratSponsorEdit" name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsorEdit" placeholder="Pilih tanggal " name="tglSuratSponsor" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_sponsors" required>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mb-8">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                            <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lihat_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Permohonan Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form formEditCuti"  method="post" action="{{ url('tubel/administrasi/updatecuti') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">
                    <input type="hidden" id="idCuti" name="idCuti" value="">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulaiLihat" placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text" value="{{ old('tglMulaiSt') }}" disabled>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSelesaiLihat" placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text" value="{{ old('tglSelesaiSt') }}" disabled>
                                </div>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                            <div class="col-lg">
                                <textarea class="form-control" type="text" id="alasanCutiLihat" name="alasan_cuti" rows="4" placeholder="alasan pengajuan cuti belajar" disabled></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratKamLihat" name="surat_izin_cuti" rows="4" placeholder="surat izin cuti" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratKamLihat" placeholder="Pilih tanggal " name="tglSurat" type="text" value="{{ old('tglKepPembebasan') }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                            <div class="col-lg">
                                <a href="{{ env('APP_URL' . '/files/') }}" id="filesuratkampuslihat" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Sponsor).pdf</a>
                            </div>
                        </div>
                        <br>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="noSuratSponsorLihat" name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsorLihat" placeholder="Pilih tanggal " name="tglSuratSponsor" type="text" value="{{ old('tglKepPembebasan') }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-8">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                            <div class="col-lg">
                                <a href="{{ env('APP_URL' . '/files/') }}" id="filesuratsponsorlihat" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Sponsor).pdf</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="upload_file_lps" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Attachment Laporan perkembangan Studi</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body scroll-y px-5 px-lg-10">
                    <div class="rounded border p-4 d-flex flex-column mb-6">
                        <form id="uploadkhs" method="put" action="" enctype="multipart/form-data">
                            {{ method_field('PUT') }}
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah KHS</label>
                                <div class="col-lg">
                                    <input id="uidlps" type="hidden" name="">
                                    <input id="filekhs" class="form-control" type="file" name="filekhs">
                                    <span id="messageuploadkhs" class="form-text text-muted" style="display: none">File berhasil diupload.</span>
                                    <div id="messagefilekhs" class="col-lg" style="display: none">
                                        Download file KHS <a href="#" id="urlfilekhs">disini</a>.
                                    </div>
                                </div>
                                <div class="form-label col-lg-2">
                                    <button id="btnUploadKhs" type="submit" class="btn btn-primary fs-6">
                                        <i class="fa fa-upload"></i> Upload
                                    </button>
                                    <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                        Loading
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="rounded border p-4 d-flex flex-column mb-6">
                        <div class="row mb-0">
                            <label class="col-lg-3"></label>
                            <div class="col-lg">
                                Silahkan download template file <a href="">disini</a>.
                            </div>
                        </div>
                        <form id="uploadlps" method="put" action="" enctype="multipart/form-data">
                            {{ method_field('PUT') }}
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Laporan</label>
                                <div class="col-lg">
                                    <input id="unggahidlps" type="hidden" name="">
                                    <input id="filelps" class="form-control" type="file" name="filelps">
                                    <span id="messageuploadlps" class="form-text text-muted" style="display: none">File berhasil diupload.</span>
                                    <div id="messagefilelps" class="col-lg" style="display: none">
                                        Download file LPS <a href="" id="urlfilelps">disini</a>.
                                    </div>
                                </div>
                                <div class="form-label col-lg-2">
                                    <button id="btnUploadLps" class="btn btn-primary">
                                        <i class="fa fa-upload"></i> Upload
                                    </button>
                                    <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                        Loading
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_permohonan_aktif" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Permohonan Pengaktifan</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formAddPengaktifan" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/permohonanaktif') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulaiAktif" placeholder="Pilih tanggal " name="tglMulaiAktif" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="suratAktifKembali" name="suratAktifKembali" placeholder="nomor surat" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratPengaktifan" placeholder="Pilih tanggal " name="tglSuratPengaktifan" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mb-10">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload Dokumen</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_pengaktifan" required>
                            </div>
                        </div>
                        <div class="text-center mb-4">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_permohonan_aktif" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Permohonan Pengaktifan</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formEditPengaktifan" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/permohonanaktif') }}">
                    @csrf
                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">
                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulaiAktifEdit" placeholder="Pilih tanggal " name="tglMulaiAktif" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="suratAktifKembali" name="suratAktifKembaliEdit" placeholder="nomor surat" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratPengaktifanEdit" placeholder="Pilih tanggal " name="tglSuratPengaktifan" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mb-10">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload Dokumen</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" name="file_pengaktifan" required>
                            </div>
                        </div>
                        <div class="text-center mb-4">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lihat_permohonan_aktif" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Permohonan Pengaktifan</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formLihatPengaktifan" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/permohonanaktif') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Mulai Aktif</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglMulaiAktifLihat" placeholder="Pilih tanggal " name="tglMulaiAktif" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">No Surat Aktif Kembali</label>
                            <div class="col-lg">
                                <input class="form-control" type="text" id="suratAktifKembali" name="suratAktifKembaliLihat" placeholder="nomor surat" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                            <div class="col-lg-4">
                                <div class="position-relative d-flex align-items-center">
                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                        <span class="symbol-label bg-secondary">
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                    <input class="form-control ps-12 flatpickr-input active" id="tglSuratPengaktifanLihat" placeholder="Pilih tanggal " name="tglSuratPengaktifan" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mb-10">
                            <label class="col-lg-3 col-form-label fw-bold fs-6">Upload Dokumen</label>
                            <div class="col-lg">
                                <input class="form-control" type="file" id="filePengaktifanLihat" name="filePengaktifanLihat" required>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_perpanjangan_st" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Permohonan Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formAddCuti" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/addcutibelajar') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulai" placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text" value="{{ old('tglMulaiSt') }}" required>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglSelesai" placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text" value="{{ old('tglSelesaiSt') }}" required>
                                    </div>
                                </div>

                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                <div class="col-lg">
                                    <textarea class="form-control" type="text" name="alasan_cuti" rows="4" placeholder="alasan pengajuan cuti belajar" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" name="surat_izin_cuti" rows="4" placeholder="surat izin cuti" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSurat" placeholder="Pilih tanggal " name="tglSurat" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_cuti">
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor" placeholder="Pilih tanggal " name="tglSuratSponsor" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_sponsors" required>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mb-8">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-reply"></i>Batal</button>

                            <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-app.js"></script>
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-account.js"></script>

    <script>
        // assets
        $("#tgl_mulai_pendidikan").flatpickr();
        $("#tgl_mulai_pendidikan2").flatpickr();
        $("#tglSurat").flatpickr();
        $("#tglSuratSponsor").flatpickr();
        $("#tglMulaiAktif").flatpickr();
        $("#tglSuratPengaktifan").flatpickr();
        $(".datepick").flatpickr();

        // button tambah perguruan tinggi
        $(".btnAddPt").click(function(){

            var programPt = '{{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : "" }}';
            var jmlPt = '{{ $countperguruantinggi }}';

            if((programPt == 'Dalam Negeri' && jmlPt == 1)|| (programPt == 'Luar Negeri' && jmlPt == 1)) {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Gagal' ,
                    text: 'tidak dapat menambah data',
                    showConfirmButton: false,
                    timer: 2000
                });
            } else {
                $('#add_perguruan_tinggi').modal('show');
            }

        });

        // button edit perguruan tinggi
        $(".btnEditPt").click(function(){
            $('#update_perguruan_tinggi').modal('show');

            var id = $(this).data('id')
            var idPt = $(this).data('1');
            var idProdi = $(this).data('2');
            var idNegara = $(this).data('3');
            var tglMulai = $(this).data('4');

            // console.log()

            $('#idPerguruanTinggi').val(id).change();
            $('#pt2').val(idPt).change();
            $('#prodi2').val(idProdi).change();
            $('#negara2').val(idNegara).change();
            $('#tgl_mulai_pendidikan').val(tglMulai);
        });

        // button edit perguruan tinggi aksi
        $(".btnUpdatePt").click(function(e){
            e.preventDefault();

            var id = $('#idPerguruanTinggi').val();
            var idTubel = $('#idTubel').val();
            var idPt = $('#pt2').val();
            var idProdi = $('#prodi2').val();
            var idNegara = $('#negara2').val();
            var tglMulai = $('#tgl_mulai_pendidikan').val();

            // var url = 'http://localhost:8000/tubel/administrasi/perguruanTinggi/'+id
            var url = '{{ env('APP_URL') }}' + '/tubel/administrasi/perguruanTinggi/'+id
            var token = $("meta[name='csrf-token']").attr("content")

            $.ajax({
                type:'patch',
                url: url,
                data: {
                    _token: token,
                    id: id,
                    idTubel: idTubel,
                    idPt: idPt,
                    idProdi: idProdi,
                    idNegara: idNegara,
                    tglMulai: tglMulai
                },
                success:function(response){
                    // console.log(response)
                        if (response.status===1) {
                            location.reload();
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Sukses!',
                                text: 'Data berhasil diupdate',
                                showConfirmButton: false,
                                timer: 1800
                            })

                        } else {
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1800
                            });
                        }
                        $('#update_perguruan_tinggi').modal('hide');

                },
                error: function(err) {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Data gagal diupdate',
                        showConfirmButton: false,
                        // width: 600,
                        // padding: '5em',
                        timer: 1200
                    })
                }
            });
        });

        // button hapus perguruan tinggi
        $(".btnHapusPt").click(function(){
            var id = $(this).data("id")
            var token = $("meta[name='csrf-token']").attr("content")
            // var url = 'http://localhost:8000/tubel/administrasi/perguruanTinggi/'+id
            var url = '{{ env('APP_URL') }}' + '/tubel/administrasi/perguruanTinggi/'+id

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: '<i class="fa fa-reply text-white"></i> Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {
                            $('#pid'+id).remove();

                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                title: 'Berhasil',
                                text: 'data berhasil dihapus',
                                // width: 600,
                                // padding: '5em',
                                timer: 1200
                            })
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });

                }
            })
        });

        // button lihat lps
        $(".btnLihatLps").click(function(){
            $('#get_laporan_perkembangan_studi').modal('show');

            var id = $(this).data('id')

            $('#perguruantinggi').val($(this).data('pt')).change();
            $('#semesterke').val($(this).data('smtke')).change();
            $('#semester').val($(this).data('smt')).change();
            $('#tahun').val($(this).data('th')).change();
            $('#lihatipk').val($(this).data('ipk')).change();
            $('#jumlah_sks').val($(this).data('sks')).change();
            // $('#alasan').val($(this).data('alasan')).change();
        });

        // button edit lps
        $(".btnEditLps").click(function(){
            $('#edit_laporan_perkembangan_studi').modal('show');

            var id = $(this).data('id')
            var idpt = $(this).data('uidpt')
            var pt = $(this).data('pt')

            if($(this).data('smt') == 1) {
                document.getElementById("rGanjil").checked = true;
            } else if ($(this).data('smt') == 2) {
                document.getElementById("rGenap").checked = true;
            }

            $('#idLps').val(id).change();
            // $('#eperguruantinggi').val($(this).data('pt')).change();
            $('#eperguruantinggi').val(idpt).change(); //
            $('#esemesterke').val($(this).data('smtke')).change();
            $('#etahun').val($(this).data('th')).change();
            $('#eipk').val($(this).data('ipk')).change();
            $('#ejumlah_sks').val($(this).data('sks')).change();

        });

        // button edit lps aksi
        $(".btnUpdateLps").click(function(e){
            e.preventDefault();

            var idLps = $('#idLps').val();
            var token = $("meta[name='csrf-token']").attr("content");

            // var url = 'http://localhost:8000/tubel/adminis/trasi/'+idLps+'/lps';
            var url = '{{ env('APP_URL') }}' + '/tubel/administrasi/'+idLps+'/lps';

            var semesterke = $('#esemesterke').val();
            var semester = $("input[name=esemester]:checked").val();
            var ipk = $('#eipk').val();
            var jumlah_sks = $('#ejumlah_sks').val();
            var tahun = $('#etahun').val();
            var perguruantinggi = $('#eperguruantinggi').val();

            $.ajax({
                type:'patch',
                url: url,
                data: {
                    _token: token,
                    idLps: idLps,
                    semesterke: semesterke,
                    semester: semester,
                    ipk: ipk,
                    jumlah_sks: jumlah_sks,
                    tahun: tahun,
                    perguruantinggi: perguruantinggi
                },
                success: function(response){
                    location.reload();
                    // $('#edit_laporan_perkembangan_studi').modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Sukses',
                        text: 'Data berhasil diupdate',
                        showConfirmButton: false,
                        timer: 1800
                    })

                },
                error: function(err) {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Data gagal diupdate',
                        showConfirmButton: false,
                        timer: 1800
                    })
                }
            });

        });

        // button hapus laporan perkembangan studi
        $(".btnHapusLps").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(this).data("url");

            //alert(url);

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(result.value);

                    $.ajax({
                        type: 'delete',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {
                            console.log(response);

                            if(response.status = 200){
                                $('#lid'+id).remove();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1800
                                });


                            } else {

                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });

                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });


                }
            })

        });

        // button kirim laporan perkembangan studi
        $('.btnKirimLps').click(function(){
            var id = $(this).data("id");
            var outputid = $(this).data("outputid");
            var output = $(this).data("output");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengirim laporan?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#40BA50',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(result.value);

                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/administrasi/lpskirim',
                        data: {
                            _token: token,
                            id: id,
                            outputid: outputid,
                            output: output
                        },
                        success: function(response) {
                            location.reload();
                            // console.log(response);
                            if(response.status == 1){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Laporan berhasil dikirim',
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });


                }
            })
        });

        // button lihat cuti
        $(".btnLihatCuti").click(function(){

            var id = $(this).data('id')
            var tglmulai = $(this).data('tglmulai');
            var tglselesai = $(this).data('tglselesai');
            var alasan = $(this).data('alasan');
            var nokampus = $(this).data('nokampus');
            var tglsuratkampus = $(this).data('tglsuratkampus');
            var nosponsor = $(this).data('nosponsor');
            var tglsuratsponsor = $(this).data('tglsuratsponsor');
            var filesuratkampus = $(this).data('filesuratkampus');
            var filesuratsponsor = $(this).data('filesuratsponsor');

            // alert(filesuratkampus)

            $("#tglMulaiLihat").attr("placeholder", tglmulai);
            $("#tglSelesaiLihat").attr("placeholder", tglselesai);
            $("#tglSuratKamLihat").attr("placeholder", tglsuratkampus);
            $("#tglSuratSponsorLihat").attr("placeholder", tglsuratsponsor);
            $("#noSuratSponsorLihat").attr("placeholder", nosponsor);
            $("#noSuratKamLihat").attr("placeholder", nokampus);
            $("#alasanCutiLihat").attr("placeholder", alasan);
            $("#filesuratkampuslihat").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratkampus"));
            $("#filesuratsponsorlihat").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratsponsor"));


            // $('#tglMulaiLihat').val(tglmulai).change();
            // $('#tglSelesaiLihat').val(tglselesai).change();
            // $('#tglSuratKamLihat').val(tglsuratkampus).change();
            // $('#tglSuratSponsorLihat').val(tglsuratsponsor).change();
            // $('#noSuratSponsorLihat').val(nosponsor);
            // $('#noSuratKamLihat').val(nokampus).change();
            // $('#alasanCutiLihat').val(alasan).change();

            $('#lihat_cuti').modal('show');
        });

        // button edit cuti
        $(".btnEditCuti").click(function(){

            var id = $(this).data('id')
            var tglmulai = $(this).data('tglmulai');
            var tglselesai = $(this).data('tglselesai');
            var alasan = $(this).data('alasan');
            var nokampus = $(this).data('nokampus');
            var tglsuratkampus = $(this).data('tglsuratkampus');
            var nosponsor = $(this).data('nosponsor');
            var tglsuratsponsor = $(this).data('tglsuratsponsor');

            $('#idCuti').val(id).change();
            $('#tglMulaiEdit').val(tglmulai).change();
            $('#tglSelesaiEdit').val(tglselesai).change();
            $('#tglSuratKamEdit').val(tglsuratkampus).change();
            $('#tglSuratSponsorEdit').val(tglsuratsponsor).change();
            $('#noSuratSponsorEdit').val(nosponsor);
            $('#noSuratKamEdit').val(nokampus).change();
            $('#alasanCutiEdit').val(alasan).change();

            $('#edit_cuti').modal('show');

        });

        // button hapus cuti
        $(".btnHapusCuti").click(function(){
            var id = $(this).data("id");
            var url = $(this).data("url");

            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(result.value);

                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {

                            if(response['status']===1){
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di hapus',
                                showConfirmButton: false,
                                timer: 1800
                                });

                                $('#cut'+id).remove();



                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });


                }
            })



        });

        // button kirim cuti
        $(".btnKirimCuti").click(function(){
            var act = $(this).data("act");
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengajukan cuti?',
                text: "Data akan masuk ke tahap berikutnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#40BA50',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: id,
                            keterangan : null
                        },
                        success: function(response) {
                            if(response.status===1){

                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di kirim',
                                showConfirmButton: false,
                                timer: 1600
                                });

                                location.reload();

                            }else{
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                                });

                            }

                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });

                }
            })
        });

        // button attachment lps
        $(".btnUploadLps").click(function(){
            $('#upload_file_lps').modal('show');

            // message upload
            $('#messageuploadkhs').hide();
            $('#messageuploadlps').hide();

            // message download file
            $('#messagefilekhs').hide();
            $('#messagefilelps').hide();

            var idlps = $(this).data('idlps')
            var urlkhs = $(this).data('urlkhs')
            var urllps = $(this).data('urllps')

            $('#uidlps').val(idlps).change();
            $('#unggahidlps').val(idlps).change();

            $("#urlfilekhs").attr("href", '{{ env('APP_URL') }}' + '/files/' + urlkhs);
            $("#urlfilelps").attr("href", '{{ env('APP_URL') }}' + '/files/' + urllps);

            // if(urlkhs == "") {
            //     alert('kosong')
            // }

            // download file khs
            // if((urlkhs !== 'null') && (urlkhs = "")) {
            if(urlkhs !== "") {
                $('#messagefilekhs').show();
            }
            // download file lps
            // if((urllps !== 'null') || (urllps = "")) {
            if((urllps !== "")) {
                $('#messagefilelps').show();
            }

        });

        // button lihat permohonan aktif kembali
        $(".btnLihatPermohonanAktif").click(function(){

            var tglmulaiaktif = $(this).data('tglmulaiaktif');
            var nosuratkembali = $(this).data('nosuratkembali');
            var tglsurat = $(this).data('tglsurat');

            // alert(filesuratkampus)

            $("#tglMulaiAktifLihat").attr("placeholder", tglmulaiaktif);
            $("#suratAktifKembaliLihat").attr("placeholder", nosuratkembali);
            $("#tglSuratPengaktifanLihat").attr("placeholder", tglsurat);
            $("#filesuratsponsorlihat").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("file"));

            $('#lihat_permohonan_aktif').modal('show');
        });

        // button edit permohonan aktif kembali
        $(".btnEditPermohonanAktif").click(function(){

            var id = $(this).data('idTubel');
            var tglmulaiaktif = $(this).data('tglmulaiaktif');
            var nosuratkembali = $(this).data('nosuratkembali');
            var tglsurat = $(this).data('tglsurat');

            // alert(filesuratkampus)

            $("#tglMulaiAktifEdit").attr("placeholder", tglmulaiaktif);
            $("#suratAktifKembaliEdit").attr("placeholder", nosuratkembali);
            $("#tglSuratPengaktifanEdit").attr("placeholder", tglsurat);
            $("#filesuratsponsorEdit").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("file"));

            $('#edit_permohonan_aktif').modal('show');
        });

        // button kirim permohonan aktif kembali
        $(".btnKirimPermohonanAktif").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengajukan permohonan aktif kembali?',
                text: "Data akan masuk ke tahap berikutnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#40BA50',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/administrasi/permohonanaktifkirim',
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: id,
                            keterangan : null
                        },
                        success: function(response) {
                            if(response.status===1){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di kirim',
                                    showConfirmButton: false,
                                    timer: 1600
                                });
                                location.reload();
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1600
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        // button hapus permohonan aktif kembali
        $(".btnHapusPermohonanAktif").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {
                            if(response['status']===1){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                                $('#cut'+id).remove();
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1800
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        // upload file khs dan lps
        $(document).ready(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // upload khs - selesai
            $('#uploadkhs').submit(function(e) {
                e.preventDefault();
                BtnLoading($("#btnUploadKhs"));

                var formData = new FormData(this);

                var uidlps = $('#uidlps').val();

                $.ajax({
                    type:'POST',
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/uploadkhs',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        if(response.status === 0) {
                            // this.reset();
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Error!',
                                text: "Message: "+ response.message,
                                showConfirmButton: false,
                                timer: 2000
                            })
                        } else {
                            var nmFileKhs = response;
                            $.ajax({
                                type: 'patch',
                                url: '{{ env('APP_URL') }}' + '/tubel/administrasi/uploadkhs',
                                data: {
                                    id: uidlps,
                                    file: nmFileKhs
                                },
                                success: function(response) {
                                    if(response.status == 1) {
                                        Swal.fire({
                                            position: 'center',
                                            icon: 'success',
                                            title: 'Berhasil',
                                            text: 'File berhasil diupload',
                                            showConfirmButton: false,
                                            timer: 2000
                                        }),

                                        document.getElementById('messageuploadkhs').style.display = '';
                                        document.getElementById('messagefilekhs').style.display = '';

                                        var nmFileKhsBaru = '{{ env('APP_URL') }}' + '/files/' + nmFileKhs;

                                        document.getElementById("urlfilekhs").href=nmFileKhsBaru;

                                    }
                                }
                            })

                        }
                        BtnReset($("#btnUploadKhs"));
                    },
                    error: function(data){
                        BtnReset($("#btnUploadKhs"));
                        console.log(data);
                    }
                });
            });

            // upload laporan - selesai
            $('#uploadlps').submit(function(e) {
                e.preventDefault();
                BtnLoading($("#btnUploadLps"));

                var formData = new FormData(this);

                var unggahidlps = $('#unggahidlps').val();

                $.ajax({
                    type:'POST',
                    url: '{{ env('APP_URL') }}' + '/tubel/administrasi/uploadlps',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        if(response.status === 0) {
                            this.reset();
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Error!',
                                text: "Message: "+ response.message,
                                showConfirmButton: false,
                                timer: 2000
                            })
                        } else {
                            var nmFileLps = response;
                            $.ajax({
                                type: 'patch',
                                url: '{{ env('APP_URL') }}' + '/tubel/administrasi/uploadlps',
                                data: {
                                    id: unggahidlps,
                                    file: nmFileLps
                                },
                                success: function(response) {
                                    if(response.status == 1) {
                                        Swal.fire({
                                            position: 'center',
                                            icon: 'success',
                                            title: 'Berhasil',
                                            text: 'File berhasil diupload',
                                            showConfirmButton: false,
                                            timer: 2000
                                        }),

                                        document.getElementById('messageuploadlps').style.display = '';
                                        document.getElementById('messagefilelps').style.display = '';

                                        var nmFileLpsBaru = '{{ env('APP_URL') }}' + '/files/' + nmFileLps;

                                        document.getElementById("urlfilelps").href=nmFileLpsBaru;
                                    }
                                }
                            })
                        }
                        BtnReset($("#btnUploadLps"));
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('<i class="spinner-border spinner-border-sm"></i>  Loading');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }

        // Validation & masking

        // modal form add laporan perkembangan studi
        $( "#formAddLps" ).validate({
            rules: {
                semesterke: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                tahun: {
                    required: true,
                    digits: true,
                    minlength: 4,
                    maxlength: 4
                },
                ipk: {
                    required: true,
                    number: true,
                },
                jumlah_sks: {
                    required: true,
                    digits: true,
                    maxlength: 3
                },
            }
        });

        // modal form edit laporan perkembangan studi
        $( "#formEditLps" ).validate({
            rules: {
                esemesterke: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                etahun: {
                    required: true,
                    digits: true,
                    minlength: 4,
                    maxlength: 4
                },
                eipk: {
                    required: true,
                    number: true,
                },
                ejumlah_sks: {
                    required: true,
                    digits: true,
                    maxlength: 3
                },
            }
        });

        Inputmask({
            "mask" : "9999"
        }).mask("#thakademik");

        Inputmask({
            "mask" : "9.99"
        }).mask("#ipk");

        Inputmask({
            "mask" : "9999"
        }).mask("#etahun");

        Inputmask({
            "mask" : "9.99"
        }).mask("#eipk");


    </script>
@endsection
