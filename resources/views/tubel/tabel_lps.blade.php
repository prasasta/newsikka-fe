
<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <!--begin::Table-->
    <table class="table table-striped align-middle">
        <!--begin::Table head-->
        <thead>
            <tr class="fw-bolder bg-secondary ">
                <th class="ps-4 min-w-150px rounded-start">Semester</th>
                <th class="min-w-150px">IPK / Jumlah SKS</th>
                <th class="min-w-250px">Perguruan Tinggi / Program Studi</th>
                <th class="min-w-100px">Attachment</th>
                <th class="min-w-100px text-center">Status</th>
                <th class="min-w-100px text-center rounded-end">Aksi</th>
            </tr>
        </thead>
        <!--end::Table head-->
        <!--begin::Table body-->
        <tbody>
            @php
                //dd($lps)
            @endphp
            @if ($lps != null)
                @foreach ($lps as $l)
                    <tr id="lid{{ $l->id }}"  class="">

                        <td class="ps-4 text-dark  mb-1 fs-6">
                            Semester {{$l->semesterKe}}
                            <br>
                            @if ($l->flagSemester == 1)
                                Ganjil
                            @elseif ($l->flagSemester == 2)
                                Genap
                            @endif
                        </td>
                        <td class="text-dark  mb-1 fs-6">
                            {{$l->ipk}} <br> {{$l->jumlahSks}} SKS
                        </td>
                        <td class="text-dark  mb-1 fs-6">
                            {{$l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }} <br> {{$l->ptPesertaTubelId->programStudiId->programStudi}}
                        </td>

                        <td class="fs-6">
                            {{-- <a href=""><i class="fa fa-file"></i> Laporan Hasil Studi.pdf</a> --}}
                            @if ($l->logStatusProbis->output !== 'LPS dikirim')
                                <button class="btn btn-sm btn-primary2 btnUploadLps" data-idlps="{{ $l->id }}" data-urlkhs="{{ $l->pathKhs }}" data-urllps="{{ $l->pathLps }}">
                                    <i class="fa fa-upload"></i> Attachment
                                </button>
                            @else
                            <div class="col-sm">
                                <a href="{{ $l->pathLps ? url('files/' . $l->pathLps) : "#" }}"><i class="fa fa-file-alt text-hover-primary"></i> Laporan Perkembangan Studi</a>
                            </div>
                            <div class="col-sm">
                                <a href="{{ $l->pathKhs ? url('files/' . $l->pathKhs) : "#" }}"><i class="fa fa-file-alt text-hover-primary"></i> Kartu Hasil Studi.pdf</a>
                            </div>
                            @endif



                        </td>
                        <td class="text-center text-dark  mb-1 fs-6">
                            @if ($l->logStatusProbis->outputId->id=='27741cda-2ddf-4b98-8cfb-f9ba4ce3c9bd')
                                <span class="badge badge-light-danger fs-7 fw-bolder">{{$l->logStatusProbis->output}}</span>
                                <br>
                                Tiket: {{ $l->logStatusProbis->nomorTiket }}
                            @else
                                <span class="badge badge-light-success fs-7 fw-bolder">{{$l->logStatusProbis->output}}</span>
                                <br>
                                Tiket: {{ $l->logStatusProbis->nomorTiket }}
                            @endif

                        </td>
                        <td class="text-center">

                            <button class="btn btn-sm btn-icon btn-info btnLihatLps" data-id="{{ $l->id }}" data-pt="{{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" data-smtke="{{ $l->semesterKe }}" data-smt="{{ $l->flagSemester }}" data-th="{{ $l->tahunAkademik }}" data-ipk="{{ $l->ipk }}" data-sks="{{ $l->jumlahSks }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat">
                                <i class="fa fa-eye"></i>
                            </button>

                            @if (isset($act))
                                @if ($act==true)

                                    @if (($l->logStatusProbis->output == 'Draft LPS dibuat') || ($l->logStatusProbis->output == 'LPS dikembalikan'))
                                        <button class="btn btn-sm btn-icon btn-warning btnEditLps" data-id="{{ $l->id }}" data-uidpt="{{ $l->ptPesertaTubelId->id }}" data-idpt="{{ $l->ptPesertaTubelId->perguruanTinggiId->id }}" data-pttubel="{{ $l->ptPesertaTubelId->id }}" data-pt="{{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" data-smtke="{{ $l->semesterKe }}" data-smt="{{ $l->flagSemester }}" data-th="{{ $l->tahunAkademik }}" data-ipk="{{ $l->ipk }}" data-sks="{{ $l->jumlahSks }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </button>

                                        <button class="btn btn-sm btn-icon btn-danger btnHapusLps" data-id="{{$l->id}}" data-url="{{url('tubel/administrasi/'.$l->id.'/lps')}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        <button class="btn btn-sm btn-primary btnKirimLps" data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim" data-id="{{ $l->id }}" data-outputid="{{ $l->logStatusProbis->outputId->id }}" data-output="{{ $l->logStatusProbis->outputId->caseOutput }}">
                                            <i class="fa fa-paper-plane"></i> Kirim
                                        </button>
                                    {{-- @else
                                        <button class="btn btn-sm btn-icon btn-info btnLihatLps" data-bs-toggle="modal" data-bs-target="#get_laporan_perkembangan_studi" data-id="{{ $l->id }}" data-pt="{{ $l->ptPesertaTubelId->perguruanTinggiId->namaPerguruanTinggi }}" data-smtke="{{ $l->semesterKe }}" data-smt="{{ $l->flagSemester }}" data-th="{{ $l->tahunAkademik }}" data-ipk="{{ $l->ipk }}" data-sks="{{ $l->jumlahSks }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat">
                                            <i class="fa fa-eye"></i>
                                        </button> --}}

                                    @endif

                                @endif

                            @endif

                        </td>
                    </tr>
                @endforeach
            @else
            <tr class="text-center">
                <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
            </tr>
            @endif
        </tbody>
        <!--end::Table body-->
    </table>
    <!--end::Table-->
</div>
