
<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabelPengaktifan">
        <thead class="fw-bolder bg-secondary fs-6">
            <tr >
                <th class="ps-4 min-w-200px rounded-start">Tanggal Aktif</th>
                <th class="min-w-200px">Surat Aktif Kembali</th>
                <th class="text-center min-w-100px">Status</th>
                <th class="text-center min-w-100px rounded-end">Aksi</th>
            </tr>
        </thead>

        <tbody id="bodyPengaktifan">
            @if ($permohonan_aktif != null)
                @foreach ($permohonan_aktif as $p)
                    <tr class="" id="cut{{$p->id}}">
                        <td class="ps-3 text-dark fw-bolder   mb-1 fs-6">
                            {{$p->nomorSuratKampus}}
                        </td>
                        <td class="text-dark fw-bolder   mb-1 fs-6">
                            {{$p->nomorSuratSponsor}}
                            <br>
                            <span class="text-muted fw-bold text-muted  fs-6">{{AppHelper::instance()->indonesian_date($p->tglSuratKepSponsor,'j F Y','')}}</span>
                        </td>
                        <td class="text-dark   mb-1 fs-6">
                            {{AppHelper::instance()->indonesian_date($p->tglMulaiCuti,'j F Y','')}}
                            <span class="text-muted fw-bold text-muted d-block fs-6">s.d</span>
                            {{AppHelper::instance()->indonesian_date($p->tglSelesaiCuti,'j F Y','')}}
                        </td>
                        <td class="text-center text-dark fw-bolder  mb-1 fs-6">
                            <span class="badge badge-light-success fs-7 fw-bolder">{{$p->logStatusProbis->output}}</span>
                            <br>
                            {{ $l->logStatusProbis->nomorTiket }}
                        </td>
                        <td class="text-center">

                            @if (isset($act))
                                @if ($act==true)

                                    @if ($p->logStatusProbis->outputId->caseId == '14c758d3-8392-40e2-a2f1-0abc23fc1261')

                                        <button  class="btn btn-sm btn-icon btn-primary btnKirimPermohonanAktif" data-id="{{$p->id}}" data-url="{{url('tubel/updatestatuscuti')}}" data-act="2edff958-c7cf-4e66-9627-8c7521ed5931" data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                            <i class="fa fa-paper-plane"></i>
                                        </button>
                                        <button  class="btn btn-sm btn-icon btn-warning btnEditPermohonanAktif" data-id="{{$p->id}}" data-tglmulai="{{$p->tglMulaiCuti}}" data-tglselesai="{{$p->tglSelesaiCuti}}" data-alasan="{{$p->alasanCuti}}" data-nokampus="{{$p->nomorSuratKampus}}" data-tglsuratkampus="{{$p->tglSuratKampus}}" data-nosponsor="{{$p->nomorSuratSponsor}}" data-tglsuratsponsor="{{$p->tglSuratKepSponsor}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-sm btn-icon btn-danger btnHapusPermohonanAktif"data-id="{{$p->id}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                    @else

                                        <a href="#" class="btn btn-sm btn-icon btn-primary btnDetail" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"><i class="fa fa-eye"></i></a>

                                    @endif

                                @endif
                            @else

                                <button class="btn btn-sm btn-icon btn-primary btnLihatPermohonanAktif" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat">
                                    <i class="fa fa-eye"></i>
                                </button>

                            @endif

                        </td>
                    </tr>
                @endforeach

            @else

                <tr class="text-center">
                    <td class="text-dark mb-1 fs-6" colspan="4">Tidak terdapat data</td>
                </tr>

            @endif

        </tbody>
    </table>
</div>
