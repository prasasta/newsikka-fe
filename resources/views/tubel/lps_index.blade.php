@extends('layout.master')

@section('content')
@php
    //dd($tubel->data)
@endphp
    <div class="post d-flex flex-column" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="col-lg-12">
                <div class="card border-warning">
                    <div class="card-header card-header-stretch" id="card-header">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder" id="tabLps">
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary <?php if ($tab=='tunggu'){ echo 'active';} ?>"  href="{{ url('tubel/lps') }}"> Menunggu Persetujuan </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary <?php if ($tab=='setuju'){ echo 'active';} ?>"  href="{{ url('tubel/lpssetuju') }}"> Telah Disetujui </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary <?php if ($tab=='tolak'){ echo 'active';} ?>"  href="{{ url('tubel/lpstolak') }}"> Ditolak </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a class="nav-link text-active-primary <?php if ($tab=='kirim'){ echo 'active';} ?>" href="{{ url('tubel/lpskirim') }}"> Telah Dikirim ke Apmedik </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary <?php if ($tab=='dashboard'){ echo 'active';} ?>" href="{{ url('tubel/lpsdashboard') }}"> Dashboard </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade <?php if ($tab=='tunggu'){ echo 'active show';} ?> " id="tunggu" role="tabpanel">
                                @if ($tab=='tunggu')
                                @include('tubel.tabel_tunggu')
                                @endif
                            </div>

                            <div class="tab-pane fade <?php if ($tab=='setuju'){ echo 'active show';} ?> " id="setuju" role="tabpanel">
                                @if ($tab=='setuju')
                                @include('tubel.tabel_tunggu')
                                @endif
                            </div>

                            <div class="tab-pane fade <?php if ($tab=='tolak'){ echo 'active show';} ?> " id="tolak" role="tabpanel">
                                @if ($tab=='tolak')
                                @include('tubel.tabel_tunggu')
                                @endif
                            </div>

                            {{-- <div class="tab-pane fade <?php if ($tab=='kirim'){ echo 'active show';} ?> " id="kirim" role="tabpanel">
                                @if ($tab=='kirim')
                                @include('tubel.tabel_setuju')
                                @endif
                            </div> --}}

                            <div class="tab-pane fade <?php if ($tab=='dashboard'){ echo 'active show';} ?> " id="dashboard" role="tabpanel">
                                @if ($tab=='dashboard')
                                @include('tubel.tabel_setuju')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal --}}
    <div class="modal fade" id="periksa_lps" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penelitian Cuti Belajar</h4>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>
                <form class="form" id="formAddCuti" method="post" action="#">
                        <input type="hidden" name="idTubel" value="">

                        <input type="hidden" name="idCuti" value="">

                        <div class="modal-body scroll-y px-5 px-lg-10">
                            <div class="rounded border p-4 d-flex flex-column mb-6">
                                <div class="row mb-3">
                                        <div class="col-md col-lg">

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nama</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jenjang Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: aSs </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Program Beasiswa</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lokasi</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lama Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Total Semester</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Kantor Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jabatan Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Surat Tugas</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Mulai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Selesai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor KEP Pembebasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">TMT</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Email Non Kedinasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Handphone</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Alamat</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>



                            <div class="rounded border p-4 d-flex flex-column mb-6">
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_cuti" type="text" name="alasan_cuti" rows="4" placeholder="" required disabled></textarea>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tglMulai" placeholder="" name="tglMulaiSt" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tglSelesai" placeholder="" name="tglSelesaiSt" type="text" value=""  disabled>
                                        </div>
                                    </div>

                                </div>

                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti" rows="4" id="surat_izin_cuti_kampus" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tglSuratKampus" placeholder="" name="tglSurat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href=""><i class="fa fa-file"></i>  Surat Izin Cuti (Kampus).pdf</a>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_sponsor" id="surat_izin_cuti_sponsor" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor" placeholder="" name="tglSuratSponsor" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href=""><i class="fa fa-file"></i>  Surat Izin Cuti (Sponsor).pdf</a>
                                    </div>
                                </div>

                            </div>

                            <div class="text-center mb-8">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class=""><<</i>Batal</button>
                                <button type="button" id="btnTolakCuti" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>Tolak</button>
                                <button type="button" id="btnSetujuCuti" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i>Setuju</button>
                            </div>

                        </div>

                    {{-- <div class="modal-footer">

                    </div> --}}
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
            $(".btnTeliti").click(function() {
                // alert('ini')
                $("#periksa_lps").modal('show');
                // alert($(this).data("tglmulai"));
                // $("#alasan_cuti").attr("placeholder", $(this).data("alasancuti"));
                // $("#tglMulai").attr("placeholder", $(this).data("tglmulai"));
                // $("#tglSelesai").attr("placeholder", $(this).data("tglselesai"));
                // $("#tglSurat").attr("placeholder", $(this).data("tglsurat"));
                // $("#surat_izin_cuti_kampus").attr("placeholder", $(this).data("suratkampus"));
                // $("#surat_izin_cuti_sponsor").attr("placeholder", $(this).data("suratsponsor"));
                // $("#tglSuratSponsor").attr("placeholder", $(this).data("tglsponsor"));
                // $("#tglSuratKampus").attr("placeholder", $(this).data("tglkampus"));
                // $("#periksa_cuti").modal('show');
                // $.fn.modal.Constructor.prototype._enforceFocus = function() {};

            });
    </script>
@endsection


