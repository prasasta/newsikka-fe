
<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <!--begin::Table-->
    <table class="table table-striped align-middle" id="tabelCuti">
        <!--begin::Table head-->
        <thead class="fw-bolder bg-secondary fs-6">
            <tr >
                <th class="ps-4 min-w-200px rounded-start">Surat Izin dari PT</th>
                <th class="min-w-200px">Surat izin dari Sponsor</th>
                <th class="min-w-200px">Mulai / Selesai</th>
                <th class="text-center min-w-100px">Status</th>
                <th class="text-center min-w-100px rounded-end">Aksi</th>
            </tr>
        </thead>
        <!--end::Table head-->
        <!--begin::Table body-->


        <tbody id="bodyCuti">
            @if ($cuti != null)
            @foreach ($cuti as $c)
            <tr class="" id="cut{{$c->id}}">
                <td class="ps-3 text-dark fw-bolder   mb-1 fs-6">
                    {{$c->nomorSuratKampus}}
                    <br>
                    <span class="text-muted fw-bold text-muted  fs-6">{{AppHelper::instance()->indonesian_date($c->tglSuratKampus,'j F Y','')}}</span>
                </td>
                <td class="text-dark fw-bolder   mb-1 fs-6">
                    {{$c->nomorSuratSponsor}}
                    <br>
                    <span class="text-muted fw-bold text-muted  fs-6">{{AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor,'j F Y','')}}</span>
                </td>
                <td class="text-dark   mb-1 fs-6">
                    {{AppHelper::instance()->indonesian_date($c->tglMulaiCuti,'j F Y','')}}
                    <span class="text-muted fw-bold text-muted d-block fs-6">s.d.</span>
                    {{AppHelper::instance()->indonesian_date($c->tglSelesaiCuti,'j F Y','')}}
                </td>
                <td class="text-center text-dark fw-bolder  mb-1 fs-6">
                    <span class="badge badge-light-success fs-7 fw-bolder">{{$c->logStatusProbis->output}}</span>
                </td>
                <td class="text-center">

                    @if (isset($act))
                        @if ($act==true)
                        {{-- @if (($l->logStatusProbis->output == 'Draft LPS dibuat') || ($l->logStatusProbis->output == 'LPS dikembalikan')) --}}
                        {{-- @if (($l->logStatusProbis->output == 'Draft LPS dibuat') || ($l->logStatusProbis->output == 'LPS dikembalikan')) --}}
                        @if ($c->logStatusProbis->output == 'Permohonan cuti dikembalikan')

                                <button  class="btn btn-sm btn-icon btn-primary btnKirimCuti" data-id="{{$c->id}}" data-url="{{url('tubel/updatestatuscuti')}}" data-act="2edff958-c7cf-4e66-9627-8c7521ed5931" data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                    <i class="fa fa-paper-plane"></i>
                                </button>

                                <button  class="btn btn-sm btn-icon btn-warning btnEditCuti" data-id="{{$c->id}}" data-tglmulai="{{$c->tglMulaiCuti}}" data-tglselesai="{{$c->tglSelesaiCuti}}" data-alasan="{{$c->alasanCuti}}" data-nokampus="{{$c->nomorSuratKampus}}" data-tglsuratkampus="{{$c->tglSuratKampus}}" data-nosponsor="{{$c->nomorSuratSponsor}}" data-tglsuratsponsor="{{$c->tglSuratKepSponsor}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </button>

                                <button data-id="{{$c->id}}" data-url="{{url('tubel/administrasi/hapuscuti')}}" class="btn btn-sm btn-icon btn-danger btnHapusCuti" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>
                            @else
                                <button class="btn btn-sm btn-icon btn-info btnLihatCuti" data-id="{{$c->id}}" data-tglmulai="{{$c->tglMulaiCuti}}" data-tglselesai="{{$c->tglSelesaiCuti}}" data-alasan="{{$c->alasanCuti}}" data-nokampus="{{$c->nomorSuratKampus}}" data-tglsuratkampus="{{$c->tglSuratKampus}}" data-nosponsor="{{$c->nomorSuratSponsor}}" data-tglsuratsponsor="{{$c->tglSuratKepSponsor}}" data-filesuratsponsor="{{$c->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$c->pathSuratIjinCutiKampus}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"><i class="fa fa-eye"></i></a>
                            @endif

                        @endif
                     @else
                        <button class="btn btn-sm btn-icon btn-info btnLihatCuti" data-id="{{$c->id}}" data-tglmulai="{{$c->tglMulaiCuti}}" data-tglselesai="{{$c->tglSelesaiCuti}}" data-alasan="{{$c->alasanCuti}}" data-nokampus="{{$c->nomorSuratKampus}}" data-tglsuratkampus="{{$c->tglSuratKampus}}" data-nosponsor="{{$c->nomorSuratSponsor}}" data-tglsuratsponsor="{{$c->tglSuratKepSponsor}}" data-filesuratsponsor="{{$c->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$c->pathSuratIjinCutiKampus}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat"><i class="fa fa-eye"></i></button>
                    @endif

                </td>
            </tr>


        @endforeach

        @else
        <tr class="text-center">
            <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
        </tr>

        @endif

        </tbody>
        <!--end::Table body-->
    </table>
    <!--end::Table-->
</div>
