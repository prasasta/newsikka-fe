@extends('layout.master')

@section('content')
@php
    //dd($tubel->data)
@endphp
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card mb-3 border-warning shadow-sm">
                        <div id="card-header" class="card-header">
                            <div class="card-title fw-bold fs-2 text-white">
                                Monitoring Pegawai Tugas Belajar
                                {{-- <span class="card-label fw-bold fs-2 mb-1 text-white">Administrasi Pegawai Tugas Belajar</span> --}}
                            </div>

                        </div>
                        {{-- End Card Header --}}
                        <div class="card-body">
                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <!--begin::Table-->
                                <table class="table table-striped align-middle" id="tabel_tubel">
                                    <!--begin::Thead-->
                                    <thead class="fw-bolder bg-secondary fs-6">
                                        <tr class="text-right">
                                            <th class="text-center rounded-start">&nbsp; No</th>
                                            <th class="">Jenjang Pendidikan</th>
                                            <th class="">Data Pegawai</th>
                                            <th class="">Program Beasiswa / Lokasi</th>
                                            <th class="">Perguruan Tinggi / Program Studi</th>
                                            <th class="">Lama Pendidikan / Total Semester</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center rounded-end">Aksi</th>
                                        </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->
                                    <tbody>
                                    @if ($tubel->data != null)
                                    @php
                                        $no = 1;
                                    @endphp
                                        @foreach ($tubel->data as $t)
                                        <tr class="text-right">
                                            <td class="text-center text-dark  mb-1 fs-6">
                                                {{$no++}}
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->jenjangPendidikanId ? $t->jenjangPendidikanId->jenjangPendidikan : null }}
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->namaPegawai }} <br>
                                                {{ $t->namaPangkat }} <br>
                                                {{ $t->nip18 }}
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->programBeasiswa }}
                                            </td>

                                            <td class="text-dark  mb-1 fs-6">

                                            </td>
                                            <td class="text-dark  mb-1 fs-6">
                                                {{ $t->lamaPendidikan && $t->totalSemester ? $t->lamaPendidikan . ' Tahun, ' . $t->totalSemester . ' Semester' : '' }}
                                            </td>

                                            <td class="text-center text-dark  mb-1 fs-6">
                                                @php
                                                    $x =  strtolower($t->logStatusProbis->caseId->namaCase);
                                                    $status = ucwords($x);
                                                @endphp

                                                <span class="badge badge-light-success fs-7 fw-bolder">{{$status}}</span>

                                            </td>

                                            <td class="text-center text-dark  mb-1 fs-6">
                                                <a href="{{ url('tubel/pegawai/'. $t->id.'?url='.$route) }}"
                                                    class="btn btn-sm btn-primary m-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail">
                                                    <i class="fa fa-eye"></i>Detail
                                                </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                            <tr class="text-center">
                                                <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
                                            </tr>
                                    @endif

                                    </tbody>
                                    <!--end::Tbody-->
                                </table>

                                <div class="d-flex flex-stack flex-wrap pt-5 p-3">
									<div class="fs-6 fw-bold text-gray-700"></div>
									<!--begin::Pages-->
									<ul class="pagination">
										<li class="page-item previous">
											<a href="#" class="page-link">
												<i class="previous"></i>
											</a>
										</li>
										<li class="page-item active">
											<a href="#" class="page-link">1</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">2</a>
										</li>
										<li class="page-item next">
											<a href="#" class="page-link">
												<i class="next"></i>
											</a>
										</li>
									</ul>
									<!--end::Pages-->
								</div>



                                <!--end::Table-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--end::Container-->
    </div>

@endsection


