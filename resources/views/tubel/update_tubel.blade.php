@extends('layout.master')

@section('content')
<div class="post d-flex flex-column" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card border-warning shadow-sm">
                    <div id="card-header" class="card-header border-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold fs-2 mb-1 text-white">Data Induk Pegawai Tugas Belajar</span>
                        </h3>
                    </div>
                    {{-- End Card Header --}}
                    <form id="formTubel" action="{{ route('tubel.update', $tubel->id) }}" method="post">
                        @csrf
                        {{ method_field('patch') }}
                        <div class="card-body">
                            <input type="text" name="pegawaiInputId" hidden value="{{ session()->get('user')['pegawaiId'] }}">
                            <input type="text" name="kantorIdAsal" hidden value="{{ session()->get('user')['kantorId'] }}">
                            <input type="text" name="namaKantorAsal" hidden value="{{ session()->get('user')['kantor'] }}">
                            <input type="text" name="jabatanIdAsal" hidden value="{{ session()->get('user')['jabatanId'] }}">
                            <input type="text" name="namaJabatanAsal" hidden value="{{ session()->get('user')['jabatan'] }}">
                            <input type="text" name="programBeasiswa" hidden value="{{ $tubel->programBeasiswa }}">
                            <div class="bg-light rounded border d-flex flex-column p-4 fs-4">
                                <div class="d-flex flex-wrap">
                                    <div class="flex-equal">
                                        <table class="table table-flush fw-bold gy-1">
                                            <tbody>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">Nama Pegawai</td>
                                                    <td class="text-gray-600">: {{ $tubel->namaPegawai }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">NIP 18</td>
                                                    <td class="text-gray-600">: {{ $tubel->nip18 }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">Pangkat</td>
                                                    <td class="text-gray-600">:  {{ $tubel->namaPangkat }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="flex-equal">
                                        <table class="table table-flush fw-bold gy-1 ">
                                            <tbody>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">Jabatan</td>
                                                    <td class="text-gray-600">: {{ $tubel->namaJabatan }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">Unit Organisasi</td>
                                                    <td class="text-gray-600">: {{ $tubel->namaUnitOrg }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-gray-600 min-w-125px w-200px">Kantor
                                                    </td>
                                                    <td class="text-gray-600">: {{ $tubel->namaKantor }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body m-5 mt-0">
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Program Beasiswa</label>
                                <div class="col-lg">
                                    <input type="text" class="form-control form-control-solid" disabled value="{{ $tubel->programBeasiswa }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Jenjang Pendidikan</label>
                                <div class="col-lg-3">
                                    <select id="JenjangPendidikan[]" name="jenjangPendidikanId" data-control="select2" type="text" class="form-select form-select-lg" required>
                                        {{-- <option value="{{ $j->id }}" {{ ($j->id == $tubel->jenjangPendidikanId->id) ? 'selected' : "" }}>{{ $j->jenjangPendidikan }}</option> --}}
                                        @foreach ($jenjang as $j)
                                            <option value="{{ $j->id }}" {{ ($j->id == $tubel->jenjangPendidikanId->id) ? 'selected' : "" }}>{{ $j->jenjangPendidikan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Lokasi Pendidikan</label>
                                <div class="col-lg-3">
                                    <select id="lokasiPendidikan[]" name="lokasiPendidikanId" data-control="select2" type="text" class="form-select form-select-lg" required>
                                        @if ($tubel->lokasiPendidikanId == null)
                                            @foreach ($lokasi as $l)
                                                <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($lokasi as $l)
                                                <option value="{{ $l->id }}" {{ ($l->id == $tubel->lokasiPendidikanId->id) ? 'selected' : "" }} >{{ $l->lokasi }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Lama Pendidikan</label>
                                <div class="col-lg-3">
                                    <input type="text" name="lamaPendidikan" class="form-control" placeholder="hanya angka" value="{{ $tubel->lamaPendidikan }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Total Semester</label>
                                <div class="col-lg-3">
                                    <input type="text" name="totalSemester" class="form-control" placeholder="hanya angka" value="{{ $tubel->totalSemester }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Nomor ST & Tgl ST</label>
                                <div class="col-lg-3">
                                    <input type="text" name="nomorSt" class="form-control" placeholder="ST-.." value="{{ $tubel->nomorSt }}" required>
                                </div>
                                <div class="col-lg-3">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSt" placeholder="Pilih tanggal " name="tglSt" type="text" value="{{ $tubel->tglSt }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Tgl mulai dan selesai ST</label>
                                <div class="col-lg-3">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglMulaiSt" placeholder="Pilih tanggal " name="tglMulaiSt" type="text" value="{{ $tubel->tglMulaiSt }}" required>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSelesaiSt" placeholder="Pilih tanggal " name="tglSelesaiSt" type="text" value="{{ $tubel->tglSelesaiSt }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Nomor & Tgl KEP Pembebasan</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" name="nomorKepPembebasan" placeholder="misal: KEP-" value="{{ $tubel->nomorKepPembebasan }}" required>
                                </div>
                                <div class="col-lg-3">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglKepPembebasan" placeholder="Pilih tanggal " name="tglKepPembebasan" type="text" value="{{ $tubel->tglKepPembebasan }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">TMT KEP Pembebasan</label>
                                <div class="col-lg-3">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tmtKepPembebasan" placeholder="Pilih tanggal " name="tmtKepPembebasan" type="text" value="{{ $tubel->tmtKepPembebasan }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Email Non Kedinasan</label>
                                <div class="col-lg-3">
                                    <input type="email" class="form-control" name="emailNonPajak" placeholder="Selain email pajak " value="{{ $tubel->emailNonPajak }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Nomor Handphone</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" name="noHandphone" placeholder="misal: 08XXXXXXXXX" value="{{ $tubel->noHandphone }}" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label required fw-bold fs-6">Alamat</label>
                                <div class="col-lg">
                                    <textarea type="text" class="form-control" name="alamatTinggal" rows="4" placeholder="Alamat tempat tinggal " required >{{ $tubel->alamatTinggal }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-center">
                                <a href="{{ url()->previous() }}" class="btn btn-sm btn-secondary my-1"><i class="fa fa-reply"></i>Batal</a>
                                <button type="submit" class="btn btn-sm btn-success my-1">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Container-->
</div>

@endsection

@section('js')
    <script>
        $("#tglSt").flatpickr();
        $("#tglMulaiSt").flatpickr();
        $("#tglSelesaiSt").flatpickr();
        $("#tglKepPembebasan").flatpickr();
        $("#tmtKepPembebasan").flatpickr();

        $( "#formTubel" ).validate({
            rules: {
                lamaPendidikan: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                totalSemester: {
                    required: true,
                    digits: true,
                    maxlength: 2
                },
                emailNonPajak: {
                    required: true,
                    email: true
                },
                noHandphone: {
                    required: true,
                    digits: true
                }
            }
        });

    </script>
@endsection
