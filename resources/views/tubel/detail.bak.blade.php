@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">

            <div class="card border-warning">
                <div class="card-header" id="card-header">
                    <h3 class="card-title">
                        <span class="fw-bold fs-2 text-white">Administrasi Pegawai Tugas Belajar</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md row mb-3">
                        <div class="col">
                            <a href="{{ url('tubel/administrasi') }}" class="btn btn-sm btn-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
                        </div>
                        <div class="col text-end">
                            <a href="{{ $tubel->id. '/update' }}" class="btn btn-sm btn-primary fs-6">
                                <i class="fa fa-plus"></i> Perbarui Data
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="rounded border p-4 d-flex flex-column mb-6">
                            <div class="row mb-3">
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Jenjang Pendidikan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->jenjangPendidikanId ? $tubel->jenjangPendidikanId->jenjangPendidikan : '#N/A' }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Program Beasiswa</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->programBeasiswa ? $tubel->programBeasiswa : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Kampus Stan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->flagStan == 0 ? 'Tidak' : 'Ya' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Lokasi</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lokasiPendidikanId ? $tubel->lokasiPendidikanId->lokasi : '#N/A'  }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Lama Pendidikan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->lamaPendidikan ? $tubel->lamaPendidikan . ' Tahun' : '#N/A'}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Total Semester</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->totalSemester ? $tubel->totalSemester . ' Semester': '#N/A' }}</span>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <br>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Kantor Asal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaKantor ? $tubel->namaKantor : '#N/A'  }}
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Jabatan Asal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->namaJabatan ? $tubel->namaJabatan : '#N/A'  }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor Surat Tugas</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorSt ? $tubel->nomorSt : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Mulai</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglMulaiSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal Selesai</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglSelesaiSt,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <br>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor KEP Pembebasan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->nomorKepPembebasan ? $tubel->nomorKepPembebasan : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Tanggal</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tglKepPembebasan,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">TMT</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ AppHelper::instance()->indonesian_date($tubel->tmtKepPembebasan,'j F Y','') }} </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md col-lg">
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Email Non Kedinasan</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->emailNonPajak ? $tubel->emailNonPajak : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Nomor Handphone</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->noHandphone ? $tubel->noHandphone : '#N/A' }} </span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label class="col-lg-5 col-md fw-bold fs-6">Alamat</label>
                                            <div class="col-lg d-flex align-items-center">
                                                <span class="fw-bolder fs-6 text-gray-600 me-2">: {{ $tubel->alamatTinggal ? $tubel->alamatTinggal : '#N/A' }} </span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <div class="text-end mb-4">
                                        <a href="{{ $tubel->id. '/update' }}" class="btn btn-sm btn-primary fs-6">Perbarui Data</a>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="rounded border p-4 d-flex flex-column mb-8">
                            <div class="col-lg row mb-6">
                                <div class="col-lg text-end">
                                    <button data-bs-toggle="modal" data-bs-target="#add_perguruan_tinggi"  class="btn btn-sm btn-primary fs-6">
                                        <i class="fa fa-plus"></i> Tambah Perguruan Tinggi
                                    </button>
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <table class="table align-middle">
                                    <thead>
                                        <tr class="fw-bolder bg-secondary">
                                            <th class="ps-4 min-w-200px rounded-start">Nama Perguruan Tinggi</th>
                                            <th class="min-w-125px">Program Studi</th>
                                            <th class="min-w-125px">Negara</th>
                                            <th class="min-w-150px">Tanggal Mulai Studi</th>
                                            <th class="min-w-100px text-center rounded-end">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($perguruantinggi as $p)
                                            <tr id="pid{{ $p->id }}" class="">
                                                <td class="ps-4 text-dark text-hover-primary mb-1 fs-6">
                                                    {{ $p->perguruanTinggiId->namaPerguruanTinggi }}
                                                    {{-- <span id="namaPerguruanTinggi" value="{{ $p->perguruanTinggiId->namaPerguruanTinggi }}" hidden></span> --}}
                                                </td>
                                                <td class="text-dark text-hover-primary mb-1 fs-6">
                                                    {{ $p->programStudiId->programStudi }}
                                                </td>
                                                <td class="text-dark text-hover-primary mb-1 fs-6">
                                                    {{ $p->negaraPtId->namaNegara }}
                                                </td>
                                                <td class="text-dark text-hover-primary mb-1 fs-6">
                                                    {{ $p->tglMulai }}
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-icon btn-warning editPt" data-toggle="modal" data-target="#update_perguruan_tinggi" data-id="{{ $p->id }}" data-1="{{ $p->perguruanTinggiId->id }}" data-2="{{ $p->programStudiId->id }}" data-3="{{ $p->negaraPtId->id }}" data-4="{{ $p->tglMulai }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-icon btn-danger deletePt" data-id="{{ $p->id }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                        </div>
                    </div>
                    <div class="card border-warning" id="tab">
                        <div class="card-header card-header-stretch" id="card-header">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder" id="kt_security_summary_tabs">
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='lps'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=lps#tab') }}">Laporan Perkembangan Studi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='cuti'){ echo 'active';} ?>"  href="{{ url('tubel/administrasi/'.$tubel->id.'?tab=cuti#tab') }}">Pengajuan Cuti Belajar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Tab content-->
                            <div class="tab-content">

                                <div class="tab-pane fade <?php if ($tab=='lps'){ echo 'active show';} ?> " id="lps" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_laporan_perkembangan_studi" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Laporan
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($tab=='lps')
                                    @include('tubel.tabel_lps')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='cuti'){ echo 'active show';} ?> " id="cuti" role="tabpanel">
                                    <div class="row">
                                        <div class="col">
                                            <div class="text-end mb-4">
                                                <button href="#" data-bs-toggle="modal" data-bs-target="#add_cuti_belajar" class="btn btn-primary btn-sm fs-6">
                                                    <i class="fa fa-plus"></i> Buat Permohonan
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($tab=='cuti')
                                    @include('tubel.tabel_cuti')
                                    @endif
                                </div>
                            </div>
                            <!--end::Tab content-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>

    {{-- Modal add perguruan tinggi --}}
    <div class="modal fade" id="add_perguruan_tinggi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-600px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h2>Tambah Perguruan Tinggi</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" method="post" action="{{ url('/tubel/administrasi/perguruanTinggi/'.$tubel->id) }}">
                    @csrf
                    {{ method_field('post') }}
                    <div class="modal-body scroll-y px-10 px-lg-15">
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Perguruan Tinggi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan nama perguruan tinggi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="pt" required>
                                <option selected disabled>Pilih Perguruan Tinggi</option>
                                @foreach ($ref_perguruantinggi as $pt)
                                    <option value="{{ $pt->id }}">{{ $pt->namaPerguruanTinggi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Program Studi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan perogram studi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="prodi" required>
                                <option selected disabled>Pilih prodi</option>
                                @foreach ($ref_prodi as $p)
                                    <option value="{{ $p->id }}">{{ $p->programStudi }}</option>
                                @endforeach
                            </select>                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Negara</label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." name="negara" required>
                                @foreach ($ref_negara as $n)
                                    <option value="{{ $n->id }}">{{ $n->namaNegara }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                    <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control form-control-solid ps-12 flatpickr-input active" id="tgl_mulai_pendidikan2" placeholder="Pilih tanggal..." name="tglMulai" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-end">
                            <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-sm btn-success">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_laporan_perkembangan_studi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Laporan Perkembangan Studi</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-10 px-lg-10">
                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
                        <div class="d-flex justify-content-center justify-content-xl-start flex-row-auto">
                            <div class="stepper-nav ps-lg-10">
                                <div class="stepper-item current" data-kt-stepper-element="nav">
                                    <div class="stepper-line w-40px"></div>
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">1</span>
                                    </div>
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Data Pegawai</h3>
                                        <div class="stepper-desc">detail pegawai tugas belajar</div>
                                    </div>
                                </div>
                                <div class="stepper-item" data-kt-stepper-element="nav">
                                    <div class="stepper-line w-40px"></div>
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">2</span>
                                    </div>
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Laporan</h3>
                                        <div class="stepper-desc">detail laporan studi</div>
                                    </div>
                                </div>
                                <div class="stepper-item" data-kt-stepper-element="nav">
                                    <div class="stepper-line w-40px"></div>
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">3</span>
                                    </div>
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Finish</h3>
                                        <div class="stepper-desc">submit laporan</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-row-fluid py-lg-5 px-lg-15">
                            <form class="form" novalidate="novalidate" id="kt_modal_create_app_form">
                                <!--begin::Step 1-->
                                <div class="current" data-kt-stepper-element="content">
                                    <div class="w-100">
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Nama Pegawai</span>
                                                {{-- <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your unique app name"></i> --}}
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nama_pegawai" value="{{ session()->get('user')['namaPegawai'] }}" disabled/>
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">NIP 18</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nip_18" value="{{ session()->get('user')['nip18'] }}" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Pangkat</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nama_pangkat" value="{{ session()->get('user')['pangkat'] }}" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Jabatan</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nama_jabatan" value="{{ session()->get('user')['jabatan'] }}" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Unit Organisasi</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nama_unit_org" value="{{ session()->get('user')['unit'] }}" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Kantor</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="nama_kantor" value="{{ session()->get('user')['kantor'] }}" />
                                        </div>
                                    </div>
                                </div>
                                <!--begin::Step 2-->
                                <div data-kt-stepper-element="content">
                                    <div class="w-100">
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Nama Perguruan Tinggi</span>
                                            </label>
                                            <select name="perguruan_tinggi" class="form-select form-select-lg form-select-solid" data-control="select2" data-placeholder="Pilih perguruan tinggi ..." data-allow-clear="true" data-hide-search="true">
                                                <option></option>
                                                <option value="1">S Corporation</option>
                                                <option value="1">C Corporation</option>
                                                <option value="2">Sole Proprietorship</option>
                                                <option value="3">Non-profit</option>
                                                <option value="4">Limited Liability</option>
                                                <option value="5">General Partnership</option>
                                            </select>
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Semester ke-</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="semester" />
                                        </div>
                                        <div class="fv-row mb-4">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Semester</span>
                                            </label>
                                            <div class="form-check form-check-custom form-check-solid mb-2">
                                                <input class="form-check-input" type="radio" value="" name="semester_ke" id="flexRadioDefault"/>
                                                <label class="form-check-label" for="flexRadioDefault">
                                                    Ganjil
                                                </label>
                                            </div>
                                            <div class="form-check form-check-custom form-check-solid">
                                                <input class="form-check-input" type="radio" value="2" name="semester_ke" id="flexRadioDefault2"/>
                                                <label class="form-check-label" for="flexRadioDefault">
                                                    Genap
                                                </label>
                                            </div>
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Tahun Akademik</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="tahun" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">IPK</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="ipk" />
                                        </div>
                                        <div class="fv-row mb-3">
                                            <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                <span class="required">Jumlah SKS</span>
                                            </label>
                                            <input type="text" class="form-control form-control form-control-solid" name="jumlah_sks" />
                                        </div>
                                    </div>
                                </div>
                                <!--begin::Step 3-->
                                <div data-kt-stepper-element="content">
                                    <div class="w-100 text-center">
                                        <!--begin::Heading-->
                                        <h1 class="fw-bolder text-dark mb-3">Notifikasi</h1>
                                        <!--end::Heading-->
                                        <!--begin::Description-->
                                        <div class="text-muted fw-bold fs-3">Pastikan data yang anda input sudah benar!</div>
                                        <!--end::Description-->
                                        <!--begin::Illustration-->
                                        <div class="text-center px-4 py-15">
                                            <img src="{{ url('assets/dist') }}/assets/media/illustrations/todo.png" alt="" class="mw-100 mh-150px" />
                                        </div>
                                        <!--end::Illustration-->
                                    </div>
                                </div>
                                <!--begin::Actions-->
                                <div class="d-flex flex-stack text-end pt-10">
                                    <!--begin::Wrapper-->
                                    <div class="me-2">
                                        <button type="button" class="btn btn-sm btn-secondary me-3" data-kt-stepper-action="previous">
                                            << Sebelumnya
                                        </button>
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Wrapper-->
                                    <div>
                                        <button type="button" class="btn btn-sm btn-success" data-kt-stepper-action="submit">
                                            <span class="indicator-label fs-bold"><i class="fa fa-save"></i>Simpan</span>
                                            <span class="indicator-progress">Loading...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-success" data-kt-stepper-action="next">Selanjutnya >>
                                        <!--end::Svg Icon--></button>
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Actions-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="update_perguruan_tinggi" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-600px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h2>Edit Perguruan Tinggi</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form id="form_update_perguruan_tinggi" class="form" method="patch" action="">
                    @csrf
                    <input type="hidden" id="idTubel" name="idTubel" value="{{ $tubel->id }}" >
                    <input type="hidden" id="idPerguruanTinggi" name="idPerguruanTinggi" value="" >
                    <div class="modal-body scroll-y px-10 px-lg-15">
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Nama Perguruan Tinggi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan nama perguruan tinggi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="pt2" name="pt2" value="" required>
                                @foreach ($ref_perguruantinggi as $pt)
                                    <option value="{{ $pt->id }}">{{ $pt->namaPerguruanTinggi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Program Studi</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="diisi dengan perogram studi"></i>
                            </label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="prodi2" name="prodi2" value="" required>
                                @foreach ($ref_prodi as $p)
                                    <option value="{{ $p->id }}">{{ $p->programStudi }}</option>
                                @endforeach
                            </select>                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Negara</label>
                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Pilih negara ..." id="negara2" name="negara2" value="" required>
                                @foreach ($ref_negara as $n)
                                    <option value="{{ $n->id }}">{{ $n->namaNegara }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <label class="required fs-6 fw-bold mb-2">Tanggal mulai pendidikan</label>
                            <div class="position-relative d-flex align-items-center">
                                <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                    <span class="symbol-label bg-secondary">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                    <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                                <input class="form-control form-control-solid ps-12 flatpickr-input active" id="tgl_mulai_pendidikan" placeholder="Pilih tanggal..." name="tglMulai2" type="text" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-end">
                            <button type="button" class="btn btn-sm btn-secondary me-3" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-sm btn-success btnUpdate"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_cuti_belajar" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Permohonan Cuti Belajar</h4>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>
                <form class="form" id="formAddCuti" method="post" action="{{ url('/tubel/administrasi/addcutibelajar') }}">
                    @csrf

                    <input type="hidden" name="idTubel" value="{{$tubel->id}}">

                    <div class="modal-body scroll-y px-5 px-lg-10">
                        <div class="rounded border p-4 d-flex flex-column mb-6"> 
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglMulai" placeholder="Pilih tanggal mulai" name="tglMulaiSt" type="text" value="{{ old('tglMulaiSt') }}" required>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active datepick" id="tglSelesai" placeholder="Pilih tanggal selesai" name="tglSelesaiSt" type="text" value="{{ old('tglSelesaiSt') }}" required>
                                    </div>
                                </div>
    
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                <div class="col-lg">
                                    <textarea class="form-control" type="text" name="alasan_cuti" rows="4" placeholder="alasan pengajuan cuti belajar" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" name="surat_izin_cuti" rows="4" placeholder="surat izin cuti" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSurat" placeholder="Pilih tanggal " name="tglSurat" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_cuti">
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                <div class="col-lg">
                                    <input class="form-control" type="text" name="surat_izin_cuti_sponsor" placeholder="surat izin cuti (sponsor)" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                <div class="col-lg">
                                    <div class="position-relative d-flex align-items-center">
                                        <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                            <span class="symbol-label bg-secondary">
                                                <span class="svg-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                            <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </span>
                                        </div>
                                        <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor" placeholder="Pilih tanggal " name="tglSuratSponsor" type="text" value="{{ old('tglKepPembebasan') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                <div class="col-lg">
                                    <input class="form-control" type="file" name="file_sponsors" required>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mb-8">
                            <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fa fa-times"></i>Batal</button>

                            <button type="submit" id="btnSimpanCuti" class="btn btn-sm btn-success"><i class="fa fa-save"></i>Simpan</button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-app.js"></script>
    <script src="{{ url('assets/dist') }}/assets/js/custom/modals/create-account.js"></script>

    <script>
        // form tambah perguruan tinggi
        $("#tgl_mulai_pendidikan").flatpickr();
        $("#tgl_mulai_pendidikan2").flatpickr();
        $("#tglSurat").flatpickr();
        $("#tglSuratSponsor").flatpickr();
        $(".datepick").flatpickr();
        

        $(".editPt").click(function(){
            $('#update_perguruan_tinggi').modal('show');

            var id = $(this).data('id')
            var idPt = $(this).data('1');
            var idProdi = $(this).data('2');
            var idNegara = $(this).data('3');
            var tglMulai = $(this).data('4');

            // console.log()

            $('#idPerguruanTinggi').val(id).change();
            $('#pt2').val(idPt).change();
            $('#prodi2').val(idProdi).change();
            $('#negara2').val(idNegara).change();
            $('#tgl_mulai_pendidikan').val(tglMulai);
        });

        // edit perguruan tinggi
        $(".btnUpdate").click(function(e){
            e.preventDefault();

            var id = $('#idPerguruanTinggi').val();
            var idTubel = $('#idTubel').val();
            var idPt = $('#pt2').val();
            var idProdi = $('#prodi2').val();
            var idNegara = $('#negara2').val();
            var tglMulai = $('#tgl_mulai_pendidikan').val();

            var url = 'http://localhost:8000/tubel/administrasi/perguruanTinggi/'+id
            var token = $("meta[name='csrf-token']").attr("content")

            $.ajax({
                type:'patch',
                url: url,
                data: {
                    _token: token,
                    id: id,
                    idTubel: idTubel,
                    idPt: idPt,
                    idProdi: idProdi,
                    idNegara: idNegara,
                    tglMulai: tglMulai
                },
                success:function(response){
                    // console.log(response)
                    $('#update_perguruan_tinggi').modal('hide');

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Sukses!',
                        text: 'Data berhasil diupdate',
                        showConfirmButton: false,
                        // width: 600,
                        // padding: '5em',
                        timer: 1200
                    })
                },
                error: function(err) {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Data gagal diupdate',
                        showConfirmButton: false,
                        // width: 600,
                        // padding: '5em',
                        timer: 1200
                    })
                }
            });
        });

        $(".deletePt").click(function(){
            var id = $(this).data("id")
            var token = $("meta[name='csrf-token']").attr("content")
            var url = 'http://localhost:8000/tubel/administrasi/perguruanTinggi/'+id

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                text: "Data tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus!'
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {
                            $('#pid'+id).remove();

                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                // width: 600,
                                // padding: '5em',
                                timer: 1200
                            })
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });

                }
            })
        });


        $(".btnHapusCuti").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(this).data("url");

            //alert(url);

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    //alert(result.value);

                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            id: id
                        },
                        success: function(response) {
                            
                            if(response['status']===1){
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di hapus',
                                showConfirmButton: false,
                                timer: 1800
                                });

                                $('#cut'+id).remove();

                                

                            }else{
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1800
                                });
                                
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                   

                }
            })



        });


        

        $(".btnKirimCuti").click(function(){
            var act = $(this).data("act");
            var id = $(this).data("id");
            var url = $(this).data("url");
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan mengajukan cuti?',
                text: "Data akan masuk ke tahap berikutnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#40BA50',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: id,
                            keterangan : null
                        },
                        success: function(response) {
                            if(response.status===1){

                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Berhasil',
                                text: 'data berhasil di kirim',
                                showConfirmButton: false,
                                timer: 1600
                                });

                                location.reload();

                            }else{
                                Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gagal' ,
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1600
                                });

                            }
                            
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });

                }
            })
        });


        
        

    </script>
@endsection
