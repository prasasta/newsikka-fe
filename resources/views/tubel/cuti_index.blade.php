@extends('layout.master')

@section('content')
@php
    //dd($tubel->data)
@endphp
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card border-warning">
                        <div class="card-header card-header-stretch" id="card-header">
                            <div class="card-toolbar">
                                <ul class="nav nav-tabs nav-line-tabs nav-stretch border-transparent fs-5 fw-bolder" id="tabLps">
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='tunggu'){ echo 'active';} ?>"  href="{{ url('tubel/cuti') }}"> Menunggu Persetujuan </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='setuju'){ echo 'active';} ?>"  href="{{ url('tubel/cutisetuju') }}"> Telah Disetujui </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='tolak'){ echo 'active';} ?>"  href="{{ url('tubel/cutitolak') }}"> Ditolak </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='selesai'){ echo 'active';} ?>" href="{{ url('tubel/cutiselesai') }}"> Telah Ditempatkan (Cuti Belajar) </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-active-primary <?php if ($tab=='dashboard'){ echo 'active';} ?>" href="{{ url('tubel/cutidashboard') }}"> Dashboard </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Tab content-->
                            <div class="tab-content">
                                <!--begin::Tab panel-->
                                <div class="tab-pane fade <?php if ($tab=='tunggu'){ echo 'active show';} ?> " id="tunggu" role="tabpanel">
                                    @if ($tab=='tunggu')
                                    @include('tubel.cuti_tunggu')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='setuju'){ echo 'active show';} ?> " id="setuju" role="tabpanel">
                                    @if ($tab=='setuju')
                                    @include('tubel.cuti_tunggu')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='tolak'){ echo 'active show';} ?> " id="tolak" role="tabpanel">
                                    @if ($tab=='tolak')
                                    @include('tubel.cuti_tunggu')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='selesai'){ echo 'active show';} ?> " id="selesai" role="tabpanel">
                                    @if ($tab=='selesai')
                                    @include('tubel.tabel_penempatancuti')
                                    @endif
                                </div>

                                <div class="tab-pane fade <?php if ($tab=='dashboard'){ echo 'active show';} ?> " id="dashboard" role="tabpanel">
                                    @if ($tab=='dashboard')
                                    @include('tubel.cuti_tunggu')
                                    @endif
                                </div>
                                <!--end::Tab panel-->
                            </div>
                            <!--end::Tab content-->
                        </div>
                    </div>
                </div>
            </div>
        <!--end::Container-->
    </div>


    <div class="modal fade" id="periksa_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penelitian Cuti Belajar</h4>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>
                <form class="form" id="formAddCuti" method="post" action="#">
                        <input type="hidden" name="idTubel" value="">

                        <input type="hidden" id="idCuti" name="idCuti" value="">

                        <div class="modal-body scroll-y px-5 px-lg-10">
                            {{-- <div class="rounded border p-4 d-flex flex-column mb-6">
                                <div class="row mb-3">
                                        <div class="col-md col-lg">

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nama</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jenjang Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: aSs </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Program Beasiswa</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lokasi</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lama Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Total Semester</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Kantor Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jabatan Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Surat Tugas</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Mulai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Selesai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor KEP Pembebasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">TMT</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Email Non Kedinasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Handphone</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Alamat</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div> --}}

                            {{-- <div class="rounded border p-4 d-flex flex-column mb-6"> --}}
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tglMulai" placeholder="" name="tglMulaiSt" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tglSelesai" placeholder="" name="tglSelesaiSt" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_cuti" type="text" name="alasan_cuti" rows="4" placeholder="" required disabled></textarea>
                                    </div>
                                </div>

                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti" rows="4" id="surat_izin_cuti_kampus" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tglSuratKampus" placeholder="" name="tglSurat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href="" id="filesuratkampus" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Kampus).pdf</a>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_sponsor" id="surat_izin_cuti_sponsor" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tglSuratSponsor" placeholder="" name="tglSuratSponsor" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href="" id="filesuratsponsor" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Sponsor).pdf</a>
                                    </div>
                                </div>

                            {{-- </div> --}}

                            <div class="text-center mb-4">
                                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class=""><<</i>Batal</button>
                                <button type="button" id="btnTolakCuti" class="btn btn-sm btn-danger" data-act="55a37cdb-0313-419d-a611-67a958ee7412"><i class="fa fa-times"></i>Tolak</button>
                                <button type="button" id="btnSetujuCuti" class="btn btn-sm btn-success" data-act="0491419d-d406-4e94-8136-87c754d444ca"><i class="fa fa-check-circle"></i>Setuju</button>
                            </div>

                        </div>

                    {{-- <div class="modal-footer">

                    </div> --}}
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lihat_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penelitian Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formLihatCuti" method="post" action="#">
                        <input type="hidden" name="idTubel" value="">

                        <input type="hidden" id="idCuti" name="idCuti" value="">

                        <div class="modal-body scroll-y px-5 px-lg-10">
                            {{-- <div class="rounded border p-4 d-flex flex-column mb-6">
                                <div class="row mb-3">
                                        <div class="col-md col-lg">

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nama</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jenjang Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: aSs </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Program Beasiswa</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lokasi</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Lama Pendidikan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Total Semester</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">: </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Kantor Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Jabatan Asal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Surat Tugas</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Mulai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal Selesai</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="mb-2">
                                                <br>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor KEP Pembebasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Tanggal</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">TMT</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md col-lg">
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Email Non Kedinasan</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Nomor Handphone</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label class="col-lg-5 col-md  fs-6">Alamat</label>
                                                <div class="col-lg d-flex align-items-center">
                                                    <span class="fw-bolder fs-6 text-gray-600 me-2">:  </span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div> --}}

                            {{-- <div class="rounded border p-4 d-flex flex-column mb-6"> --}}
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active"  id="tgl_mulai_st_lihat" placeholder="" name="tgl_mulai_st_lihat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active"  id="tgl_selesai_st_lihat" placeholder="" name="tgl_selesai_st_lihat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_cuti_lihat" type="text" name="alasan_cuti_lihat" rows="4" placeholder="" required disabled></textarea>
                                    </div>
                                </div>

                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_kampus_lihat" rows="4" id="surat_izin_cuti_kampus_lihat" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_kampus_lihat" placeholder="" name="tgl_surat_kampus_lihat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href="" id="file_surat_kampus_lihat" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Kampus).pdf</a>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_sponsor_lihat" id="surat_izin_cuti_sponsor_lihat" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_sponsor_lihat" placeholder="" name="tgl_surat_sponsor_lihat" type="text" value=""  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">File Surat</label>
                                    <div class="col-lg">
                                        <a href="" id="file_surat_sponsor" class="form-control-plaintext"><i class="fa fa-file"></i>  Surat Izin Cuti (Sponsor).pdf</a>
                                    </div>
                                </div>

                            {{-- </div> --}}

                        </div>

                    {{-- <div class="modal-footer">

                    </div> --}}
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="rekam_cuti" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog mw-900px">
            <div class="modal-content rounded">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penelitian Cuti Belajar</h4>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>
                <form class="form" id="formAddCutiAdmin" method="post" enctype="multipart/form-data" action="{{ url('tubel/administrasi/addcutibelajaradmin') }}">
                    @csrf
                        <input type="hidden" name="idTubel" value="">

                        <div class="modal-body scroll-y px-5 px-lg-10">
                            <div class="row mb-3">
                                <label class="col-lg-3 col-form-label fw-bold fs-6">NIP Pegawai</label>
                                <div class="col-lg-3">
                                    <input class="form-control" type="text" name="nip" id="niprekam" placeholder="NIP 18 digit">
                                </div>
                                <div class="form-label col-lg-3">
                                    <button id="btnGetPegawai" class="btn btn-primary">
                                        <i class="fa fa-search"></i> Cari
                                    </button>
                                    <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                        Loading
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            <div id="formInput" style="display: none">
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Mulai / selesai Cuti</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tgl_mulai_cuti_rekam" placeholder="Pilih tanggal mulai" name="tgl_mulai_cuti_rekam" type="text" value="" >
                                        </div>
                                    </div>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active datepick"  id="tgl_selesai_cuti_rekam" placeholder="Pilih tanggal selesai" name="tgl_selesai_cuti_rekam" type="text" value="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Alasan Cuti</label>
                                    <div class="col-lg">
                                        <textarea class="form-control" id="alasan_cuti_rekam" type="text" name="alasan_cuti_rekam" rows="4" placeholder="alasan pengajuan cuto belajar" required></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Kampus)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_kampus_rekam" rows="4" id="surat_izin_cuti_kampus_rekam" placeholder="surat izin cuti">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_kampus_rekam" placeholder="Pilih tanggal" name="tgl_surat_kampus_rekam" type="text" value="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_cuti_rekam">
                                    </div>
                                </div>
                                <br>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Surat Izin Cuti (Sponsor)</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="text" name="surat_izin_cuti_sponsor_rekam" id="surat_izin_cuti_sponsor_rekam" placeholder="surat izin cuti (sponsor)">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Tanggal Surat</label>
                                    <div class="col-lg">
                                        <div class="position-relative d-flex align-items-center">
                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                <span class="symbol-label bg-secondary">
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"></rect>
                                                                <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <input class="form-control ps-12 flatpickr-input active" id="tgl_surat_sponsor_rekam" placeholder="Pilih tanggal" name="tgl_surat_sponsor_rekam" type="text" value="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-12">
                                    <label class="col-lg-3 col-form-label fw-bold fs-6">Unggah Surat</label>
                                    <div class="col-lg">
                                        <input class="form-control" type="file" name="file_sponsors_rekam" required>
                                    </div>
                                </div>

                                <div class="text-center mb-3">
                                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class=""><<</i>Batal</button>
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i>Simpan</button>
                                </div>
                            </div>
                        </div>

                    {{-- <div class="modal-footer">

                    </div> --}}
                </form>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <script>
        // assets
        $("#tgl_mulai_cuti_rekam").flatpickr();
        $("#tgl_selesai_cuti_rekam").flatpickr();
        $("#tgl_surat_kampus_rekam").flatpickr();
        $("#tgl_surat_sponsor_rekam").flatpickr();


        // button periksa cuti
        $(".btnPeriksa").click(function() {
            // alert($(this).data("idcuti"));

            $("#idCuti").val($(this).data("idcuti"));
            $("#alasan_cuti").attr("placeholder", $(this).data("alasancuti"));
            $("#tglMulai").attr("placeholder", $(this).data("tglmulai"));
            $("#tglSelesai").attr("placeholder", $(this).data("tglselesai"));
            $("#tglSurat").attr("placeholder", $(this).data("tglsurat"));
            $("#surat_izin_cuti_kampus").attr("placeholder", $(this).data("suratkampus"));
            $("#surat_izin_cuti_sponsor").attr("placeholder", $(this).data("suratsponsor"));
            $("#tglSuratSponsor").attr("placeholder", $(this).data("tglsponsor"));
            $("#tglSuratKampus").attr("placeholder", $(this).data("tglkampus"));
            $("#filesuratkampus").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratkampus"));
            $("#filesuratsponsor").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratsponsor"));
            $("#periksa_cuti").modal('show');
            $.fn.modal.Constructor.prototype._enforceFocus = function() {};

        });

        // button lihat cuti
        $(".btnLihat").click(function() {
            // alert($(this).data("idcuti"));

            $("#tgl_mulai_st_lihat").attr("placeholder", $(this).data("tglmulai"));
            $("#tgl_selesai_st_lihat").attr("placeholder", $(this).data("tglselesai"));
            $("#alasan_cuti_lihat").attr("placeholder", $(this).data("alasancuti"));
            $("#surat_izin_cuti_kampus_lihat").attr("placeholder", $(this).data("suratkampus"));
            $("#tgl_surat_kampus_lihat").attr("placeholder", $(this).data("tglkampus"));
            $("#surat_izin_cuti_sponsor_lihat").attr("placeholder", $(this).data("suratsponsor"));
            $("#tgl_surat_sponsor_lihat").attr("placeholder", $(this).data("tglsponsor"));
            $("#file_surat_kampus_lihat").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratkampus"));
            $("#file_surat_sponsor").attr("href", '{{ env('APP_URL') }}' + '/files/' + $(this).data("filesuratsponsor"));

            $("#lihat_cuti").modal('show');

        });

        // button setuju cuti aksi
        $("#btnSetujuCuti").click(function(){
            var act = $(this).data("act");
            var id = $('#idCuti').val()

            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menyetujui data?',
                text: "Data akan masuk ke tahap berikutnya!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#50CD89',
                cancelButtonColor: '#E4E6EF',
                confirmButtonText: 'Setuju!',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    // alert(id);
                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/updatestatuscuti',
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: id,
                            keterangan: null
                        },
                        success: function(response) {
                            // console.log(response);
                            if(response['status'] === 0){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal' ,
                                    text: response['message'],
                                    showConfirmButton: false,
                                    // width: 600,
                                    // padding: '5em',
                                    timer: 1600
                                });
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di setujui',
                                    showConfirmButton: false,
                                    // width: 600,
                                    // padding: '5em',
                                    timer: 1600
                                });
                                // location.reload();
                                // window.location = "{{url('tubel/lps')}}";
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        // button tolak cuti aksi
        $("#btnTolakCuti").click(function(){
            var act = $(this).data("act")
            var idCuti = $('#idCuti').val()
            var token = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                input: 'textarea',
                inputLabel: 'Alasan Tolak',
                inputPlaceholder: 'Masukan Alasan Penolakan',
                inputAttributes: {
                    'aria-label': 'Masukan Alasan Penolakan'
                },
                title: 'Yakin akan menolak data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#F1416C',
                cancelButtonColor: '#E4E6EF',
                confirmButtonText: 'Tolak!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        type: 'post',
                        url: '{{ env('APP_URL') }}' + '/tubel/updatestatuscuti',
                        data: {
                            _token: token,
                            caseOutputId: act,
                            id: idCuti,
                            keterangan: result.value
                        },
                        success: function(response) {
                            console.log(response)
                            if(response.status=0){

                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1600
                                });

                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil' ,
                                    text: 'data berhasil di tolak',
                                    showConfirmButton: false,
                                    timer: 1600
                                });
                                location.reload();

                            }

                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        // button rekam cuti
        $("#btnTolakCuti").click(function(){
            var act = $(this).data("act")
            var idCuti = $('#idCuti').val()
            var token = $("meta[name='csrf-token']").attr("content");
        });

        // button rekam cuti > get pegawai
        $("#btnGetPegawai").click(function() {
            var $this = $(this);
            $('#formInput').hide();

            //Call Button Loading Function
            BtnLoading($this);

            let url = '{{ env('APP_URL') }}' + '/tubel/administrasi/getTubel';

            $.ajax({
                type: 'get',
                url: url,
                success: function(response){
                    var msg = response.status
                    if(msg === 0) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Gagal!',
                            text: "Message: " + response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else {
                        // $("#idTubel").val(response.idtubel);
                        $('#niprekam').prop('disabled', true);
                        $('#formInput').show();
                    }

                    // reset button
                    BtnReset($this);
                }
            });

        });


        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }
    </script>
@endsection


