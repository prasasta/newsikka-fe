@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
                <div class="col-lg-12">
                    <div class="card mb-3 border-warning shadow-sm">
                        <div id="card-header" class="card-header">
                            <div class="card-title fw-bold fs-4 text-white">
                                Administrasi Pegawai Tugas Belajar
                            </div>
                        </div>
                        {{-- End Card Header --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10"></div>
                                <div class="col-md-2">
                                    <div class="text-end mb-4">
                                        {{-- <button id="btnTarikData" class="btn btn-sm btn-primary fs-6" onclick="getDataTubel()" url="{{ env('API_URL').'/tubel/sikka/'.'882000399' }}"> --}}
                                        <button id="btnTarikData" class="btn btn-sm btn-primary fs-6" url="{{ env('API_URL').'/tubel/sikka/'.'882000399' }}">
                                            <i class="fa fa-download text-white"></i>Tarik Data
                                        </button>
                                        <button class="btn btn-primary btn-sm fs-6" type="button" hidden disabled>
                                            Loading...
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive bg-gray-400 rounded border border-gray-300">
                                <!--begin::Table-->
                                <table class="table table-striped align-middle" id="tabel_tubel">
                                    <!--begin::Thead-->
                                    <thead class="fw-bolder bg-secondary fs-6">
                                        <tr class="text-right">
                                            <th class="rounded-start ps-4">Jenjang Pendidikan</th>
                                            <th class="">Program Beasiswa / Lokasi</th>
                                            <th class="">Perguruan Tinggi / Program Studi</th>
                                            <th class="">Lama Pendidikan / Total Semester</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center rounded-end ps-4">Aksi</th>
                                        </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->
                                    <tbody>
                                        @php
                                            //dd($tubel);
                                        @endphp
                                        @if ($tubel != null)
                                            @foreach ($tubel as $t)
                                                <tr class="text-right">
                                                    <td class="ps-4 text-dark  mb-1 fs-6">
                                                        {{ $t->jenjangPendidikanId ? $t->jenjangPendidikanId->jenjangPendidikan : null }}
                                                    </td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        {{ $t->programBeasiswa }} <br> {{ $t->lokasiPendidikanId ? $t->lokasiPendidikanId->lokasi : null }}
                                                    </td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        {{-- {{ $t->jenjangPendidikanId ? $t->jenjangPendidikanId->jenjangPendidikan : null }} --}}
                                                    </td>
                                                    <td class="text-dark  mb-1 fs-6">
                                                        {{ $t->lamaPendidikan ? $t->lamaPendidikan.' Tahun' : null}} <br>
                                                        {{$t->totalSemester ? $t->totalSemester. ' Semester' : null }}

                                                    </td>
                                                    <td class="text-center text-dark  mb-1 fs-6">
                                                        <span class="badge badge-light-success fs-7 fw-bolder">Proses</span>
                                                    </td>
                                                    <td class="text-center text-dark mb-1 fs-6 ps-4">
                                                        <a href="{{ url('tubel/administrasi/'. $t->id) }}"
                                                            class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Kelola">
                                                            {{-- class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Kelola"> --}}
                                                            <i class="fa fa-cog"></i>Kelola
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                                <tr class="text-center">
                                                    <td class="text-dark  mb-1 fs-6" colspan="6">Tidak terdapat data</td>
                                                </tr>
                                        @endif
                                    </tbody>
                                    <!--end::Tbody-->
                                </table>
                                <!--end::Table-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <script>

        $(document).ready(function() {
            $("#btnTarikData").click(function() {
                var $this = $(this);

                //Call Button Loading Function
                BtnLoading($this);

                let url = '{{ env('APP_URL') }}' + '/tubel/administrasi/getTubel';

                $.ajax({
                    type: 'get',
                    url: url,
                    success: function(response){
                        var msg = response.status
                        if(msg === 0) {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Data gagal ditambahkan!',
                                text: "Message: " + response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        } else {
                            setTimeout(function() {
                                $(this).prop("disabled", true);
                                Swal.fire({
                                    // position: 'top-right',
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: response.message,
                                    showConfirmButton: false,
                                    // width: 600,
                                    // padding: '5em',
                                    timer: 2000
                                })
                            }, location.reload());
                        }

                        // reset button
                        BtnReset($this);
                    }
                });

            });
        });

        // spinner / loading button
        function BtnLoading(elem) {
            $(elem).attr("data-original-text", $(elem).html());
            $(elem).prop("disabled", true);
            $(elem).html('Loading... <i class="spinner-border spinner-border-sm"></i>');
        }

        // spinner off / reset button
        function BtnReset(elem) {
            $(elem).prop("disabled", false);
            $(elem).html($(elem).attr("data-original-text"));
        }
    </script>
@endsection
