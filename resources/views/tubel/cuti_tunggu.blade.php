@php
    //dd($cuti->data);
        $batas = 10;

        $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;

        $halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;

        $previous = $halaman - 1;

        $next = $halaman + 1;

        $jumlah_data = 8;

        $total_halaman = ceil($jumlah_data / $batas);

        $nomor = $halaman_awal + 1;
@endphp

@if ($tab=='setuju')
<div class="row">
    <div class="col">
        <div class="text-end mb-4">
            <button data-bs-toggle="modal" data-bs-target="#rekam_cuti" class="btn btn-primary btn-sm fs-6 btnRekamCuti">
                <i class="fa fa-plus"></i> Rekam Cuti
            </button>
        </div>
    </div>
</div>

@endif

<div class="table-responsive bg-gray-400 rounded border border-gray-300">
    <table class="table table-striped align-middle" id="tabel_cuti">
        <thead class="fw-bolder bg-secondary fs-6">
            <tr>
                <th class="text-center rounded-start">No</th>
                <th class="">Pegawai</th>
                <th class="">Pendidikan</th>
                <th class="">Surat ijin dari PT</th>
                <th class="">Surat ijin dari Sponsor</th>
                <th class="">Mulai / Selesai</th>
                <th class="rounded-end text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($cuti->data != null)

                @if ($tab == 'selesai')

                    @foreach ($cuti as $c)

                    @endforeach

                @else

                    @foreach ($cuti->data as $c)
                        <tr class="" id="cut{{$c->id}}">
                            <td class="text-dark text-center fw-bolder mb-1 fs-6 ps-4">
                                {{$nomor++}}
                            </td>

                            <td class="text-dark fw-bolder mb-1 fs-6">
                                <a href="{{ url('tubel/pegawai/'. $c->indukTubel->id) }}">
                                    {{$c->indukTubel->namaPegawai}}
                                </a>
                                <br>
                                <span class="text-muted fw-bold text-muted  fs-6">{{$c->indukTubel->nip18}}</span>
                            </td>

                            <td class="text-dark fw-bolder   mb-1 fs-6">
                                {{ $c->indukTubel->jenjangPendidikanId ? $c->indukTubel->jenjangPendidikanId->jenjangPendidikan : null }}
                                <br>
                                <span class="text-muted fw-bold text-muted  fs-6">{{ $c->indukTubel->lokasiPendidikanId ? $c->indukTubel->lokasiPendidikanId->lokasi : null }}</span>
                            </td>

                            <td class="text-dark fw-bolder   mb-1 fs-6">
                                {{$c->nomorSuratKampus}}
                                <br>
                                <span class="text-muted fw-bold text-muted  fs-6">{{AppHelper::instance()->indonesian_date($c->tglSuratKampus,'j F Y','')}}</span>
                            </td>

                            <td class="text-dark fw-bolder   mb-1 fs-6">
                                {{$c->nomorSuratSponsor}}
                                <br>
                                <span class="text-muted fw-bold text-muted  fs-6">{{AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor,'j F Y','')}}</span>
                            </td>

                            <td class="text-dark   mb-1 fs-6">
                                {{AppHelper::instance()->indonesian_date($c->tglMulaiCuti,'j F Y','')}}
                                <span class="text-muted fw-bold text-muted d-block fs-6">s.d</span>
                                {{AppHelper::instance()->indonesian_date($c->tglSelesaiCuti,'j F Y','')}}
                            </td>

                            <td class="text-center">
                                @if ($tab == 'setuju')
                                    <button class="btn btn-icon btn-sm btn-info m-2 btnLihat"
                                    data-idcuti="{{ $c->id }}" data-alasancuti="{{$c->alasanCuti}}" data-tglmulai="{{AppHelper::instance()->indonesian_date($c->tglMulaiCuti,'j F Y','')}}" data-tglselesai="{{AppHelper::instance()->indonesian_date($c->tglSelesaiCuti,'j F Y','')}}"  data-suratkampus="{{$c->nomorSuratKampus}}" data-tglkampus="{{AppHelper::instance()->indonesian_date($c->tglSuratKampus,'j F Y','')}}" data-suratsponsor="{{$c->nomorSuratSponsor}}" data-tglsponsor="{{AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor,'j F Y','')}}" data-filesuratsponsor="{{$c->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$c->pathSuratIjinCutiKampus}}"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                @else
                                    <button class="btn btn-sm btn-primary m-2 btnPeriksa"
                                    data-idcuti="{{ $c->id }}" data-alasancuti="{{$c->alasanCuti}}" data-tglmulai="{{AppHelper::instance()->indonesian_date($c->tglMulaiCuti,'j F Y','')}}" data-tglselesai="{{AppHelper::instance()->indonesian_date($c->tglSelesaiCuti,'j F Y','')}}"  data-suratkampus="{{$c->nomorSuratKampus}}" data-tglkampus="{{AppHelper::instance()->indonesian_date($c->tglSuratKampus,'j F Y','')}}" data-suratsponsor="{{$c->nomorSuratSponsor}}" data-tglsponsor="{{AppHelper::instance()->indonesian_date($c->tglSuratKepSponsor,'j F Y','')}}" data-filesuratsponsor="{{$c->pathSuratIjinCutiSponsor}}" data-filesuratkampus="{{$c->pathSuratIjinCutiKampus}}"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Periksa">
                                        <i class="fa fa-cog"></i>Periksa
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                @endif


            @else

                <tr class="text-center">
                    <td class="text-dark  mb-1 fs-6" colspan="7">Tidak terdapat data</td>
                </tr>

            @endif

        </tbody>
    </table>

    @if ($cuti->data != null)
        <div class="d-flex flex-stack flex-wrap pt-5 p-3">
            <div class="fs-6 fw-bold text-gray-700"></div>
        </div>
    @endif
</div>
