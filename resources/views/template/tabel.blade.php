@extends('layout.master')

@section('content')
    <div class="post d-flex flex-column" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Tabel 1-->
            <div class="card mb-5 mb-xl-8">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">Member Statistics</span>
                        <span class="text-muted mt-1 fw-bold fs-7">Over 500 new members</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Menu-->
                        <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                            <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </button>
                        <!--begin::Menu 2-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px"
                            data-kt-menu="true">
                            <!--begin::Menu separator-->
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item p-3">
                                <a href="#" class="menu-link px-3">New Ticket</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu 2-->
                        <!--end::Menu-->
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::tabel-->
                <div class="card-body py-3">
                    <!--begin::Table container-->
                    <div class="table-responsive">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr class="fw-bolder text-muted bg-light">
                                    <th class="ps-4 min-w-300px rounded-start">Agent</th>
                                    <th class="min-w-125px">Earnings</th>
                                    <th class="min-w-125px">Comission</th>
                                    <th class="min-w-200px">Company</th>
                                    <th class="min-w-150px">Rating</th>
                                    <th class="min-w-200px text-end rounded-end"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50px me-5">
                                                <span class="symbol-label bg-light">
                                                    <img src="{{ url('assets/dist') }}/assets/media/svg/avatars/001-boy.svg"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <div class="d-flex justify-content-start flex-column">
                                                <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Brad
                                                    Simmons</a>
                                                <span class="text-muted fw-bold text-muted d-block fs-7">HTML, JS,
                                                    ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$8,000,000</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Pending</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$5,400</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Intertico</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Web, UI/UX Design</span>
                                    </td>
                                    <td>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                        </div>
                                        <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">Best Rated</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">View</a>
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Edit</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50px me-5">
                                                <span class="symbol-label bg-light">
                                                    <img src="{{ url('assets/dist') }}/assets/media/svg/avatars/047-girl-25.svg"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <div class="d-flex justify-content-start flex-column">
                                                <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Lebron
                                                    Wayde</a>
                                                <span class="text-muted fw-bold text-muted d-block fs-7">PHP, Laravel,
                                                    VueJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$8,750,000</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$7,400</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Agoda</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Houses &amp; Hotels</span>
                                    </td>
                                    <td>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                        </div>
                                        <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">Above Avarage</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">View</a>
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Edit</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50px me-5">
                                                <span class="symbol-label bg-light">
                                                    <img src="{{ url('assets/dist') }}/assets/media/svg/avatars/006-girl-3.svg"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <div class="d-flex justify-content-start flex-column">
                                                <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Brad
                                                    Simmons</a>
                                                <span class="text-muted fw-bold text-muted d-block fs-7">HTML, JS,
                                                    ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$8,000,000</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">In Proccess</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$2,500</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Rejected</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">RoadGee</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                        </div>
                                        <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">Best Rated</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">View</a>
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Edit</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50px me-5">
                                                <span class="symbol-label bg-light">
                                                    <img src="{{ url('assets/dist') }}/assets/media/svg/avatars/014-girl-7.svg"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <div class="d-flex justify-content-start flex-column">
                                                <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Natali
                                                    Trump</a>
                                                <span class="text-muted fw-bold text-muted d-block fs-7">HTML, JS,
                                                    ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$700,000</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Pending</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$7,760</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <a href="#" class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">The
                                            Hill</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Insurance</span>
                                    </td>
                                    <td>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                        </div>
                                        <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">Avarage</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">View</a>
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Edit</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50px me-5">
                                                <span class="symbol-label bg-light">
                                                    <img src="{{ url('assets/dist') }}/assets/media/svg/avatars/020-girl-11.svg"
                                                        class="h-75 align-self-end" alt="" />
                                                </span>
                                            </div>
                                            <div class="d-flex justify-content-start flex-column">
                                                <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Jessie
                                                    Clarcson</a>
                                                <span class="text-muted fw-bold text-muted d-block fs-7">HTML, JS,
                                                    ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$1,320,000</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Pending</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">$6,250</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Paid</span>
                                    </td>
                                    <td>
                                        <a href="#"
                                            class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Intertico</a>
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Web, UI/UX Design</span>
                                    </td>
                                    <td>
                                        <div class="rating">
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                            <div class="rating-label me-2 checked">
                                                <i class="bi bi-star-fill fs-5"></i>
                                            </div>
                                        </div>
                                        <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">Best Rated</span>
                                    </td>
                                    <td class="text-end">
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">View</a>
                                        <a href="#"
                                            class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Edit</a>
                                    </td>
                                </tr>
                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--begin::Body-->
            </div>
            <!--end::Tabel 1-->

            <!--begin::Tabel 1-->
            <div class="card mb-5 mb-xl-8">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">Datatable</span>
                        <span class="text-muted mt-1 fw-bold fs-7">Over 500 new products</span>
                    </h3>
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-sm btn-light-primary">
                            <!--begin::Svg Icon | path: icons/duotone/Communication/Add-user.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                    version="1.1">
                                    <path
                                        d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path
                                        d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                        fill="#000000" fill-rule="nonzero"></path>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->Input Data
                        </a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-3">
                    <!--begin::Table container-->
                    <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5">
                        <thead>
                            <tr class="fw-bold fs-6 text-muted">
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tiger Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                            </tr>
                            <tr>
                                <td>Garrett Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>63</td>
                                <td>2011/07/25</td>
                                <td>$170,750</td>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                            </tr>
                            <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>$86,000</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </tfoot>
                    </table>
                    <!--end::Table container-->
                </div>
                <!--begin::Body-->
            </div>
            <!--end::Tabel 1-->


        </div>
        <!--end::Container-->
    </div>

@endsection


@section('js')
    <script>
        $("#kt_datatable_example_1").DataTable();
    </script>
@endsection
