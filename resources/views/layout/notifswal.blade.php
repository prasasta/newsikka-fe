@if ( null !== session()->get('error'))
    <script>
        var msg = '{{session()->get('error')}}';
        swalError(msg);
    </script>
@endif

@if ( null !== session()->get('success'))
    <script>
        var msg = '{{session()->get('success')}}';
        swalSuccess(msg);
    </script>
@endif