@extends('layout.master')

@section('content')
<?php
    //dump($usulanHariLibur);
    //dump($jumlahUsulan);
?>
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-fluid">
            <!--begin::Card-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header mt-6">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1 me-5">
                            <!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            <?php
                                if(isset($_GET['nomorSuratDasar'])){
                                    $searchValue = $_GET['nomorSuratDasar'];
                                }else if(isset($_GET['nomorTicket'])){
                                    $searchValue = $_GET['nomorTicket'];
                                }else if(isset($_GET['keteranganSuratDasar'])){
                                    $searchValue = $_GET['keteranganSuratDasar'];
                                }else{
                                    $searchValue = "";
                                }
                            ?>
                            <select class="form-select form-select-solid w-250px" data-control="select2" data-placeholder="Select an option" data-allow-clear="true" name="searchKategori" id="searchKategori">
                                <option value="nomorSuratDasar" {{ (isset($_GET['nomorSuratDasar']) ? 'selected' : '') }}>Nomor Surat Dasar</option>
                                <option value="nomorTicket" {{ (isset($_GET['nomorTicket']) ? 'selected' : '') }}>Nomor Ticket</option>
                                <option value="keteranganSuratDasar" {{ (isset($_GET['keteranganSuratDasar']) ? 'selected' : '') }}>Keterangan Surat Dasar</option>
                            </select>
                            <input type="text" data-kt-permissions-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Usulan" id="searchData" value="{{ $searchValue }}"/>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Card title-->
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <button type="button" class="btn btn-light-primary btnTambahPermohonan" data-bs-toggle="modal" data-bs-target="#kt_modal_permohonan">
                        <!--begin::Svg Icon | path: icons/duotone/Interface/Plus-Square.svg-->
                        <span class="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd" d="M6.54184 2.36899C4.34504 2.65912 2.65912 4.34504 2.36899 6.54184C2.16953 8.05208 2 9.94127 2 12C2 14.0587 2.16953 15.9479 2.36899 17.4582C2.65912 19.655 4.34504 21.3409 6.54184 21.631C8.05208 21.8305 9.94127 22 12 22C14.0587 22 15.9479 21.8305 17.4582 21.631C19.655 21.3409 21.3409 19.655 21.631 17.4582C21.8305 15.9479 22 14.0587 22 12C22 9.94127 21.8305 8.05208 21.631 6.54184C21.3409 4.34504 19.655 2.65912 17.4582 2.36899C15.9479 2.16953 14.0587 2 12 2C9.94127 2 8.05208 2.16953 6.54184 2.36899Z" fill="#12131A" />
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 17C12.5523 17 13 16.5523 13 16V13H16C16.5523 13 17 12.5523 17 12C17 11.4477 16.5523 11 16 11H13V8C13 7.44772 12.5523 7 12 7C11.4477 7 11 7.44772 11 8V11H8C7.44772 11 7 11.4477 7 12C7 12.5523 7.44771 13 8 13H11V16C11 16.5523 11.4477 17 12 17Z" fill="#12131A" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Tambah Permohonan Hari Libur</button>
                        <!--end::Button-->
                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <table class="table align-middle table-striped fs-6 gy-5 mb-0" id="kt_permissions_table">
                        <!--begin::Table head-->
                        <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-white fw-bolder fs-7 text-uppercase gs-0" style="background-color:#1d428a;">
                                <th class="min-w-50px text-center">No</th>
                                <th class="min-w-100px">No Dasar Hukum</th>
                                <th class="min-w-120px">Keterangan Dasar Hukum</th>
                                <th class="min-w-125px">Tanggal Hari Libur</th>
                                <th class="min-w-125px">Jenis</th>
                                <th class="min-w-125px">scope</th>
                                <th class="text-center min-w-100px">Actions</th>
                            </tr>
                            <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                            <?php $no = $numberOfElements; ?>
                            @foreach ($usulanHariLibur as $usulan)
                                <tr id="libur{{$usulan->id}}">
                                    <td class="text-center">{{ $no }}</td>
                                    <td>{{ $usulan->nomorSuratDasar }} <hr/>{{ AppHelper::convertDateDMY($usulan->tanggalSuratDasar) }}</td>
                                    <td>{{ $usulan->keteranganSuratDasar }}</td>
                                    <td>
                                        <?php
                                            if(isset($usulan->hariLibur->id)){
                                                $rangeHariLibur = AppHelper::convertDateDMY($usulan->hariLibur->tanggalAwal)." - ".
                                                                AppHelper::convertDateDMY($usulan->hariLibur->tanggalAkhir);
                                                echo $rangeHariLibur.'<hr/><span class="text-info">('.
                                                    $usulan->hariLibur->keterangan.')</span>';

                                                $idHariLibur = $usulan->hariLibur->id;

                                                // if(null == $usulan->hariLibur->scope){
                                                //     $usulan->hariLibur->scope = '';
                                                // }
                                            }else{
                                                $idHariLibur = '';
                                            }
                                            
                                            if(!isset($usulan->hariLibur->provinsiIds))
                                            {
                                                $usulan->hariLibur->provinsiIds = [];
                                            }
                                            //dump(json_encode($usulan->hariLibur->provinsiIds));
                                            
                                        ?>
                                    </td>
                                    <td>
                                        <span class="text-success">{{ $usulan->hariLibur->jenis->nama }}</span>
                                    </td>
                                    <td class="text-danger">
                                        <span>{{ (0 != $usulan->hariLibur->scope)? $scope[$usulan->hariLibur->scope] : ""}}</span>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-icon btn-warning btnEditUsulan mt-1" data-id="{{ $usulan->id }}" data-url="{{url('harilibur/administrasi/showFormEditUsulan')}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </button>

                                        <button class="btn btn-sm btn-icon btn-danger btnHapusUsulan mt-1" data-url="{{url('harilibur/administrasi/hapusUsulan')}}" data-id="{{ $usulan->id }}" data-idharilibur="{{$idHariLibur}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        <button class="btn btn-sm btn-primary btnKirimUsulan mt-1" data-id="{{ $usulan->id }}" data-url="{{url('harilibur/administrasi/kirimUsulan')}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Kirim">
                                            <i class="fa fa-paper-plane"></i> Kirim
                                        </button>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                    @if ($totalItems != 0)
                    <!--begin::pagination-->
                    <div class="d-flex flex-stack flex-wrap pt-5 p-3">
                        <div class="fs-6 fw-bold text-gray-700"></div>

                        @php
                            $disabled = '';
                            $nextPage= '';
                            $disablednext = '';
                            $hidden = '';
                            $pagination = '';

                            // tombol pagination
                            if ($totalItems == 0) {
                                // $pagination = 'none';
                            }

                            // tombol back
                            if($currentPage == 1){
                                $disabled = 'disabled';
                            }

                            // tombol next
                            if($currentPage != $totalPages) {
                                $nextPage = $currentPage + 1;
                            }

                            // tombol pagination
                            if($currentPage == $totalPages || $totalPages == 0){
                                $disablednext = 'disabled';
                                $hidden = 'hidden';
                            }

                        @endphp

                        <ul class="pagination" style="display: {{ $pagination }}">
                            <li class="page-item disabled"><a href="#" class="page-link">Halaman {{ $currentPage }} dari {{ $totalPages }}</a></li>

                            <li class="page-item previous {{ $disabled }}"><a href="{{ url()->current() . '?page=' . ($currentPage - 1) }}" class="page-link"><i class="previous"></i></a></li>
                            <li class="page-item active"><a href="{{ url()->current() . '?page=' . $currentPage }}" class="page-link">{{ $currentPage }}</a></li>
                            <li class="page-item" {{ $hidden }}><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link">{{ $nextPage }}</a></li>
                            <li class="page-item next {{ $disablednext }}"><a href="{{ url()->current() . '?page=' . $nextPage }}" class="page-link"><i class="next"></i></a></li>
                        </ul>

                    </div>
                @endif
                <!-- end::Pagination -->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
            <!--begin::Modals-->
            <!--begin::Modal - Add permohonan-->
            <div class="modal fade" id="kt_modal_permohonan" tabindex="-1" aria-hidden="true">
                <!--begin::Modal dialog-->
                <div class="modal-dialog modal-dialog-centered mw-1000px">
                    <!--begin::Modal content-->
                    <div class="modal-content">
                        <!--begin::Modal header-->
                        <div class="modal-header">
                            <!--begin::Modal title-->
                            <h2 class="fw-bolder">Form Permohonan Hari Libur</h2>
                            <!--end::Modal title-->
                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-permissions-modal-action="close">
                                <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                            <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                            <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                        </g>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </div>
                            <!--end::Close-->
                        </div>
                        <!--end::Modal header-->
                        <!--begin::Modal body-->
                        <div class="modal-body scroll-y mx-5 mx-xl-15 my-5">
                            <!--begin::Stepper-->
                            <div class="stepper stepper-links d-flex flex-column" id="kt_stepper_example_basic">
                                <!--begin::Nav-->
                                <div class="stepper-nav py-5">
                                    <!--begin::Step 1-->
                                    <div class="stepper-item current" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">1. Dasar Hukum Penetapan</h3>
                                    </div>
                                    <!--end::Step 1-->
                                    <!--begin::Step 2-->
                                    <div class="stepper-item" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">2. Usul Hari Libur</h3>
                                    </div>
                                    <!--end::Step 2-->
                                    <!--begin::Step 3-->
                                    <div class="stepper-item" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">3. Scope Detail</h3>
                                    </div>
                                    <!--end::Step 3-->
                                    <!--begin::Step 4-->
                                    <div class="stepper-item" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">4. Completed</h3>
                                    </div>
                                    <!--end::Step 4-->
                                </div>
                                <!--end::Nav-->

                                <!--begin::Form-->
                                <form class="form w-lg-500px mx-auto" id="kt_stepper_example_basic_form" action="{{ route('harilibur.createPermohonan') }}" method="post">
                                    @csrf

                                    {{-- menampilkan error validasi --}}
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <!--begin::Group-->
                                    <div class="mb-5">
                                        <!--begin::Step 1-->
                                        <div class="flex-column current" data-kt-stepper-element="content">
                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <!--begin::Label-->
                                                <label class="form-label">Dasar Hukum</label>
                                                <!--end::Label-->

                                                <!--begin::Input-->
                                                <input type="text" class="form-control form-control-solid" name="nomorSuratDasar" id="nomorSuratDasar" placeholder="" value="{{ old('nomorSuratDasar') }}"/>
                                                <input type="hidden" class="form-control form-control-solid" name="idUsulan" id="idUsulan" placeholder="" value=""/>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Tanggal Dasar Hukum</label>
                                                <!--end::Label-->
                                                <!--begin::Wrapper-->
                                                <div class="position-relative d-flex align-items-center">
                                                    <!--begin::Icon-->
                                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24" />
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1" />
                                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000" />
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </div>
                                                    <!--end::Icon-->
                                                    <!--begin::Input-->
                                                    <input class="form-control form-control-solid ps-12" placeholder="Pick date" name="tanggalSuratDasar" id="tanggalSuratDasar" value="{{ old('tanggalSuratDasar') }}" />
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Keterangan Dasar Hukum</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3" placeholder="Mohon isi penjelasan mengenai dasar hukum diatas" id="keteranganSuratDasar" name="keteranganSuratDasar">{{ old('keteranganSuratDasar') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                        </div>
                                        <!--begin::Step 1-->

                                        <!--begin::Step 2-->
                                        <div class="flex-column" data-kt-stepper-element="content">
                                            <!--begin::Input group-->
                                            <div class="fv-row mb-10">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Tanggal Hari Libur</label>
                                                <!--end::Label-->
                                                <!--begin::Wrapper-->
                                                <div class="position-relative d-flex align-items-center">
                                                    <!--begin::Icon-->
                                                    <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                        <span class="symbol-label bg-secondary">
                                                            <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-grid.svg-->
                                                            <span class="svg-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24" />
                                                                        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1" />
                                                                        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000" />
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </div>
                                                    <!--end::Icon-->
                                                    <!--begin::Input-->
                                                    <input class="form-control form-control-solid ps-12" placeholder="Pick date rage" id="tanggalHariLibur" name="tanggalHariLibur"/>
                                                    <input type="hidden" class="form-control form-control-solid" name="idHariLibur" id="idHariLibur" placeholder="" value=""/>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="fv-row mb-8">
                                                <!--begin::Label-->
                                                <label class="required fs-6 fw-bold mb-2">Keterangan Hari Libur</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control form-control-solid" rows="3" placeholder="Mohon isi penjelasan hari libur" id="keteranganHariLibur" name="keterangan">{{ old('keterangan') }}</textarea>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span class="required">Jenis Hari Libur</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid" data-control="select2" data-placeholder="Select an option" data-allow-clear="true" name="jenis" id="jenis">
                                                    <option></option>
                                                    @foreach ($jenisLibur as $jenis)
                                                        <option value="{{ $jenis->id }}">{{ $jenis->nama }}</option>
                                                    @endforeach
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row" id='div-scope'>
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span>Scope Hari Libur</span>
                                                    <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Harap diisi apabila jenis libur merupakan libur lokal/fakultatif"></i>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid" data-control="select2" data-placeholder="Select an option" data-allow-clear="true" name="scope" id="scope">
                                                    <option></option>
                                                    @foreach ($scope as $key => $val)
                                                        <option value="{{ $key }}">{{ $val }}</option>
                                                    @endforeach
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->
                                        </div>
                                        <!--begin::Step 2-->

                                        <!--begin::Step 3-->
                                        <div class="flex-column" data-kt-stepper-element="content">
                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row" id="div-provinsi">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span>Provinsi</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid select-provinsi" data-placeholder="Select an option" data-allow-clear="true" name="provinsiIds[]" id="provinsiIds" multiple="multiple">
                                                    <option></option>
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row" id="div-kota">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span>Kota</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid select-kota" data-placeholder="Select an option" data-allow-clear="true" name="kotaiIds[]" id="kotaiIds" multiple="multiple">
                                                    <option></option>
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row" id="div-agama">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span>Agama</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid select-agama" data-placeholder="Select an option" data-allow-clear="true" name="agamaIds[]" id="agamaIds" multiple="multiple">
                                                    <option value="ef6af353-3736-4bd0-87ff-c39c4cfb8647">HINDU</option>
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->

                                            <!--begin::Input group-->
                                            <div class="d-flex flex-column mb-5 fv-row" id="div-kantor">
                                                <!--begin::Label-->
                                                <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                                    <span>Kantor</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Select-->
                                                <select class="form-select form-select-solid select-kantor" data-placeholder="Select an option" data-allow-clear="true" name="kantorIds[]" id="kantorIds" multiple="multiple">
                                                    <option></option>
                                                </select>
                                                <!--end::Select-->
                                            </div>
                                            <!--end::Input group-->
                                        </div>
                                        <!--begin::Step 3-->

                                        <!--begin::Step 4-->
                                        <div class="flex-column" data-kt-stepper-element="content">
                                            <!--begin::Wrapper-->
                                            <div class="w-100">
                                                <!--begin::Heading-->
                                                <div class="pb-12 text-center">
                                                    <!--begin::Title-->
                                                    <h1 class="fw-bolder text-dark">Usulan Selesai Dibuat!</h1>
                                                    <!--end::Title-->
                                                    <!--begin::Description-->
                                                    <div class="text-muted fw-bold fs-4">Silahkan klik submit untuk mengirim permohonan usulan hari libur Bapak/Ibu</div>
                                                    <!--end::Description-->
                                                </div>
                                                <!--end::Heading-->
                                            </div>
                                            <!--end::Wrapper-->
                                        </div>
                                        <!--begin::Step 4-->
                                    </div>
                                    <!--end::Group-->

                                    <!--begin::Actions-->
                                    <div class="d-flex flex-stack">
                                        <!--begin::Wrapper-->
                                        <div class="me-2">
                                            <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">
                                                Back
                                            </button>
                                        </div>
                                        <!--end::Wrapper-->

                                        <!--begin::Wrapper-->
                                        <div>
                                            <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                                <span class="indicator-label">
                                                    Submit
                                                </span>
                                                <span class="indicator-progress">
                                                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                            </button>

                                            <button type="button" class="btn btn-primary" data-kt-stepper-action="next">
                                                Continue
                                            </button>
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Actions-->
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Stepper-->
                        </div>
                        <!--end::Modal body-->
                    </div>
                    <!--end::Modal content-->
                </div>
                <!--end::Modal dialog-->
            </div>
            <!--end::Modal - Add permohonan-->

        </div>
    </div>

@endsection

@section('js')
    <script>
        // Stepper lement
        var element = document.querySelector("#kt_stepper_example_basic");

        // stepper submit element
        var r = element.querySelector('[data-kt-stepper-action="submit"]');

        // Initialize Stepper
        var stepper = new KTStepper(element);

        // Handle next step
        stepper.on("kt.stepper.next", function (stepper) {
            currentStep = stepper.getCurrentStepIndex();
            jenis       = $(formSub.querySelector('[name="jenis"]')).val();

            if(2 == currentStep && (1 == jenis || 4 == jenis)){
                stepper.goTo(4);
            }else{
                stepper.goNext(); // go next step
            }
        });

        // Handle previous step
        stepper.on("kt.stepper.previous", function (stepper) {
            currentStep = stepper.getCurrentStepIndex();
            jenis       = $(formSub.querySelector('[name="jenis"]')).val();

            if(4 == currentStep && (1 == jenis || 4 == jenis)){
                stepper.goTo(2);
            }else{
                stepper.goPrevious(); // go previous step
            }
        });

        // form element
        var formSub = document.querySelector('#kt_stepper_example_basic_form');

        $(formSub.querySelector('[name="jenis"]')).on("change", (function() {
            var jenis = $(formSub.querySelector('[name="jenis"]'));

            //currentStep = stepper.getCurrentStepIndex();

            if(1 == jenis.val() || 4 == jenis.val())
            {
                element.querySelector('#div-scope').style.setProperty("display", "none", "important");
                stepper.goTo(4);
            } 
            else
            {
                element.querySelector('#div-scope').style.setProperty("display", "block", "important");
            }
        }));

        $(formSub.querySelector('[name="scope"]')).on("change", (function() {
            var scope = $(formSub.querySelector('[name="scope"]'));
            
            if(1 == scope.val())
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } 
            else if(2 == scope.val())
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } 
            else if(3 == scope.val())
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "none", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            } 
            else if(4 == scope.val())
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "none", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } 
            else if(5 == scope.val())
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "none", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "none", "important");
            } 
            else 
            {
                element.querySelector('#div-provinsi').style.setProperty("display", "block", "important");
                element.querySelector('#div-kota').style.setProperty("display", "block", "important");
                element.querySelector('#div-agama').style.setProperty("display", "block", "important");
                element.querySelector('#div-kantor').style.setProperty("display", "block", "important");
            }

        }));

        r.addEventListener("click", (function(e) {
            r.disabled = !0, r.setAttribute("data-kt-indicator", "on"), setTimeout((function() {
                r.removeAttribute("data-kt-indicator"), r.disabled = !1, Swal.fire({
                    text: "Form has been successfully submitted!",
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then((function() {
                    formSub.submit();
                }))
            }), 2e3)
        }));

        $("#tanggalSuratDasar").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format("YYYY"),10)
            }/*, function(start, end, label) {
                var years = moment().diff(start, "years");
                alert("You are " + years + " years old!");
            }*/
        );

        $("#tanggalHariLibur").daterangepicker();

        $("#searchData").keyup(function(event){
            var kategori = $("#searchKategori").val();
            var search   = $("#searchData").val();
            var url      = '{{url("/harilibur/administrasi?")}}'+kategori+'='+search;

            if (event.keyCode === 13) {
                window.location.href = url;
            }
        });

        $(".btnTambahPermohonan").click(function(){
            $('#nomorSuratDasar').val('').change();
            $('#keteranganSuratDasar').val('').change();
            $('#tanggalSuratDasar').val('');
            $('#tanggalHariLibur').val('');
            $('#keteranganHariLibur').val('').change();
            $('select[name=jenis]').val('').change();
            $('#scope').val('').change();
            $('#agamaIds').empty();
            $('#provinsiIds').empty();
            $('#kantorIds').empty();
            $('#kotaIds').empty();
        });

        $(".btnEditUsulan").click(function(){
            //$('#kt_modal_permohonan').modal('show');

            var idUsulan            = $(this).data('id');
            var url                 = $(this).data("url");

            window.location.href    = url+'?string='+btoa(idUsulan);

            /*var idHariLibur         = $(this).data('idharilibur')
            var nomorSuratDasar     = $(this).data('nomorsuratdasar');
            var tanggalSuratDasar   = $(this).data('tanggalsuratdasar');
            var ketSuratDasar       = $(this).data('keterangansuratdasar');
            var tanggalHariLibur    = $(this).data('tanggalharilibur');
            var keteranganHariLibur = $(this).data('keterangan-hari-libur');
            var jenisLibur          = $(this).data('jenis');
            var scope               = $(this).data('scope');
            var provinsiIds         = $(this).data('provinsi-ids');
            var agamaIds            = $(this).data('agama-ids');

            $('#idUsulan').val(idUsulan).change();
            $('#idHariLibur').val(idHariLibur).change();
            $('#nomorSuratDasar').val(nomorSuratDasar).change();
            $('#keteranganSuratDasar').val(ketSuratDasar).change();
            $('#tanggalSuratDasar').val(tanggalSuratDasar);
            $('#tanggalHariLibur').val(tanggalHariLibur);
            $('#keteranganHariLibur').val(keteranganHariLibur).change();
            $('select[name=jenis]').val(jenisLibur).change();
            $('#scope').val(scope).change();
            //$('#agamaIds').select2().val(agamaIds).trigger("change");

            //create selected provinsi item then set value on select2 jquery
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            if(0 < provinsiIds.length){
                var provinsiIdsSelect = $('#provinsiIds');

                provinsiIdsSelect.empty();
                //$('#provinsiIds').select2().val(null).trigger("change");

                // Fetch the preselected item, and add to the control
                for(let i = 0; i < provinsiIds.length; i++){
                    //$('#provinsiIds').append($("<option selected='selected'></option>").val(provinsiIds[i]).text("provinsi "+i)).trigger('change');
                    $.ajax({
                        type: 'GET',
                        url: '/harilibur/getProvinsi?provinsiId=' + provinsiIds[i]
                    }).then(function (data) {
                        // create the option and append to Select2
                        var option = new Option(data.text, data.id, true, true);
                        provinsiIdsSelect.append(option).trigger('change');

                        //alert(JSON.stringify(data));

                        // manually trigger the `select2:select` event
                        provinsiIdsSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });
                }
                //$('#provinsiIds').select2().val(provinsiIds).trigger("change");
            }

            if(0 < agamaIds.length){
                var agamaSelect = $('#agamaIds');

                //agamaSelect.empty();
                //$('#provinsiIds').select2().val(null).trigger("change");

                // Fetch the preselected item, and add to the control
                for(let i = 0; i < agamaIds.length; i++){
                    //
                    $.ajax({
                        type: 'GET',
                        url: '/harilibur/getAgama?agamaId=' + agamaIds[i]
                    }).then(function (data) {
                        // create the option and append to Select2
                        agamaSelect.append($("<option selected='selected'></option>").val(data.id).text(data.text)).trigger('change');

                        // manually trigger the `select2:select` event
                        agamaSelect.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        });
                    });
                }
                //$('#provinsiIds').select2().val(provinsiIds).trigger("change");
            }*/
            
        });

        $(".btnHapusUsulan").click(function(){
            var id          = $(this).data("id");
            var idHariLibur = $(this).data("idharilibur");
            var url         = $(this).data("url");
            var token       = $("meta[name='csrf-token']").attr("content");

            Swal.fire({
                title: 'Yakin akan menghapus data?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#C5584B',
                cancelButtonColor: '#B2B2B2',
                confirmButtonText: 'Hapus!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {
                            _token: token,
                            idUsulan: id,
                            idHariLibur:idHariLibur
                        },
                        success: function(response) {
                            //alert(JSON.stringify(response));
                            if(response['status']===1){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'data berhasil di hapus',
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                                $('#libur'+id).remove();

                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal menghapus data!' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                console.log(response)
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })
        });

        $(".btnKirimUsulan").click(function(){
            var id          = $(this).data("id");
            var url         = $(this).data("url");
            var token       = $("meta[name='csrf-token']").attr("content");
            //alert(id);
            //alert(url+'?string='+btoa(id));
            Swal.fire({
                title: 'Yakin akan mengirim usulan?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009ef7',
                cancelButtonColor: '#C5584B',
                confirmButtonText: 'Kirim!',
                cancelButtonText: 'Batal',
                reverseButtons: true
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'post',
                        url: url+'?string='+btoa(id),
                        data: {
                            _token: token,
                            idUsulan: id
                        },
                        success: function(response) {
                            //alert(JSON.stringify(response));
                            if(response['status']===1){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'usulan berhasil di kirim',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                window.location.href = document.URL;
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Gagal mengirim usulan!' ,
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            })

        });

        $(document).ready(function() {
            <?php
                if(count($errors) > 0){
                    echo "$('#kt_modal_permohonan').modal('show');";
                }   
            ?>

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            /*$(".select-provinsi").select2({
                ajax: {
                    url: "{{ url('/harilibur/getProvinsiList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        //console.log(JSON.stringify(data));
                        params.page = params.page || 1;
                        return {
                            results: data,
                            pagination: {
                            more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search for a Provinsi',
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });*/

            $(".select-provinsi").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getProvinsiList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });

            $(".select-kota").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getKotaList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });

            $(".select-kantor").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getKantorList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });

            $(".select-agama").select2({
                minimumInputLength: 1,
                ajax: {
                    url: "{{ url('/harilibur/getAgamaList') }}",
                    dataType: 'json',
                    delay: 250,
                    allowClear: true,
                    data: function (params) {
                        //alert(params.term);
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });
        });

    </script>
@endsection