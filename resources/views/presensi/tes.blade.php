@extends('layout.master')

@section('content')

<form class="form w-lg-500px mx-auto" id="kt_stepper_example_basic_form"  action="/harilibur/showPostSelect" method="post">
    @csrf
    <input type="text" class="form-control form-control-solid" name="idHariLibur" id="idHariLibur" placeholder="" value="{{ old('nomorSuratDasar') }}"/><br/>
    <input type="text" class="form-control form-control-solid" name="idUsulan" id="idUsulan" placeholder="" value=""/></br/>
    <div class="col-md-6"> 
        <select class="form-select select-provinsi"  data-placeholder="Select an option" data-allow-clear="true" name="provinsi[]" id="provinsi" multiple="multiple">
            <option value="HTML">HTML</option>
            <option value="Jquery">Jquery</option>
            <option value="CSS">CSS</option>
            <option value="Bootstrap 3">Bootstrap 3</option>
            <option value="Bootstrap 4">Bootstrap 4</option>
            <option value="Java">Java</option>
            <option value="Javascript">Javascript</option>
            <option value="Angular">Angular</option>
            <option value="Python">Python</option>
            <option value="Hybris">Hybris</option>
            <option value="SQL">SQL</option>
            <option value="NOSQL">NOSQL</option>
            <option value="NodeJS">NodeJS</option>
        </select> 
    </div>
    <br/>
    <div class="col-md-6"> 
        <select class="form-select "  data-placeholder="Select an option" data-allow-clear="true" name="mySelect2[]" id="mySelect2" multiple="multiple">

        </select> 
    </div>
    <br/>
    <input type="submit" value="Submit">
</form>

@endsection

@section('js')
<script type="text/javascript">

    //url: '{{ url("/harilibur/getJenisLibur") }}',

    var data = {
        "id": 1,
        "text": "Tyto alba",
        "genus": "Tyto",
        "species": "alba"
    };

    $('#mySelect2').trigger({
        type: 'select2:select',
        params: {
            data: data
        }
    });

    $(".select-provinsi").select2({
        ajax: {
            url: "{{ url('/harilibur/getProvinsiList') }}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data,
                    pagination: {
                    more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for a Jenis Libur',
        minimumInputLength: 1,
        templateResult: formatRepoJenisLibur,
        templateSelection: formatRepoJenisLiburSelection
    });

    function formatRepoJenisLibur (repo) {
        if (repo.loading) {
            return repo.text;
        }

        var $container = $(
            "<div class='select2-result-text'>" + repo.nama + "</div>"
        );

        return $container;
    }

    function formatRepoJenisLiburSelection (repo) {
        //$('#idHariLibur').val(repo.id).change();
        return repo.nama;
    }

</script>
@endsection