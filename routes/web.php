<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\TubelController;
use App\Http\Controllers\HariLiburController;
use Illuminate\Support\Facades\Route;

// Route::middleware(['CekAuth', 'web', 'MenuAccess'])->group(function () {
Route::middleware(['web'])->group(function () {
    Route::get('/beranda', [HomeController::class, 'index']);

    Route::get('/template/form', [TemplateController::class, 'form']);
    Route::get('/template/notifikasi', [TemplateController::class, 'notifikasi']);
    Route::get('/template/tabel', [TemplateController::class, 'tabel']);
    Route::get('/template/cari', [TemplateController::class, 'cari']);

    Route::get('/tubel/administrasi', [TubelController::class, 'index']);
    Route::get('/tubel/administrasi/getTubel', [TubelController::class, 'getDataTubel']);
    Route::get('/tubel/administrasi/{id}', [TubelController::class, 'detailTubel']);
    Route::get('/tubel/administrasi/{id}/update', [TubelController::class, 'updateTubel'])->name('tubel.update');
    Route::patch('/tubel/administrasi/{id}/update', [TubelController::class, 'updateTubelAction'])->name('tubel.update_action');

    Route::get('/tubel/administrasi/perguruanTinggi/{id}', [TubelController::class, 'getPerguruanTinggi']);
    Route::post('/tubel/administrasi/perguruanTinggi/{id}', [TubelController::class, 'addPerguruanTinggi']);
    Route::patch('/tubel/administrasi/perguruanTinggi/{id}', [TubelController::class, 'updatePerguruanTinggi']);
    Route::delete('/tubel/administrasi/perguruanTinggi/{id}', [TubelController::class, 'deletePerguruanTinggi']);

    Route::get('/tubel/administrasi/{id}/lps', [TubelController::class, 'detailLps']);
    Route::post('/tubel/administrasi/{id}/lps', [TubelController::class, 'addLps']);
    Route::patch('/tubel/administrasi/{id}/lps', [TubelController::class, 'updateLps']);
    Route::delete('/tubel/administrasi/{id}/lps', [TubelController::class, 'deleteLps']);
    Route::put('/tubel/administrasi/uploadkhs', [TubelController::class, 'uploadKhs']);
    Route::patch('/tubel/administrasi/uploadkhs', [TubelController::class, 'updateKhsFile']);
    Route::put('/tubel/administrasi/uploadlps', [TubelController::class, 'uploadLps']);
    Route::patch('/tubel/administrasi/uploadlps', [TubelController::class, 'updateLpsFile']);

    Route::post('/tubel/administrasi/lpskirim', [TubelController::class, 'kirimLps']);

    Route::post('/tubel/administrasi/permohonanaktif', [TubelController::class, 'addPermohonanAktif']);
    Route::post('/tubel/administrasi/permohonanaktif', [TubelController::class, 'PermohonanAktifKirim']);

    Route::get('/tubel/keppenempatancuti', [TubelController::class, 'penempatancuti']);
    Route::post('/tubel/keppenempatancuti', [TubelController::class, 'addKepCuti']);


    //teguh => cuti

    //tes

    Route::get('/tubel/tes', [TubelController::class, 'tesApi']);
    Route::post('/tubel/tes', [TubelController::class, 'tesApi']);
    Route::patch('/tubel/tes', [TubelController::class, 'tesApi']);
    Route::delete('/tubel/tes', [TubelController::class, 'tesApi']);

    Route::post('/tubel/administrasi/addcutibelajar', [TubelController::class, 'addCutiBelajar']);
    Route::post('/tubel/administrasi/addcutibelajaradmin', [TubelController::class, 'addCutiBelajarAdmin']);
    Route::post('/tubel/administrasi/updatecuti', [TubelController::class, 'updateCuti']);

    Route::get('/tubel/pegawai', [TubelController::class, 'monitoringTubel']);
    Route::get('/tubel/pegawai/{id}', [TubelController::class, 'detailTubelMon']);

    Route::get('/tubel/lps', [TubelController::class, 'lps']);
    Route::get('/tubel/lpstunggu', [TubelController::class, 'lpsMenunggu']);
    Route::get('/tubel/lpssetuju', [TubelController::class, 'lpsSetuju']);
    Route::get('/tubel/lpstolak', [TubelController::class, 'lpsDitolak']);
    Route::get('/tubel/lpskirim', [TubelController::class, 'lpsDikirim']);
    Route::get('/tubel/lpsdashboard', [TubelController::class, 'lpsDashboard']);
    Route::get('/tubel/telitilps/{id}', [TubelController::class, 'telitiLps']);
    Route::get('/tubel/detaillps/{id}', [TubelController::class, 'detailLps']);
    Route::post('/tubel/updateStatusLps', [TubelController::class, 'updateStatusLps']);
    Route::post('/tubel/administrasi/hapuscuti', [TubelController::class, 'deleteCuti']);

    Route::get('/tubel/cuti', [TubelController::class, 'cuti']);
    Route::get('/tubel/cutisetuju', [TubelController::class, 'cutiSetuju']);
    Route::get('/tubel/cutitolak', [TubelController::class, 'cutiTolak']);
    Route::get('/tubel/cutiselesai', [TubelController::class, 'cutiSelesai']);
    Route::get('/tubel/cutidashboard', [TubelController::class, 'cutiDashboard']);
    Route::post('/tubel/updatestatuscuti', [TubelController::class, 'updateStatusCuti']);
    Route::post('/tubel/updatecuti', [TubelController::class, 'updateCuti']);

    Route::post('/tubel/updatecuti', [TubelController::class, 'updateCuti']);

    Route::get('/harilibur/administrasi', [HariLiburController::class, 'index']);

    Route::post('/harilibur/administrasi/hapusUsulan',[HariLiburController::class, 'deletePermohonan']);

    Route::get('/harilibur/administrasi/showFormEditUsulan',[HariLiburController::class, 'showFormEditUsulan']);

    Route::post('/harilibur/administrasi/kirimUsulan',[HariLiburController::class, 'kirimUsulan']);

    Route::post('/harilibur/createPermohonan', [HariLiburController::class, 'createPermohonan'])->name('harilibur.createPermohonan');

    Route::get('/harilibur/testDelete',[HariLiburController::class, 'testDelete']);

    Route::get('/harilibur/getProvinsiList',[HariLiburController::class, 'getProvinsiList']);

    Route::get('/harilibur/getProvinsi',[HariLiburController::class, 'getProvinsi']);

    Route::get('/harilibur/getKotaList',[HariLiburController::class, 'getKotaList']);

    Route::get('/harilibur/getKantorList',[HariLiburController::class, 'getKantorList']);

    Route::get('/harilibur/getAgamaList',[HariLiburController::class, 'getAgamaList']);

    Route::get('/harilibur/getAgama',[HariLiburController::class, 'getAgama']);

    Route::get('/files/{file}', [TubelController::class, 'downloadFile']);

    Route::get('/harilibur/persetujuan', [HariLiburController::class, 'showPersetujuan']);

    Route::post('harilibur/persetujuan/approveUsulan',[HariLiburController::class, 'approveUsulan']);

    // Logout
    Route::get('/logout', [LoginController::class, 'logout']);
});


// Login
Route::middleware(['guest', 'web'])->group(function () {
    Route::get('/login', [LoginController::class, 'index']);
    Route::post('/login', [LoginController::class, 'login']);
});
