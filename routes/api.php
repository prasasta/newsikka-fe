<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/beranda', [homeController::class, 'index']);

Route::prefix('menu')->group(function () {
    Route::post('/getmenu', 'Api\MenuApi@getMenu');

    Route::post('/menuaccess', 'Api\MenuApi@menuAccess');

    Route::post('/getmenuaccess', 'Api\MenuApi@getMenuAccess');
});
