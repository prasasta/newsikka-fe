<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{ url('assets/dist') }}/assets/plugins/global/plugins.bundle.js"></script>
<script src="{{ url('assets/dist') }}/assets/js/scripts.bundle.js"></script>
<script src="{{ url('assets/dist') }}/assets/js/custom/widgets.js"></script>
<script src="{{ url('assets/dist') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Custom Javascript-->

<!--start::Custom Javascript-->
@yield('js')
<!--end::Custom Javascript-->
<script>
    jQuery(document).ready(function () {
        var arrMenu = <?=$menu['current']?>;
        var i;
        for (i = 0; i < arrMenu.length; i++) {
                $( "#menu-a-"+arrMenu[i] ).addClass( "active" );
                $( "#menu-acc-"+arrMenu[i] ).addClass( "menu-active-bg" );
                $( "#menu-acc2-"+arrMenu[i] ).addClass( "menu-active-bg" );
                $( "#menu-here-"+arrMenu[i] ).addClass( "here show" );
        }

    });
</script>
