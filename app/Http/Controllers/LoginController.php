<?php

namespace App\Http\Controllers;

use App\Helpers\LoginHelper;
use App\Helpers\MenuHelper;
use App\Models\MenuModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth/login');
    }

    // public function login(request $request)
    // {

    //     $request->validate([
    //         'username' => 'required',
    //         // 'password' => 'required'
    //     ]);

    //     if ($request == null) {
    //         return redirect()->back();
    //     }

    //     /* =================== User Login =================== */

    //     $username = $request->get('username');
    //     // $password = $request->get('password');
    //     $password = 'Pajak123';

    //     /* =================== Get token IAM =================== */
    //     $client1 = new Client([
    //         'base_uri' => 'https://iam.pajak.or.id',
    //         'timeout' => 3600,
    //     ]);

    //     try {
    //         $res1 = $client1->request('POST', '/api/authentication', [
    //             'json' => [
    //                 'username' => $username,
    //                 'password' => $password
    //             ]
    //         ]);
    //         $body = $res1->getBody();
    //         $body_array = json_decode($body);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         return redirect()->back()->with('success', 'gagal login');
    //         // return redirect()->back()->json(['status' => 0, 'message' => 'gagal login']);
    //     }

    //     // dd($body_array);

    //     // token IAM
    //     $token = $body_array->token;
    //     $refresh_token = $body_array->refresh_token;

    //     // get id (whoami)
    //     $client2 = new Client([
    //         'base_uri' => 'https://iam.pajak.or.id',
    //         'headers' => [
    //             'Authorization' => 'Bearer ' . $token,
    //             'Accept' => 'application/json',
    //         ],
    //         'timeout' => 3600
    //     ]);

    //     $res2 = $client2->request('POST', '/api/whoami');
    //     $body2 = $res2->getBody();
    //     $data = json_decode($body2);

    //     // get id pegawai from iam whoami
    //     $id_pegawai = $data->pegawai->id;
    //     $role = $data->roles[0];
    //     // dd($data);

    //     /* =================== Get Data SDM 00 =================== */
    //     $client3 = new Client([
    //         'base_uri' => 'https://hris-ref.pajak.or.id',
    //         'headers' => [
    //             'Authorization' => 'Bearer ' . $token,
    //             'Accept' => 'application/json',
    //         ],
    //         'timeout' => 3600
    //     ]);

    //     // dd($client2);

    //     $response2 = $client3->request('POST', '/get_data_by_pegawai_id', [
    //         'json' => [
    //             'pegawaiId' => $id_pegawai,
    //             ]
    //         ]);
    //     // dd($response2);

    //     $body2 = $response2->getBody();
    //     $data_login = json_decode($body2);
    //     // dd($data_login);

    //     $user['role'] = $data->data_role = $role;
    //     $user['data_login'] = $data_login;
    //     $user['token'] = $token;



    //     /* =================== Set Session =================== */
    //     if ($data_login->message == 'success') {
    //         // token iam
    //         session()->put('token_apps', $token);
    //         session()->put('role', $data->data_role);

    //         // menu access

    //         $menu_access = MenuHelper::getMenuAccess();

    //         if($menu_access == null){
    //             return abort(500);
    //         }


    //         // $menu = MenuModel::getMenu(1);

    //         session()->put('menu_access', $menu_access);
    //         // session()->put('menu', $menu);

    //         // session data pegawai
    //         $data_user['pegawaiId'] = $data_login->data->pegawaiId;
    //         $data_user['namaPegawai'] = $data_login->data->namaPegawai;
    //         $data_user['nip9'] = $data_login->data->nip9;
    //         $data_user['nip18'] = $data_login->data->nip18;
    //         $data_user['nik'] = $data_login->data->nik;
    //         $data_user['pangkatId'] = $data_login->data->pangkatId;
    //         $data_user['pangkat'] = $data_login->data->pangkat;
    //         $data_user['jabatanId'] = $data_login->data->jabatanId;
    //         $data_user['jabatan'] = $data_login->data->jabatan;
    //         $data_user['unitId'] = $data_login->data->unitId;
    //         $data_user['unit'] = $data_login->data->unit;
    //         $data_user['kantorId'] = $data_login->data->kantorId;
    //         $data_user['kantor'] = $data_login->data->kantor;

    //         session()->put('user', $data_user);

    //         return redirect('beranda');
    //     } else {
    //         return redirect()->back()->with('failed', 'Gagal login');
    //     }
    // }


    public function loginLama(Request $request)
    {

            // session data pegawai
            $data_user['pegawaiId'] = '02732b22-646b-4f22-8c74-909f7a69f78b';
            $data_user['namaPegawai'] = 'RAKHMANI WIDYAKUSUMA';
            $data_user['nip9'] = '833250907';
            $data_user['nip18'] = '198507302009012003';
            $data_user['nik'] = '1234567891212121';
            $data_user['pangkatId'] = '0e9b4465-0e0d-4a04-ab6d-3ad3b40f7bc3';
            $data_user['pangkat'] = 'Penata/IIIc';
            $data_user['jabatanId'] = '25d35666-f7fd-4053-89a1-ed9982f8471c';
            $data_user['jabatan'] = 'Kepala Subbag';
            $data_user['unitId'] = '0e9b4465-0e0d-4a04-ab6d-3ad3b40f7bc3';
            $data_user['unit'] = 'Subbagian Umum dan Kepatuhan Internal';
            $data_user['kantorId'] = '3948585d-422a-4b7b-bacc-0311bf8755b3';
            $data_user['kantor'] = 'KPP Pratama Bangka';

            session()->put('user', $data_user);
            return redirect('beranda');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('login');
    }

    // public function login(request $request)
    // {
    //     #validate
    //     $request->validate([
    //         'username' => 'required',
    //         // 'password' => 'required'
    //     ]);

    //     if ($request == null) {
    //         return redirect()->back();
    //     }

    //     #proses login
    //     $username = $request->get('username');
    //     // $password = $request->get('password');
    //     $password = 'Pajak123';

    //     $authiam= LoginHelper::iampost('authentication',[
    //                 'username' => $username,
    //                 'password' => $password,
    //         ]);

    //     #data iam
    //     $whoamiiam = LoginHelper::iampostbaru('/whoami',[],$authiam['data']->token);
    //     $id_pegawai = $whoamiiam['data']->pegawai->id;
    //     $nip9 = $whoamiiam['data']->pegawai->nip9;
    //     $role = $whoamiiam['data']->roles[0];

    //     #data href
    //     $dataHref = LoginHelper::get('master_pegawais/'.$nip9.'/data',$authiam['data']->token);

    //     #set session
    //     if ($dataHref['data']->message == "success") {
    //         # token iam
    //         session()->put('token_apps', $authiam['data']->token);
    //         session()->put('role',$role);

    //         # menu access

    //         $menu_access = MenuHelper::getMenuAccess();

    //         if($menu_access == null){
    //             return abort(500);
    //         }
    //         session()->put('menu_access', $menu_access);
    //         session()->put('user', $dataHref['data']->data);

    //         return redirect('beranda');
    //     } else {
    //         return redirect()->back()->with('failed', 'Gagal login');
    //     }
    // }

    public function login(request $request)
    {
        #validate
        $request->validate([
            'username' => 'required',
            // 'password' => 'required'
        ]);

        if ($request == null) {
            return redirect()->back();
        }

        #proses login
        $username = $request->get('username');
        // $password = $request->get('password');
        $password = 'Pajak123';
        
        $data =  Http::accept('application/json')
            ->withOptions([
                "verify"=>false,
            ])
            ->post('https://iam.simulasikan.com/api/authentication',[
            'username' => $username,
            'password' => $password,
            ]);
        
        $body = $data->getBody();
        $authiam['data'] = json_decode($body);

        #data iam
        $whoamiiam = LoginHelper::iampostbaru('/api/whoami',[],$authiam['data']->token);
        #$id_pegawai = $whoamiiam['data']->pegawai->id;
        $nip9 = $whoamiiam['data']->pegawai->nip9;        
        $role = $whoamiiam['data']->roles[0];

        

        #data href 
        $dataHref = LoginHelper::get('master_pegawais/'.$nip9.'/data',$authiam['data']->token);

        #golongan dan pangkat
        $pangkatId = $dataHref['data']->data->pangkatId;
        $pangkat = LoginHelper::get('pangkats/'.$pangkatId,$authiam['data']->token);
        $golongan = LoginHelper::get($pangkat['data']->golongan,$authiam['data']->token);
        
        $jabatan['golongan'] = $golongan['data']->nama;

        #data jabatans iam
        $dataJabatan =  LoginHelper::iamget('/api/jabatans/'.$dataHref['data']->data->jabatanId,$authiam['data']->token);
        $jabatan['kelasJabatan']=$dataJabatan['data']->nama;
        $jabatan['levelJabatan']=$dataJabatan['data']->level;
        $jabatan['kd_jabatan_id']=$dataJabatan['data']->legacyKode;
        
        #data kantor iam
        $dataKantors =  LoginHelper::iamget('/api/kantors?page=1&legacyKode='.$dataHref['data']->data->kantorLegacyKode,$authiam['data']->token);
        $jeniskantor = LoginHelper::iamget($dataKantors['data'][0]->jenisKantor,$authiam['data']->token);
        $kanwil     = LoginHelper::iamget($dataKantors['data'][0]->parent,$authiam['data']->token);
        $arrParent = explode('/',$dataKantors['data'][0]->parent);

        if($jeniskantor['data']->tipe != 'UPT'){ 
            $kantor['kdkpp'] = $dataKantors['data'][0]->legacyKodeKpp;
            $kantor['kdkanwil'] = $dataKantors['data'][0]->legacyKodeKanwil;
            $kantor['kantorInduk'] = $arrParent[3];           
            $kantor['nmkanwil'] = $kanwil['data']->nama;            
        }else{
            $kantor['kdkpp'] = '';
            $kantor['kdkanwil'] = '';
            $kantor['tipe'] = $jeniskantor['data']->tipe;
            $kantor['kantorInduk'] = $arrParent[3];
            $kantor['nmkanwil'] = $kanwil['data']->nama;            
        }

        #set session
        if ($dataHref['data']->message == "success") {

            # token iam
            session()->put('token_apps', $authiam['data']->token);
            session()->put('role',$role);

            # menu access
            $menu_access = MenuHelper::getMenuAccess();

            if($menu_access == null){
                return abort(500);
            }
            session()->put('menu_access', $menu_access);
            session()->put('user', $dataHref['data']->data,);
            session()->put('jabatan', $jabatan);
            session()->put('kantor', $kantor);
            
            return redirect('beranda');
        } else {
            return redirect()->back()->with('failed', 'Gagal login');
        }
    }

}
