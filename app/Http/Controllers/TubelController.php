<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper;
use App\Helpers\AppHelper;
use App\Models\MenuModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;

class TubelController extends Controller
{

    public function index()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        $pegawaiId = session()->get('user')['pegawaiId'];

        $data['tubel'] = ApiHelper::get('/tubel/pegawai/' . $pegawaiId);

        // dd($data);

        return view('tubel.index', $data);
    }

    public function getDataTubel()
    {
        $response = ApiHelper::post('/tubel/sikka/' . session()->get('user')['nip9'], [
            'pegawaiId' => session()->get('user')['pegawaiId'],
            'namaPegawai' => session()->get('user')['namaPegawai'],
            'nip18' => session()->get('user')['nip18'],
            'pangkatId' => session()->get('user')['pangkatId'],
            'namaPangkat' => session()->get('user')['pangkat'],
            'jabatanId' => session()->get('user')['jabatanId'],
            'namaJabatan' => session()->get('user')['jabatan'],
            'unitOrgId' => session()->get('user')['pangkatId'],
            'namaUnitOrg' => session()->get('user')['unit'],
            'kantorId' => session()->get('user')['kantorId'],
            'namaKantor' => session()->get('user')['kantor'],
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function detailTubel($id)
    {
        if (!empty($_GET['url'])) {
            $url = $_GET['url'];
        } else {
            $url = 'beranda';
        }

        if (!empty($_GET['tab'])) {
            $tab = $_GET['tab'];
        } else {
            $tab = 'lps';
        }

        // menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        // get data tubel pegawai
        $data['tubel'] = ApiHelper::get('/tubel/' . $id);
        $data['perguruantinggi'] = ApiHelper::get('/perguruan_tinggi_peserta/tubel/' . $id);
        $data['countperguruantinggi'] = count(ApiHelper::get('/perguruan_tinggi_peserta/tubel/' . $id));

        if ($tab == 'lps') {
            $data['lps'] = ApiHelper::get('/laporan/tubel/' . $id);
        } elseif ($tab == 'cuti') {
            $data['cuti'] = ApiHelper::get('/cuti_belajar/tubel/' . $id);
        } elseif ($tab == 'pengaktifan') {
            $data['permohonan_aktif'] = ApiHelper::get('/cuti_belajar/tubel/' . $id);
        }

        // if ($tab == 'lps') {
        //     $data['lps'] = ApiHelper::get('/laporan/tubel/' . $id);
        // } elseif ($tab == 'cuti') {
        //     $data['cuti'] = ApiHelper::get('/cuti_belajar/tubel/' . $id);
        // }

        $data['tab'] = $tab;
        $data['act'] = true;
        // referensi
        $data['ref_lokasi'] = ApiHelper::get('/lokasi_pendidikan');
        $data['ref_perguruantinggi'] = ApiHelper::get('/perguruan_tinggi');
        $data['ref_negara'] = ApiHelper::get('/negara_perguruan_tinggi');
        $data['ref_prodi'] = ApiHelper::get('/program_studi');

        // ddtubel
        // dd($data);

        $data['ptTubel'] = ApiHelper::get('/perguruan_tinggi_peserta/tubel/' . $id);

        return view('tubel.detail', $data);
    }

    public function updateTubel($id)
    {
        // menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        // get data tubel
        $data['tubel'] = ApiHelper::get('/tubel/' . $id);

        // get lokasi pendidikan
        $data['lokasi'] = ApiHelper::get('/lokasi_pendidikan');

        // get jenjang pendidikan
        $data['jenjang'] = ApiHelper::get('/jenjang_pendidikan');

        return view('tubel.update_tubel', $data);
    }

    public function updateTubelAction(Request $request, $id)
    {
        // dd(request()->all());

        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');

        $response = ApiHelper::patchBaru('/tubel/' . $id, [
            "pegawaiInputId" => $request->pegawaiInputId,
            "lamaPendidikan" => $request->lamaPendidikan,
            "totalSemester" => $request->totalSemester,
            "nomorSt" => $request->nomorSt,
            "tglSt" => $request->tglSt,
            "tglMulaiSt" => $request->tglMulaiSt,
            "tglSelesaiSt" => $request->tglSelesaiSt,
            "nomorKepPembebasan" => $request->nomorKepPembebasan,
            "tglKepPembebasan" => $request->tglKepPembebasan,
            "tmtKepPembebasan" => $request->tmtKepPembebasan,
            "emailNonPajak" => $request->emailNonPajak,
            "noHandphone" => $request->noHandphone,
            "alamatTinggal" => $request->alamatTinggal,
            "lokasiPendidikanId" => $request->lokasiPendidikanId,
            "flagStan" => 0,
            "programBeasiswa" => $request->programBeasiswa,
            "jenjangPendidikanId" => $request->jenjangPendidikanId,
            "kantorIdAsal" => $request->kantorIdAsal,
            "namaKantorAsal" => $request->namaKantorAsal,
            "jabatanIdAsal" => $request->jabatanIdAsal,
            "namaJabatanAsal" => $request->namaJabatanAsal
        ]);

        if ($response['status'] === 1) {
            return redirect('/tubel/administrasi/' . $id)->with('success', 'Data berhasil diupdate');
        } else {
            return redirect()->back()->with('error', 'Data gagal di tambahkan');
        }
    }

    public function addPerguruanTinggi(Request $request, $idtubel)
    {
        // dd(request()->all());
        $response = ApiHelper::postBaru('/perguruan_tinggi_peserta/tubel/' . $idtubel, [
            'pegawaiInputId' => session()->get('user')['pegawaiId'],
            'dataIndukTubelId' => $idtubel,
            'perguruanTinggiId' => $request->pt,
            'tglMulai' => $request->tglMulai,
            'programStudiId' => $request->prodi,
            'negaraPtId' => $request->negara,
        ]);

        if ($response['status'] === 1) {
            return redirect()->back()->with('success', 'Data berhasil di tambahkan');
        } else {
            return redirect()->back()->with('error', 'Data gagal di tambahkan');
        }
    }

    public function getPerguruanTinggi($id)
    {
        $response = ApiHelper::get('/perguruan_tinggi_peserta/' . $id);

        return response()->json($response);
    }

    public function updatePerguruanTinggi(Request $request)
    {
        $response = ApiHelper::patchBaru('/perguruan_tinggi_peserta/' . $request->id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId'],
            'dataIndukTubelId' => $request->idTubel,
            'perguruanTinggiId' => $request->idPt,
            'tglMulai' => $request->tglMulai,
            'programStudiId' => $request->idProdi,
            'negaraPtId' => $request->idNegara
        ]);

        return response()->json($response);
    }

    public function deletePerguruanTinggi($id)
    {
        $response = ApiHelper::delete('/perguruan_tinggi_peserta/' . $id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function addLps(Request $request, $id)
    {

        // dd(request()->all());

        $isi = [
            'semesterKe' => $request->semesterke,
            'ipk' => $request->ipk,
            'jumlahSks' => $request->jumlah_sks,
            'flagSemester' => $request->semester,
            'pathKhs' => "",
            'pathLps' => "",
            'tahunAkademik' => $request->tahun,
            'ptPesertaTubelId' => $request->perguruantinggi
        ];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        $response = ApiHelper::postBaru('/laporan/tubel/' . $id, $body);

        // dd($body);

        if ($response['status'] === 1) {
            return redirect('tubel/administrasi/' . $id)->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect('tubel/administrasi/' . $id)->with('error', $response['message']);
        }
    }

    public function updateLps(Request $request)
    {

        $filepath = $request->file('filelps')->getRealPath();
        $nama = $request->file('filelps')->getClientOriginalName();
        $mime = $request->file('filelps')->getMimeType();
        $ukuran = $request->file('filelps')->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        return response()->json($response->body());
    }

    public function deleteLps($id)
    {
        $response = ApiHelper::delete('/laporan/' . $id, [
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    public function uploadKhs(Request $request)
    {

        $filepath = $request->file('filekhs')->getRealPath();
        $nama = $request->file('filekhs')->getClientOriginalName();
        $mime = $request->file('filekhs')->getMimeType();
        $ukuran = $request->file('filekhs')->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        return response()->json($response->body());
    }

    public function updateKhsFile(Request $request)
    {
        $isi = [
            'pathKhs' => $request->file,
        ];

        $response = ApiHelper::patchBaru('/laporan/files/' . $request->id, $isi);

        return response()->json($response);
    }

    public function uploadLps(Request $request)
    {

        $filepath = $request->file('filelps')->getRealPath();
        $nama = $request->file('filelps')->getClientOriginalName();
        $mime = $request->file('filelps')->getMimeType();
        $ukuran = $request->file('filelps')->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        return response()->json($response->body());
    }

    public function updateLpsFile(Request $request)
    {

        $isi = [
            'pathLps' => $request->file,
        ];

        $response = ApiHelper::patchBaru('/laporan/files/' . $request->id, $isi);

        return response()->json($response);
    }

    public function kirimLps(Request $request)
    {

        // next case = Laporan dikirim
        $outputid = '14620ceb-ec11-4f5e-8ac5-d453d7a213fc';

        $isi = [
            'keterangan' => $request->output,
        ];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        $response = ApiHelper::postBaru('/laporan/' . $request->id . '/proses/' . $outputid, $body);

        return response()->json($response);
    }

    public function addCuti($id)
    {
        $response = ApiHelper::post('/tubel/sikka/' . $id, [
            'pegawaiId' => session()->get('user')['pegawaiId'],
            'namaPegawai' => session()->get('user')['namaPegawai'],
            'nip18' => session()->get('user')['nip18'],
            'pangkatId' => session()->get('user')['pangkatId'],
            'namaPangkat' => session()->get('user')['pangkat'],
            'jabatanId' => session()->get('user')['jabatanId'],
            'namaJabatan' => session()->get('user')['jabatan'],
            'unitOrgId' => session()->get('user')['pangkatId'],
            'namaUnitOrg' => session()->get('user')['unit'],
            'kantorId' => session()->get('user')['kantorId'],
            'namaKantor' => session()->get('user')['kantor'],
            'pegawaiInputId' => session()->get('user')['pegawaiId']
        ]);

        return response()->json($response);
    }

    //teguh

    public function monitoringTubel()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/pegawai');
        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
        } else {
            $page = 1;
        }
        $size = 5;

        $data['route'] = 'tubel/pegawai';

        $data['tubel'] = ApiHelper::get('/tubel?page=' . $page . '&size=' . $size);

        return view('tubel.monitoring_tubel', $data);
    }

    public function detailTubelMon($id)
    {


        if (!empty($_GET['url'])) {
            $url = $_GET['url'];
        } else {
            $url = 'beranda';
        }

        if (!empty($_GET['tab'])) {
            $tab = $_GET['tab'];
        } else {
            $tab = 'lps';
        }

        $data['tab'] = $tab;


        // menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), $url);
        //dd($_GET['url'] ) ;
        // get data tubel pegawai

        if ($tab == 'lps') {
            $data['lps'] = ApiHelper::get('/laporan/tubel/' . $id);
        } elseif ($tab == 'cuti') {
            $data['cuti'] = ApiHelper::get('/cuti_belajar/tubel/' . $id);
        }


        if ($tab == 'lps') {
            $data['lps'] = ApiHelper::get('/laporan/tubel/' . $id);
        } elseif ($tab == 'cuti') {
            $data['cuti'] = ApiHelper::get('/cuti_belajar/tubel/' . $id);
        }

        //dd($data['lps']);
        $data['tubel'] = ApiHelper::get('/tubel/' . $id);
        $data['perguruantinggi'] = ApiHelper::get('/perguruan_tinggi_peserta/tubel/' . $id);

        // referensi
        $data['ref_lokasi'] = ApiHelper::get('/lokasi_pendidikan');
        $data['ref_perguruantinggi'] = ApiHelper::get('/perguruan_tinggi');
        $data['ref_negara'] = ApiHelper::get('/negara_perguruan_tinggi');
        $data['ref_prodi'] = ApiHelper::get('/program_studi');

        // dd($data);

        return view('tubel.detail_tubel', $data);
    }


    public function lps()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');
        // $body =  AppHelper::generateBody(session()->get('user'));
        // dd($body);
        $data['route'] = 'tubel/lps';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/lps';

        $data['tubel'] = ApiHelper::get('/laporan/proses/persetujuan?page=' . $page . '&size=' . $size);

        $data['tab'] = 'tunggu';

        // dd($data);

        return view('tubel.lps_index', $data);
    }


    public function telitiLps($id)
    {
        // menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');
        $data['idLps'] = $id;
        $data['tab'] = 'teliti';
        $data['lps'] = ApiHelper::get('/laporan/' . $id);
        return view('tubel.detail_lps', $data);
    }

    public function detailLps($id)
    {
        // menu
        // if (!empty($_GET['url'])) {
        //     $url = $_GET['url'];
        // } else {
        //     $url = 'tubel/lps';
        // }

        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/administrasi');
        $data['idLps'] = $id;
        $data['tab'] = 'detail';
        $data['lps'] = ApiHelper::get('/laporan/' . $id);

        // dd($data);

        return view('tubel.detail_lps', $data);
    }

    public function lpsSetuju()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'setuju';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/lpssetuju';

        $data['tubel'] = ApiHelper::get('/laporan/proses/setuju?page=' . $page . '&size=' . $size);



        return view('tubel.lps_index', $data);
    }

    public function lpsDitolak()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'tolak';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/lpstolak';

        $data['tubel'] = ApiHelper::get('/laporan/proses/tolak?page=' . $page . '&size=' . $size);

        //dd($data['tubel']);

        return view('tubel.lps_index', $data);
    }


    public function lpsDikirim()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'kirim';

        return view('tubel.lps_index', $data);
    }

    public function lpsDashboard()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/lps');

        $data['route'] = 'tubel/lps';

        $data['tab'] = 'dashboard';

        return view('tubel.lps_index', $data);
    }


    public function updateStatusLps(Request $request)
    {

        $caseOutputId = $request->caseOutputId;

        $id = $request->id;

        $isi = ['keterangan' => $request->keterangan];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);

        $response = ApiHelper::post('/laporan/' . $id . '/proses/' . $caseOutputId, $body);

        return response()->json($response);
    }



    // public function pagination($page)
    // {
    //     //Pagination
    //     $batas = 10;
    //     $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    //     $halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;

    //     $previous = $halaman - 1;

    //     $next = $halaman + 1;

    //     $jumlah_data = 9;

    //     $total_halaman = ceil($jumlah_data / $batas);

    //     $nomor = $halaman_awal + 1;



    // }


    public function addCutiBelajar(Request $request)
    {

        // dd(request()->all());

        $file_cuti = $this->upload($request->file_cuti);

        if ($file_cuti['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('error', $file_cuti['message']);
        }

        // dd($file_cuti['body']);

        $file_sponsor = $this->upload($request->file_sponsors);

        if ($file_sponsor['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('error', $file_sponsor['message']);
        }

        // dd($file_sponsor['body']);

        $isi = [
            'alasanCuti' => $request->alasan_cuti,
            'tglMulaiCuti' => $request->tglMulaiSt,
            'tglSelesaiCuti' => $request->tglSelesaiSt,
            'nomorSuratKampus' => $request->surat_izin_cuti,
            'tglSuratKampus' => $request->tglSurat,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor,
            'tglSuratKepSponsor' => $request->tglSuratSponsor,
            'pathSuratIjinCutiKampus' => $file_cuti['body'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['body'],
            'dataIndukTubelId' => $request->idTubel
        ];

        // dd($isi);

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        $response = ApiHelper::postBaru('/cuti_belajar/pegawai', $body);

        if ($response['status'] = 1) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('error', $response['message']);
        }
    }

    public function addCutiBelajarAdmin(Request $request)
    {

        // dd(request()->all());

        $idTubel = '3d98c9ae-87ed-4f57-8d75-f1cc6e657fe0';

        $file_cuti = $this->upload($request->file_cuti_rekam);

        if ($file_cuti['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('error', $file_cuti['message']);
        }

        // dd($file_cuti['body']);

        $file_sponsor = $this->upload($request->file_sponsors_rekam);

        if ($file_sponsor['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=cuti#tab')->with('error', $file_sponsor['message']);
        }


        $isi = [
            'alasanCuti' => $request->alasan_cuti_rekam,
            'tglMulaiCuti' => $request->tgl_mulai_cuti_rekam,
            'tglSelesaiCuti' => $request->tgl_selesai_cuti_rekam,
            'nomorSuratKampus' => $request->surat_izin_cuti_kampus_rekam,
            'tglSuratKampus' => $request->tgl_surat_kampus_rekam,
            'nomorSuratSponsor' => $request->surat_izin_cuti_sponsor_rekam,
            'tglSuratKepSponsor' => $request->tgl_surat_sponsor_rekam,
            'pathSuratIjinCutiKampus' => $file_cuti['body'],
            'pathSuratIjinCutiSponsor' => $file_sponsor['body'],
            // 'dataIndukTubelId' => $request->idTubel
            'dataIndukTubelId' => $idTubel
        ];

        // dd($isi);

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        // dd($body);
        $response = ApiHelper::postBaru('/cuti_belajar/admin', $body);
        // dd($response);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function addkepCuti(Request $request)
    {
        // dd(request()->all());

        // split lokasi penempatan
        $lokasi_penempatan = $request->lokasi_penempatan_rekam;
        $result = explode('|', $lokasi_penempatan);

        $lokasi_penempatan_id = $result[0];
        $lokasi_penempatan_nama = $result[1];

        // dd($lokasi_penempatan_id);

        $file_kep = $this->upload($request->file_kep_rekam);

        if ($file_kep['status'] == 0) {
            return redirect()->back()->with('error', $file_kep['message']);
        }

        $isi = [
            'nomorSuratKep' => $request->nomor_kep_rekam,
            'tglSuratKep' => $request->tgl_kep_rekam,
            'pathKepPenempatan' => $file_kep['body'],
            'tglTmtPenempatan' => $request->tmt_penempatan_rekam,
            // 'kantorIdPenempatan' => $lokasi_penempatan_id,
            'kantorIdPenempatan' => '3fa85f64-5717-4562-b3fc-2c963f66afa6',
            'namaKantorPenempatan' => $lokasi_penempatan_nama
        ];

        // dd($isi);

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        // dd($body);
        $response = ApiHelper::postBaru('/cuti_belajar/penempatan/' . $request->id_cuti, $body);
        // dd($response);

        if ($response['status'] == 1) {
            return redirect()->back()->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateCuti(Request $request)
    {
        dd(request()->all());
    }

    public function deleteCuti(Request $request)
    {
        $body =  AppHelper::generateBody(session()->get('user'));

        $response = ApiHelper::deleteBaru('/cuti_belajar/' . $request->id, $body);

        return response()->json($response);
    }

    public function cuti()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/cuti';

        $data['cuti'] = ApiHelper::get('/cuti_belajar/proses/persetujuan?page=' . $page . '&size=' . $size);

        $data['tab'] = 'tunggu';

        return view('tubel.cuti_index', $data);
    }



    public function cutiSetuju()
    {

        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/cuti';

        $data['cuti'] = ApiHelper::get('/cuti_belajar/proses/terima?page=' . $page . '&size=' . $size);

        $data['tab'] = 'setuju';

        // dd($data);

        return view('tubel.cuti_index', $data);
    }


    public function cutiTolak()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/cuti';


        $data['cuti'] = ApiHelper::get('/cuti_belajar/proses/tolak?page=' . $page . '&size=' . $size);

        $data['tab'] = 'tolak';

        return view('tubel.cuti_index', $data);
    }


    public function cutiSelesai()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/cuti';

        $data['penempatancuti'] = ApiHelper::get('/cuti_belajar/proses/ditempatkan?page=' . $page . '&size=' . $size);

        $data['tab'] = 'selesai';

        // dd($data);

        return view('tubel.cuti_index', $data);
    }


    public function cutiDashboard()
    {
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/cuti');
        $data['route'] = 'tubel/cuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/cuti';


        $data['cuti'] = ApiHelper::get('/cuti_belajar/proses/persetujuan?page=' . $page . '&size=' . $size);

        $data['tab'] = 'dashboard';

        return view('tubel.cuti_index', $data);
    }

    public function updateStatusCuti(Request $request)
    {
        $caseOutputId = $request->caseOutputId;

        $id = $request->id;

        $isi = ['keterangan' => $request->keterangan];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);

        $response = ApiHelper::postBaru('/cuti_belajar/' . $id . '/proses/' . $caseOutputId, $body);

        // dd($response);

        return response()->json($response);
    }

    public function addPermohonanAktif(request $request)
    {
        // dd(request()->all());

        $file_pengaktifan = $this->upload($request->filePengaktifanLihat);

        if ($file_pengaktifan['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('error', $file_pengaktifan['message']);
        }

        // dd($file_pengaktifan['body']);

        $isi = [
            'tglMulaiAktif' => $request->tglMulaiAktif,
            'nomorSuratPengaktifanKembaliKampus' => $request->suratAktifKembali,
            'tglSuratPengaktifanKembaliKampus' => $request->tglSuratPengaktifan,
            'pathSuratPengaktifanKembaliKampus' => $file_pengaktifan['body'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        $response = ApiHelper::postBaru('/pengaktifan_kembali/', $body);

        if ($response['status'] == 1) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('success', 'Data Berhasil ditambah!');
        } else {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('error', $response['message']);
        }
    }

    public function updatePermohonanAktif(Request $request)
    {
        // dd(request()->all());

        $file_pengaktifan = $this->upload($request->file_pengaktifan);

        if ($file_pengaktifan['status'] == 0) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('error', $file_pengaktifan['message']);
        }

        // dd($file_pengaktifan['body']);

        $isi = [
            'tglMulaiAktif' => $request->tglMulaiAktif,
            'nomorSuratPengaktifanKembaliKampus' => $request->suratAktifKembali,
            'tglSuratPengaktifanKembaliKampus' => $request->tglSuratPengaktifan,
            'pathSuratPengaktifanKembaliKampus' => $file_pengaktifan['body'],
            'dataIndukTubelId' => $request->idTubel
        ];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);
        $response = ApiHelper::postBaru('/cuti_belajar/pegawai', $body);

        if ($response['status'] == 1) {
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('success', 'Data Berhasil ditambah!');
        } else {
            // echo 'ini';
            return redirect('tubel/administrasi/' . $request->idTubel . '?tab=pengaktifan#tab')->with('error', $response['message']);
        }
    }

    public function updateStatusPermohonanAktif(Request $request)
    {
        $caseOutputId = $request->caseOutputId;

        $id = $request->id;

        $isi = ['keterangan' => $request->keterangan];

        $body =  AppHelper::generateBody(session()->get('user'), $isi);

        $response = ApiHelper::postBaru('/cuti_belajar/' . $id . '/proses/' . $caseOutputId, $body);

        // dd($response);

        return response()->json($response);
    }

    public function penempatancuti()
    {
        // menu
        $data['menu'] = MenuModel::getMenu(session()->get('role'), 'tubel/keppenempatancuti');
        $data['route'] = 'tubel/keppenempatancuti';

        $size = 5;

        if (!empty($_GET['page'])) {
            $page =  $_GET['page'];
            $x = $page - 1;
            $data['start_no'] = ($x * $size) + 1;
            $data['page'] = $page;
        } else {
            $page = 1;
            $data['start_no'] = 1;
            $data['page'] = 1;
        }

        $data['link'] = 'tubel/keppenempatancuti';

        $data['penempatancuti'] = ApiHelper::get('/cuti_belajar/proses/terima?page=' . $page . '&size=' . $size);

        $data['tab'] = 'setuju';

        // dd($data);

        return view('tubel.penempatancuti_index', $data);
    }

    public function upload($file)
    {
        $filepath = $file->getRealPath();
        $nama = $file->getClientOriginalName();
        $mime = $file->getMimeType();
        $ukuran = $file->getSize();

        // validasi mime file
        if ($mime != 'application/pdf') {
            return ['status' => 0, 'message' => 'Format file bukan .pdf'];
        }

        // validasi ukuran 2mb
        if ($ukuran > 2097152) {
            return ['status' => 0, 'message' => 'Ukuran file lebih dari 2 MB'];
        }

        //dd($filepath);

        $response = Http::attach('file', file_get_contents($filepath), $nama)
            ->put(env('API_URL') . '/files');

        return ['status' => 1, 'body' => $response->body()];
    }


    public function tesApi()
    {
        $id = 'ab63931f-00d1-467f-8a1e-2c55137b6569';
        $response = ApiHelper::getBaru('/tubel/' . $id);
        // dd($response);
    }

    public function downloadFile($namafile)
    {

        // dd($tmp_direktori);
        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        // dd($response->headers()['Content-Type']);

        $headers = ['Content-Type: ' . $response->headers()['Content-Type'][0]];

        return response()->download($tmp_direktori, $namafile, $headers);
        // return response()->file($tmp_direktori, [
        //     'Content-Type' => 'application/pdf'
        // ]);
    }

    public function getFile($namafile)
    {

        $tmp_direktori = tempnam(sys_get_temp_dir(), $namafile);

        $response = Http::get(env('API_URL') . '/files/' . $namafile);

        file_put_contents($tmp_direktori, $response->body());

        return response()->file($tmp_direktori, [
            'Content-Type' => 'application/pdf'
        ]);
    }
}
