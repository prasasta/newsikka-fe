<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuApi extends Controller
{

    // public function getMenuAccess(Request $request)
    public function getMenuAccess()
    {
        # validator
        // $validator = Validator::make($request->all(), [
        //     'role' => 'required',
        // ]);

        # cek validation
        // if ($validator->fails()) {
        //     return response()->json(['status' => 0, 'message' => 'Data yang dikirim tidak valid']);
        // }

        // $role = $request->input('role');

        $arrMenu = [];

        $menu = DB::table('r_menu')
                    ->distinct()
                    ->select('r_menu.id','r_menu.route')
                    // ->join('r_menu', 'r_menu.id', '=', 'r_role_menu.id_menu')
                    // ->whereIn('r_role_menu.id_role', $role)
                    // ->where([
                    //     ['r_menu.aktif', '=', 1],
                    // ])
                    // ->whereNotNull('r_menu.route')
                    ->get();

        foreach ($menu as $menu) {
            $arrMenu[$menu->id] = $menu->route;
        }

        return response()->json(['status' => 1, 'message' => 'Data berhasil ditemukan', 'data' => $arrMenu]);

    }

    public function getMenu(Request $request)
    {
        # validator
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'route' => 'required',
        ]);

        # cek validation
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => 'Data yang dikirim tidak valid']);
        }

        $role = $request->input('role') == null ? ['99'] : $request->input('role');
        $route = $request->input('route') == null ? 'dashboard' : $request->input('route');;
        $addBreadcumbs = $request->input('bread') == null ? [] : $request->input('bread');

        ## prepare array menu
        $arrMenu = [];
        $arrMenuPost = [];

        ## statement
        $menu = DB::table('r_role_menu a')
                    ->distinct()
                    ->select('b.id','b.nm_menu','b.route','b.icon','b.sub_dari','b.no_urut','b.breadcumbs')
                    ->join('r_menu b', 'b.id', '=', 'a.id_menu')
                    ->whereIn('a.id_role', $role)
                    ->where([
                        ['b.aktif', '=', 1],
                    ])
                    ->orderBy('b.no_urut', 'asc')
                    ->get();
                    // return response()->json(['status' => 0, 'message' => 'Proses pengambilan data gagal, harap ulangi kembali']);

        $currentMenu = DB::table('r_role_menu')
                            ->select('r_menu.breadcumbs','r_menu.nm_menu','r_menu.id')
                            ->join('r_menu', 'r_menu.id', '=', 'r_role_menu.id_menu')
                            ->where('r_menu.route', '=', $route)
                            ->first();
                            // print_r($route);die;

        $breadArr = \json_decode($currentMenu->breadcumbs);

        $bread = DB::table('r_role_menu')
                    ->distinct()
                    ->select('r_menu.nm_menu')
                    ->join('r_menu', 'r_menu.id', '=', 'r_role_menu.id_menu')
                    ->whereIn('r_menu.id', $breadArr)
                    ->get();

        $breadcumbs = array();

        try {

            foreach ($bread as $value) {
                \array_push($breadcumbs,$value->nm_menu);
            }

            foreach ($menu as $menu) {
                if(! array_key_exists($menu->sub_dari, $arrMenu)){
                    //$arrMenu[$menu->sub_dari] = array();
                }
                //array_push($arrMenu[$menu->sub_dari],array($menu->id,$menu->nm_menu,$menu->route,$menu->icon,$menu->sub_dari,$menu->no_urut));
                $arrMenu[$menu->sub_dari][$menu->id] = array($menu->id,$menu->nm_menu,$menu->route,$menu->icon,$menu->sub_dari,$menu->no_urut,$menu->breadcumbs);
            }

            $arrMenuPost['menu'] = $arrMenu[0];
            //unset($arrMenu[0]);

            foreach ($arrMenuPost['menu'] as $key => $value) {
                if(array_key_exists($key, $arrMenu)){
                    $arrMenuPost['menu'][$key]['sub'] = $arrMenu[$key];
                    //unset($arrMenu[$key]);

                    foreach ($arrMenuPost['menu'][$key]['sub'] as $key2 => $value2) {
                        if(array_key_exists($key2, $arrMenu)){
                            $arrMenuPost['menu'][$key]['sub'][$key2]['sub'] = $arrMenu[$key2];
                            //unset($arrMenu[$key2]);

                        }else{
                            $arrMenuPost['menu'][$key]['sub'][$key2]['sub'] = [];
                        }
                    }

                }else{
                    $arrMenuPost['menu'][$key]['sub'] = [];
                }
            }

            # tambah bread tambahan
            if(!empty($addBreadcumbs)){
                foreach ($addBreadcumbs as $addValue) {
                    \array_push($breadcumbs,$addValue);
                }
            }

            $arrMenuPost['current'] = $currentMenu->breadcumbs;
            $arrMenuPost['title'] = $currentMenu->nm_menu;
            $arrMenuPost['breadcumbs'] = $breadcumbs;

            return response()->json(['status' => 1, 'message' => 'Data berhasil ditemukan', 'data' => $arrMenuPost]);

        } catch (\Throwable $th) {
            return response()->json(['status' => 0, 'message' => 'Proses pengambilan data gagal, harap ulangi kembali']);
        }

    }

    private function searchMenu($route){

        # count path
        $arrRoute = explode("/",$route);
        $c = count($arrRoute)-1;
        $currRoute = '';

        # loop path
        for ($i=$c; $i >= 0; $i--) {

            for ($i2=0; $i2 <= $i ; $i2++) {
                $currRoute .= $arrRoute[$i2].'/';
            }

            $currRoute = substr($currRoute,0,-1);
            $menu = DB::table('r_menu')->where([['route',$currRoute],['aktif','1']])->first();

            if($menu !== null){
                return $menu->id;
            }

            $currRoute = '';
        }

        return false;

    }

    public function menuAccess(Request $request)
    {
        # validator
        $validator = Validator::make($request->all(), [
            'route' => 'required',
            'id_role' => 'required',
        ]);

        # cek validation
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => 'Data yang dikirim tidak valid']);
        }

        $route = $request->input('route');
        $role = $request->input('id_role');

        $idMenu = $this->searchMenu($route);

        if ($idMenu == false) {
            return response()->json(['status' => 0, 'message' => 'Menu tidak ditemukan']);
        }

        try {

            $cek = DB::table('r_role_menu')
                ->whereIn('id_role', $role)
                ->where('id_menu',$idMenu)
                ->count();

            return response()->json(['status' => 1, 'message' => 'Data berhasil ditemukan', 'data' => $cek]);

        } catch (\Throwable $th) {
            return response()->json(['status' => 0, 'message' => 'Proses pengambilan data gagal, harap ulangi kembali']);
        }

    }

}
