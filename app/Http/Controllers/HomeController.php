<?php

namespace App\Http\Controllers;

use App\Helpers\MenuHelper;
use App\Models\MenuModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {

        $menu = MenuModel::getMenu(session()->get('role'),'beranda');

         //dd(session()->all());
        // $menu = MenuHelper::getMenu(session()->get('role'), 'Beranda');

        // $data = DB::table('frontend.r_role_menu a')
        //     // ->distinct()
        //     ->select('b.id', 'b.nm_menu', 'b.route', 'b.icon', 'b.sub_dari', 'b.no_urut')
        //     ->join('r_menu b', 'b.id', '=', 'a.id_menu')
        //     ->whereIn('a.id_role', [1])
        //     ->where([
        //         ['b.aktif', '=', '1'],
        //     ])
        //     ->orderBy('b.no_urut', 'asc')
        //     ->get();

        // $menu = DB::select(DB::raw("select
        //                 b.id,
        //                 b.nm_menu,
        //                 b.route,
        //                 b.icon,
        //                 b.sub_dari,
        //                 b.no_urut,
        //                 b.breadcumbs
        //             from
        //                 r_role_menu a
        //             inner join r_menu b on
        //                 b.id = a.id_menu
        //             where
        //                 a.id_role in (1)
        //                 and (b.aktif = '1')
        //             order by
        //                 b.no_urut asc"
        //     ));

        // dd($menu);


        // $menu = MenuHelper::getMenu()->get();
        // $menu['menu'] = $menu;

        // $menu = DB::table('r_menu')->get()->where('aktif', '1');
        $data['menu'] = $menu;

        // $id_role = ['1', '2'];

        // $data = MenuModel::getMenu($id_role);

        // $data['session'] = session()->all();

        // dd($data);

        return view('beranda', $data);
    }
}
