<?php

namespace App\Http\Middleware;

use App\Helpers\MenuHelper;
use Illuminate\Support\Facades\Route;
use Closure;

class MenuAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $route = Route::current()->uri;

        $data = [
            '1' => 'beranda',
            '2' => 'tubel',
            '3' => 'tubel/administrasi',
            '4' => 'logout',
            '5' => 'template/notifikasi',
            '6' => 'pages'
        ];

        // $cekAkses = MenuHelper::MenuAccess($route,session()->get('id_role'));
        // $cekAkses = MenuHelper::CekMenuAccess($route,session()->get('menu_access'));

        // mas satria
        $cekAkses = MenuHelper::CekMenuAccess($route, $data);

        if ($cekAkses == false) {
            return abort(403);
        }

        return $next($request);
    }
}
