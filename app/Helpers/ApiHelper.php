<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\SidupakfilesClientHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\FacadesStorage;

class ApiHelper
{

    public static function instance()
    {
        return new ApiHelper();
    }

    public static function post(String $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->post(env('SIDUPAK_API_URL').$route, $body);
        // $request = Http::withToken(session()->get('token_jwt'))->post('http://localhost:8000/newsikka/api/'.$route, $body);
        // $request = Http::post(env('API_URL') . $route, $body);
        $request = Http::post('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function patch(String $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->patch(env('SIDUPAK_API_URL').$route, $body);
        // $request = Http::withToken(session()->get('token_jwt'))->patch('http://localhost:8000/newsikka/api/'.$route, $body);
        // $request = Http::patch(env('API_URL') . $route, $body);
        $request = Http::patch('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function get(String $route)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->get(env('SIDUPAK_API_URL').$route);
        // $request = Http::withToken(session()->get('token_jwt'))->get('http://localhost:8000/newsikka/api/'.$route);
        //$request = Http::get(env('API_URL') . $route);
        $request = Http::get('http://localhost:8080' . $route);

        # cek status servis
        if (!$request->successful()) {
            return ['status' => 0, 'message' => 'Error Web Service!'];
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }

    public static function delete(String $route, array $body)
    {
        // $request = Http::withToken(session()->get('token_jwt'))->get(env('SIDUPAK_API_URL').$route);
        // $request = Http::withToken(session()->get('token_jwt'))->get('http://localhost:8000/newsikka/api/'.$route);
        // $request = Http::delete(env('API_URL') . $route, $body);
        $request = Http::delete('http://localhost:8080' . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            // return ['status' => 0, 'message' => 'Error Web Service!'];
            return json_decode($body = $request->getBody());
        }

        $body = $request->getBody();

        // return $request->json();
        return json_decode($body);
    }




    public static function tespostDS()
    {

        //dd(fopen(Storage::path('pak\pak.pdf'), 'r'));

        $file = Storage::exists('pak\pak.pdf');

        if ($file == false) {
            return ['status' => 0, 'message' => 'Gagal', 'data' => ['error' => 'file sumber tidak ditemukan', 'status_code' => 400]];
        }

        $response  = Http::withHeaders([
            'Authorization' => 'basic ' . env('ESIGN_PIN')
        ])
            ->attach(
                'file',
                file_get_contents(Storage::path('pak\pak.pdf'))
            )
            ->post(env('ESIGN_API_URL') . '?nik=3575013108930001&passphrase=satria1993&jenis_dokumen=dupak&nomor=Dupak-1/2020&tujuan=Sekretariat penilai&perihal=dupak&info=dupak&tampilan=invisible', ['multipart' => ['contents' => fopen(Storage::path('pak\pak.pdf'), 'r'), 'name' => 'file']]);

        # cek status servis
        if (!$response->successful()) {
            return ['status' => 0, 'message' => 'Gagal', 'data' => $response->json()];
        }

        // $fileDS = $response->body();
        // $stringBody = (string) $fileDS;

        // Storage::put('pak/pakDS.pdf',$stringBody);

        // $path = Storage::path('pak/pak_'.$request->input('id').'.pdf');

        // // inisiasi Object storage
        // $objStore = new SidupakfilesClientHelper();

        // // save file
        // $upload = $objStore->putFile($path,'pak_'.$request->input('id'),'application/pdf',0,'Pak',15,'.pdf');

        return ['status' => 1, 'message' => 'Berhasil', 'data' => $response];
    }

    public static function postDS($path, String $nik, String $passphrase, String $jenis, String $nomor, String $tujuan, String $perihal, String $info)
    {

        $file = Storage::exists($path);

        if ($file == false) {
            return ['status' => 0, 'message' => 'Gagal', 'data' => ['error' => 'file sumber tidak ditemukan', 'status_code' => 400]];
        }

        $response  = Http::withHeaders([
            'Authorization' => 'basic ' . env('ESIGN_PIN')
        ])
            ->attach(
                'file',
                file_get_contents(Storage::path($path))
            )
            ->post(env('ESIGN_API_URL') . '?nik=' . $nik . '&passphrase=' . $passphrase . '&jenis_dokumen=' . $jenis . '&nomor=' . $nomor . '&tujuan=' . $tujuan . '&perihal=' . $perihal . '&info=' . $info . '&tampilan=invisible', ['multipart' => ['contents' => fopen(Storage::path($path), 'r'), 'name' => 'file']]);

        # cek status servis
        if (!$response->successful()) {
            return ['status' => 0, 'message' => 'Gagal', 'data' => $response->json()];
        }

        // $fileDS = $response->body();
        // $stringBody = (string) $fileDS;

        // Storage::put('pak/pakDS.pdf',$stringBody);

        // $path = Storage::path('pak/pak_'.$request->input('id').'.pdf');

        // // inisiasi Object storage
        // $objStore = new SidupakfilesClientHelper();

        // // save file
        // $upload = $objStore->putFile($path,'pak_'.$request->input('id'),'application/pdf',0,'Pak',15,'.pdf');

        // return $response->successful();

        return ['status' => 1, 'message' => 'Berhasil', 'data' => $response];
    }

    public static function postBaru(String $route, array $body)
    {
        $request = Http::post(env('API_URL') . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();


        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patchBaru(String $route, array $body)
    {
        $request = Http::patch(env('API_URL') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function getBaru(String $route)
    {
        $request = Http::get(env('API_URL') . $route);

        //dd($request->getBody());

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function getFile(String $route)
    {
        $request = Http::get(env('API_URL') . $route);

        //dd($request->getBody());

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        // dd($request);

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function put(String $route, array $body)
    {
        $request = Http::put(env('API_URL') . $route, $body);

        if (!$request->successful()) {
            $x = json_decode($request);

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function deleteBaru(String $route, array $body)
    {
        $request = Http::delete(env('API_URL') . $route, $body);

        # cek status servis
        if (!$request->successful()) {
            $x = json_decode($request);


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #method untuk create Bearer Token IAM
    public static function getBearerIAM(){

        $userData = session()->get('user');

        $body = [
                'username' => 'root',
                'password' => 'toor'
        ];

        $request =  Http::accept('application/json')
                ->withOptions(["verify"=>false])
                ->post(env('API_URL_IAM').'authentication',[
                    'username' => 'root',
                    'password' => 'toor',
                ]);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body               = $request->getBody();
        
        $authiam['data']    = json_decode($body);

        session()->put('bearerToken', $authiam);

        return $authiam;
    }

    #function get data SDM 0.1.2
    public static function getDataSDM012(String $route, String $token){

        $request =  Http::accept('application/json')
                ->withOptions([
                    "verify"=>false,
                    'headers'   => [
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ])
                ->get(env('API_URL_SDM012').$route);

        ///dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {
            
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #get data id+json format
    public static function getDataSDM012IdJson(String $route, String $token){

        $request =  Http::accept('application/ld+json')
                ->withOptions([
                    "verify"=>false,
                    'headers'   => [
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ])
                ->get(env('API_URL_SDM012').$route);

        ///dd($request->getReasonPhrase().$request->getStatusCode());

        # cek status servis
        if ("OK" != $request->getReasonPhrase()) {
            
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        #dump($request);die();

        $data = (array) json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function post data SDM 0.1.2
    public static function postDataSDM012(String $route, String $token, Array $body){

        $request =  Http::accept('application/json')
                ->withOptions([
                    "verify"=>false,
                    'headers'   => [
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ])
                ->post(env('API_URL_SDM012').$route, $body);

        #dump($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis
        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {
            
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function delete data SDM 0.1.2
    public static function deleteDataSDM012(String $route, String $token){
        $request =  Http::accept('application/json')
                ->withOptions([
                    "verify"=>false,
                    'headers'   => [
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ])
                ->delete(env('API_URL_SDM012').$route);

        # dump($request->getReasonPhrase().'|'.env('API_URL_SDM012').$route.'|'.$request->getStatusCode());

        # cek status servis
        if ("No Content" != $request->getReasonPhrase()) {
            
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function patch data SDM 0.1.2
    public static function patchDataSDM012(String $route, String $token, Array $body){

        $request =  Http::accept('application/json')
                ->withOptions([
                    "verify"    => false,
                    'headers'   => [
                        'Authorization' => 'Bearer ' . $token,
                        'Content-Type'  => 'application/merge-patch+json',
                    ]
                ])->patch(
                    env('API_URL_SDM012').$route, 
                    $body
                );

        #dd($request->getReasonPhrase().$request->getStatusCode());
        # cek status servis

        if ("OK" != $request->getReasonPhrase() && "Created" != $request->getReasonPhrase()) {
            
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    #function cek token iam
    public static function cekTokenIAM(){
        if(session()->exists('bearerToken')){
            $authiam    = session()->get('bearerToken');

             // Cek validitas token
            list($tokenHeader, $payload, $signature) = explode('.', $authiam['data']->token);
            $decodedPayload = json_decode(base64_decode($payload), true);
            $tokenExpired = $decodedPayload['exp'];

            // kalau token sudah expired, refresh minta token baru
            if (time() > ($tokenExpired - 60)) {
                $authiam    = self::getBearerIAM();
            }
        }else{
            $authiam    = self::getBearerIAM();
        }

        return $authiam;
    }
}
