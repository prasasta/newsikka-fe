<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\SidupakfilesClientHelper;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use Illuminate\Support\FacadesStorage;

class LoginHelper
{

    public static function instance()
    {
        return new LoginHelper();
    }

    public static function post(String $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'post',
            $route,
            $body,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer {$header}",
                    'Accept' => 'application/json',
                ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());

            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function patch(String $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'patch',
            $route,
            $body,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer {$header}",
                    'Accept' => 'application/json',
                ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function get(String $route, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'GET',
            $route,
            [
                "verify"=>false,
                'headers' =>
                [
                    'Authorization' => "Bearer {$header}",
                    'Accept' => 'application/json',
                ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function delete(String $route, array $body, $header)
    {
        $client3 = new Client([
            // 'base_uri' => 'https://hris-ref.pajak.or.id/',
            'base_uri' => 'https://hris-ref.simulasikan.com',
        ]);

        $request = $client3->request(
            'delete',
            $route,
            $body,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer {$header}",
                    'Accept' => 'application/json',
                ]
            ]
        );

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }


        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampost(String $route, array $body)
    {
        // $request = Http::post('https://iam.pajak.or.id/api/' . $route, $body);
        $request = Http::post('https://iam.simulasikan.com' . $route, $body);

        # cek status servis
        if (!$request->successful()) {

            $x = json_decode($request);


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();
        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampostbaru(String $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            "verify"  =>false,
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('post', $route);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iampatch(String $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('patch', $route, $body);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iamget(String $route, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            "verify"=>false,
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('get', $route);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }

    public static function iamdelete(String $route, array $body, $header)
    {
        // dd($route, $body, $header);
        $client3 = new Client([
            // 'base_uri' => 'https://iam.pajak.or.id',
            'base_uri' => 'https://iam.simulasikan.com',
            'headers' => [
                'Authorization' => 'Bearer ' . $header,
                'Accept' => 'application/json',
            ]
        ]);

        $request = $client3->request('delete', $route, $body);

        # cek status servis
        if ($request->getReasonPhrase() != "OK") {
            $x = json_decode($request->getBody());


            $message = (isset($x->apierror)) ? $x->apierror->message : 'Error Web Services';

            return ['status' => 0, 'message' => $message];
        }

        $body = $request->getBody();

        $data = json_decode($body);

        return ['status' => 1, 'data' => $data];
    }
}
