<?php

namespace App\Helpers;

class AppHelper
{
    public static function instance()
    {
        return new AppHelper();
    }

    public function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB')
    {
        date_default_timezone_set('Asia/Jakarta');

        if (trim($timestamp) == '') {
            $timestamp = time();
        } elseif (!ctype_digit($timestamp)) {
            $timestamp = strtotime($timestamp);
        }

        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace("/S/", "", $date_format);

        $pattern = array(
            '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
            '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
            '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
            '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
            '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
            '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
            '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
            '/November/', '/December/',
        );

        $replace = array(
            'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
            'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
            'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
        );

        $date = date($date_format, $timestamp);
        $date = preg_replace($pattern, $replace, $date);
        $date = "{$date} {$suffix}";

        return $date;
    }

    public static function generateBody($user, $isi = null)
    {
        $arrpeg = [
            'pegawaiId' => $user['pegawaiId'],
            'namaPegawai' => $user['namaPegawai'],
            'nip18' => $user['nip18'],
            'pangkatId' => $user['pangkatId'],
            'namaPangkat' => $user['pangkat'],
            'jabatanId' => $user['jabatanId'],
            'namaJabatan' => $user['jabatan'],
            'unitOrgId' => $user['pangkatId'],
            'namaUnitOrg' => $user['unit'],
            'kantorId' => $user['kantorId'],
            'namaKantor' => $user['kantor'],
            'pegawaiInputId' => $user['pegawaiId']
        ];
        if ($isi != null) {
            $arrisi = $isi;
        } else {
            $arrisi = [];
        }
        $body = array_merge($arrpeg, $arrisi);

        return $body;
    }

    public static function getScopeLibur(){
        $arrayScope = array(
            1 => 'Provinsi',
            2 => 'Provinsi + Kota',
            3 => 'Kantor',
            4 => 'Agama',
            5 => 'Provinsi + Agama',
            6 => 'Provinsi + Kota + Agama'
        );

        return $arrayScope;
    }

    #method untuk konversi tanggal ke format Y-m-d dari format m/d/y
    public static function convertDate(String $tanggal){
        $arr_tanggal = explode('/',$tanggal);

        return $arr_tanggal[2].'-'.$arr_tanggal[0].'-'.$arr_tanggal[1];
    }

    #method untuk konversi tanggal dari format Y-m-dTH:i:s dari format d/m/y
    public static function convertDateDMY(String $tanggal){
        $arr_tanggal_full   = explode('T',$tanggal);
        $arr_tanggal        = explode('-',$arr_tanggal_full[0]);

        return $arr_tanggal[2].'/'.$arr_tanggal[1].'/'.$arr_tanggal[0];
    }

    #method untuk konversi format tanggal dari format Y-m-dTH:i:s dari format m/d/y
    public static function convertDateMDY(String $tanggal){
        $arr_tanggal_full   = explode('T',$tanggal);
        $arr_tanggal        = explode('-',$arr_tanggal_full[0]);

        return $arr_tanggal[1].'/'.$arr_tanggal[2].'/'.$arr_tanggal[0];
    }

}
