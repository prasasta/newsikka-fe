<?php

namespace App\Helpers;

/**
 * Class HCPClientHelper
 * class untuk pengelolaan object storage, identitas dari variable .env
 */
class HCPClientHelper
{
    /** @var null user object storage */
    private $uid;
    /** @var null password object storage */
    private $pwd;
    /** @var null hostname server object storage */
    private $host;

    private $info;
    private $opts = [];

    /**
     * HCPClientHelper constructor.
     *
     * @param null $uid
     * @param null $pwd
     * @param null $host
     */
    public function __construct($uid = null, $pwd = null, $host = null)
    {
        $this->uid = $uid;
        $this->pwd = $pwd;
        $this->host = $host;

        if (empty($uid) or empty($pwd) or empty($host)) {
            $this->uid = env('OBJ_STORAGE_UID');
            $this->pwd = env('OBJ_STORAGE_PWD');
            $this->host = env('OBJ_STORAGE_HOST');
        }
    }

    /**
     * Set opsi default curl yang dikirim ke driver
     */
    private function setDefaultOpts()
    {
        // Untuk saat ini, verifikasi SSL dimatikan karena sertifikat SSL object storage
        $this->opts = [
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => ['Authorization: HCP ' . base64_encode($this->uid) . ':' . md5($this->pwd)]
        ];
    }

    /**
     * Generate url untuk disimpan di object storage
     * @param $alias string nama file yang akan disimpan
     * @param null $folder
     *
     * @return string
     */
    private function genUrl($alias, $folder = null)
    {
        $folder = trim($folder, '/');
        return $this->host . '/' . (empty($folder) ? null : $folder . '/') . $alias;
    }

    public function getLastInfo()
    {
        return $this->info;
    }

    /**
     * Inisiasi curl
     * @return false|resource
     */
    private function getHCPCurl()
    {
        $ch = curl_init();
        curl_setopt_array($ch, $this->opts);
        return $ch;
    }

    /**
     * Simpan file ke object storage
     * @param $file string lokasi file
     * @param $alias string nama file yang akan disimpan di object storage
     * @param null $folder nama folder lokasi file akan disimpan
     *
     * @return bool
     */
    public function putFromFile($file, $alias, $folder = null)
    {
        // Buka filenya
        $fp = fopen($file, 'r');

        // Buat Uri untuk penyimpanan
        $uri = $this->genUrl($alias, $folder);


        // Set curl option
        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_HTTPGET] = false;
        $this->opts[CURLOPT_PUT] = true;
        $this->opts[CURLOPT_INFILE] = $fp;
        $this->opts[CURLOPT_INFILESIZE] = filesize($file);

        // Nyalakan curl dan masukkan opsinya
        $ch = $this->getHCPCurl();

        curl_setopt_array($ch, $this->opts);

        // Jalankan curl-nya
        $ret = curl_exec($ch);

        // Ambil informasi
        $this->info = curl_getinfo($ch);

        // Tutup curl dan file
        curl_close($ch);
        fclose($fp);

        if ($this->info['http_code'] != 201)
            return $this->info;

        return $uri;
    }

    /**
     * Simpan dari konten
     * @param $content string
     * @param $alias string
     * @param null $mime
     * @param null $folder
     *
     * @return bool|string
     */
    private function putFromContent($content, $alias, $mime = null, $folder = null)
    {
        // Buka konten ke buffer memory
        $fp = fopen('php://memory', 'rw');

        // Tulis ke memory
        fwrite($fp, $content);
        rewind($fp);

        // Generate uri-nya
        $uri = $this->genUrl($alias, $folder);

        // Set curl option
        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_HTTPGET] = false;
        $this->opts[CURLOPT_PUT] = true;
        $this->opts[CURLOPT_INFILE] = $fp;
        $this->opts[CURLOPT_INFILESIZE] = strlen($content);

        // Kalau ada mime type, tambahkan
        if (!empty($mime))
            $this->opts[CURLOPT_HTTPHEADER] = array_merge($this->opts[CURLOPT_HTTPHEADER], ['Content-type: ' . $mime]);

        // Nyalakan curl dan masukkan opsinya
        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        // Jalankan curl-nya
        $ret = curl_exec($ch);

        // Ambil informasinya
        $this->info = curl_getinfo($ch);

        // Tutup koneksi curl dan buffer
        curl_close($ch);
        fclose($fp);

        // Kalau file conflict, buat alias random
        if ($this->info['http_code'] == 409)
            return $this->putFromContent($content, $alias . '-' . rand(10, 99), $mime, $folder);

        // TODO: Perlu buat log/ notifikasi untuk pesan code lainnya, seperti HTTP 403 dan 413

        // Kalau gagal upload
        if ($this->info['http_code'] != 201)
            return false;

        // Kembalikan uri dari object storage
        return $uri;
    }

    /**
     * Untuk menambahkan metadata pada file yang sudah di setor ke object storage
     * @param $uri
     * @param array $metadatas
     *
     * @return bool
     */
    public function putMetadata($uri, $metadatas = [])
    {
        $urimeta = $uri . '?type=custom-metadata&annotation=filemetadata';

        $dom = new \DOMDocument('1.0', 'utf-8');
        $root = $dom->createElement('mako');
        $dom->appendChild($root);

        foreach ($metadatas as $key => $value) {
            $node = $dom->createElement($key);
            $node->appendChild($dom->createTextNode($value));
            $root->appendChild($node);
        }

        //$fp = fopen('php://memory','rw');
        $fp = fopen('php://temp ', 'w+');
        $xml = $dom->saveXML();
        fwrite($fp, $xml);
        rewind($fp);

        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $urimeta;
        $this->opts[CURLOPT_HTTPGET] = false;
        $this->opts[CURLOPT_PUT] = true;
        $this->opts[CURLOPT_INFILE] = $fp;
        $this->opts[CURLOPT_INFILESIZE] = strlen($xml);

        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        $ret = curl_exec($ch);
        $this->info = curl_getinfo($ch);
        curl_close($ch);
        fclose($fp);

        if ($this->info['http_code'] != 201)
            return false;

        return true;
    }

    /**
     * Untuk mengambil file dari object storage
     * @param $uri
     *
     * @return bool|false|string
     */
    private function getFile($uri)
    {
        //$fp = fopen("php://memory",'rw');
        $fp = fopen('php://temp ', 'w+');
        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_FILE] = $fp;
        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        $ret = curl_exec($ch);
        $this->info = curl_getinfo($ch);
        curl_close($ch);

        if ($this->info['http_code'] != 200) {
            fclose($fp);
            return false;
        }

        rewind($fp);
        $isi = stream_get_contents($fp);
        fclose($fp);
        return $isi;
    }

    /**
     * Untuk menghapus file dari object storage
     * @param $uri
     *
     * @return bool
     */
    public function delFile($uri)
    {
        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_HTTPGET] = false;
        $this->opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        $ret = curl_exec($ch);
        $this->info = curl_getinfo($ch);
        curl_close($ch);

        if ($this->info['http_code'] != 200)
            return false;

        return true;
    }

    /**
     * @param $file
     * @param $alias
     * @param $nama
     * @param $ukuran
     * @param $mime
     * @param $jenis
     * @param null $folder
     *
     * @return bool
     */
    public function putFilewData($file, $alias, $nama, $ukuran, $mime, $jenis, $folder = null)
    {
        $url = $this->putFromFile($file, $alias, $folder);

        if (\is_array($url)) {
            if ($url['http_code'] >= 300)
                // throw new \Exception('Storage service unavailable');
                // return false;
                die('Storage service unavailable');
            // $url = $url['url']; // hamya umtuk tes
        }

        if ($url === false)
            return false;

        if ($this->putMetadata($url, ['nama' => $nama, 'mime' => $mime, 'ukuran' => $ukuran, 'jenis' => $jenis]) === false)
            return false;

        return $url;
    }

    public function putContentwData($alias, $nama, $ukuran, $mime, $jenis, $content, $folder = null)
    {
        $uri = $this->putFromContent($content, $alias, $mime, $folder);

        if ($uri === false)
            throw new \Exception('Tidak dapat menyimpan ' . $nama . ' alias=' . $alias);

        if ($this->putMetadata($uri, ['nama' => $nama, 'mime' => $mime, 'ukuran' => $ukuran, 'jenis' => $jenis]) === false)
            throw new \Exception('Tidak dapat menyimpan metadata ' . $nama . ' alias=' . $alias);

        return $uri;
    }

    public function getFilewData($uri)
    {
        $isi = $this->getFile($uri);

        if ($isi === false)
            throw new \Exception('Tidak dapat mengambil ' . $uri);

        $ret = $this->getMetadata($uri);

        if ($ret === false)
            throw new \Exception('Tidak dapat mengambil metadata ' . $uri);

        $ret['Isi'] = $isi;
        return $ret;
    }

    public function getMetadata($uri)
    {
        $uri .= '?type=custom-metadata&annotation=filemetadata';
        //$fp = fopen("php://memory",'rw');
        $fp = fopen('php://temp ', 'w+');
        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_FILE] = $fp;
        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        $ret = curl_exec($ch);
        $this->info = curl_getinfo($ch);
        curl_close($ch);

        if ($this->info['http_code'] != 200)
            return false;

        rewind($fp);
        $isi = stream_get_contents($fp);
        fclose($fp);

        $ret = [];
        $dom = new \DOMDocument('1.0', 'utf-8');
        $dom->loadXML($isi);
        $xpath = new \DOMXPath($dom);
        $nodes = $xpath->query('//mako/nama');
        $ret['Nama'] = $nodes->item(0)->nodeValue;
        $nodes = $xpath->query('//mako/ukuran');
        $ret['Ukuran'] = $nodes->item(0)->nodeValue;
        $nodes = $xpath->query('//mako/mime');
        $ret['Mime'] = $nodes->item(0)->nodeValue;

        return $ret;
    }

    /**
     * Untuk mengambil file dari object storage
     * @param $uri
     *
     * @return bool|false|string
     */
    public function getFileDirect($uri, $location)
    {
        //Open file handler.
        $fp = fopen($location, 'w+');

        //If $fp is FALSE, something went wrong.
        if ($fp === false) {
            throw new \Exception('Could not open: ' . $location);
        }

        $this->setDefaultOpts();
        $this->opts[CURLOPT_URL] = $uri;
        $this->opts[CURLOPT_FILE] = $fp;
        $ch = $this->getHCPCurl();
        curl_setopt_array($ch, $this->opts);

        //Execute the request.
        curl_exec($ch);

        //If there was an error, throw an Exception
        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }

        //Get the HTTP status code.
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //Close the cURL handler.
        curl_close($ch);

        //Close the file handler.
        fclose($fp);

        if ($statusCode == 200) {
            return true;
        } else {
            return false;
        }
    }
}
